use crate::tokens::*;
use crate::error::*;
use std::collections::{HashSet, HashMap};

type TypeCheckResult = Result<Type, Vec<CompilerError>>;

#[derive(Clone)]
pub struct TypeContext {
    variables: HashMap<String, Variable>,
    functions: HashMap<String, Function>,
}

impl TypeContext {
    pub fn new() -> TypeContext {
        TypeContext {
            variables: Default::default(),
            functions: Default::default(),
        }
    }

    pub fn add_variable(&mut self, variable: Variable) {
        self.variables.insert(variable.clone().name, variable);
    }
    pub fn add_function(&mut self, function: Function) {
        self.functions.insert(function.clone().name, function);
    }

    pub fn find_variable_type(&self, name: String) -> Result<Type, CompilerError> {
        let variable = self.variables.get(&name);
        match variable {
            None => Err(CompilerError::NotFound),
            Some(variable) => Ok(variable.clone().variable_type),
        }
    }

    pub fn find_function_return_type(&self, name: String) -> Result<Type, CompilerError> {
        let function = self.find_function(name)?;
        return Ok(function.return_type);
    }

    pub fn find_function_paarmeters_type(&self, name: String) -> Result<Vec<Type>, CompilerError> {
        let function = self.find_function(name)?;
        let parameters = function.parameters;
        let parameters_types = parameters.iter().map(
            |parameter| parameter.clone().variable_type
        ).collect();
        Ok(parameters_types)
    }

    pub fn find_function(&self, name: String) -> Result<Function, CompilerError> {
        let function = self.functions.get(&name);
        match function {
            None => Err(CompilerError::NotFound),
            Some(function) => Ok(function.clone()),
        }
    }
}

pub fn check_variable(variable: Variable, context: TypeContext) -> TypeCheckResult {
    let name = variable.clone().name;
    let description = format!("variable '{}'", name);

    match context.find_variable_type(name) {
        Ok(variable_type) => Ok(variable_type),
        Err(error) => return Err(
            vec![
                CompilerError::ContentError(
                    description,
                    vec![error],
                )
            ]
        ),
    }
}

pub fn check_reference(reference: ReferenceTarget, mutable: Mutable, context: TypeContext) -> TypeCheckResult {
    match reference {
        ReferenceTarget::Expression(expression) => Ok(Type::Reference(Box::from(check_expression(*expression, context)?), mutable)),
        ReferenceTarget::Variable(variable) => Ok(Type::Reference(Box::from(check_variable(variable, context)?), mutable))
    }
}

pub fn check_dereference(expression: Expression, context: TypeContext) -> TypeCheckResult {
    let expression_type = check_expression(expression.clone(), context)?;

    match expression_type.clone() {
        Type::Reference(expression_type, _) => {
            Ok(*expression_type)
        },
        _ => Err(
            vec![
                CompilerError::BadTypes(
                    to_set(
                        &[
                            Type::Reference(
                                Box::from(Type::Any),
                                Mutable::Any,
                            )
                        ]
                    ),
                    vec![expression_type],
                    "can't dereference a non-reference expression".to_string()
                )
            ]
        )
    }
}

pub fn check_unary_operation(operator: UnaryOperator, expression: Expression, context: TypeContext) -> TypeCheckResult {
    let description = format!("unary operation '{}'", operator.get_sign());

    let real_type = check_expression(expression, context)?;
    let return_type = operator.get_return_type(real_type.clone());
    let allowed_types = operator.get_allowed_types();

    if return_type == Type::Unknown {
        return Err(vec![
            CompilerError::BadTypes(
                allowed_types,
                vec![real_type],
                description,
            )
        ]);
    }

    Ok(return_type)
}

pub fn check_binary_operation(left: Expression, operator: BinaryOperator, right: Expression, context: TypeContext) -> TypeCheckResult {
    let description = format!("binary operation '{}'", operator.get_sign());

    let mut type_errors: Vec<CompilerError> = vec![];
    let left_type = match check_expression(left, context.clone()) {
        Ok(expression_type) => expression_type,
        Err(errors) => {
            add_errors_from_statement_content(&mut type_errors, errors, description.clone());
            Type::Unknown
        }
    };
    let right_type = match check_expression(right, context.clone()) {
        Ok(expression_type) => expression_type,
        Err(errors) => {
            add_errors_from_statement_content(&mut type_errors, errors, description.clone());
            Type::Unknown
        }
    };

    if !type_errors.is_empty() {
        return Err(type_errors);
    }

    let return_type = operator.get_return_type(left_type.clone(), right_type.clone());
    let allowed_types = operator.get_allowed_types();

    if return_type == Type::Unknown {
        return Err(vec![
            CompilerError::BadTypes(
                allowed_types,
                vec![Type::Tuple(vec![left_type, right_type])],
                description,
            )
        ]);
    }

    Ok(return_type)
}

pub fn check_block(block: Block, context: TypeContext) -> TypeCheckResult {
    let mut type_errors: Vec<CompilerError> = vec![];
    let mut context = context.clone();

    let statements = match block.clone() {
        Block::NonReturning(statements) => statements,
        Block::Returning(statements, _) => statements,
    };

    for statement in statements {
        let result = check_statement(statement.clone(), context.clone());
        match result {
            Ok(_) => {}
            Err(errors) => {
                for error in errors {
                    type_errors.push(error);
                }
            }
        };
        match statement.clone() {
            Statement::Declaration(variable, _) => context.add_variable(variable),
            _ => {}
        }
    }

    let return_type = match block.clone() {
        Block::NonReturning(_) => Type::None,
        Block::Returning(_, statement) => {
            let result = check_statement(statement, context.clone());
            match result {
                Ok(result_type) => result_type,
                Err(errors) => {
                    for error in errors {
                        type_errors.push(error);
                    };
                    Type::Unknown
                }
            }
        }
    };

    if !type_errors.is_empty() {
        return Err(
            vec![
                CompilerError::ContentError(
                    String::from("block content"),
                    type_errors,
                )
            ]
        );
    } else {
        return Ok(return_type);
    }
}

pub fn check_expression(expression: Expression, context: TypeContext) -> TypeCheckResult {
    let expression_type = match expression {
        Expression::Number(_) => Type::Number,
        Expression::Boolean(_) => Type::Boolean,
        Expression::Reference(reference, mutable) => check_reference(reference, mutable, context)?,
        Expression::Dereference(expression) => check_dereference(*expression, context)?,
        Expression::Variable(variable) => check_variable(variable, context)?,
        Expression::UnaryOperation(operator, expression) => check_unary_operation(operator, *expression, context)?,
        Expression::BinaryOperation(left, operator, right) => check_binary_operation(*left, operator, *right, context)?,
        Expression::FunctionCall(function_call) => check_function_call(function_call, context)?,
        Expression::Block(block) => check_block(*block, context)?,
        Expression::If(if_block) => check_if(*if_block, context)?,
        Expression::ParenthizedExpression(expression) => check_expression(*expression, context)?,
        Expression::None => Type::Unknown,
    };

    Ok(expression_type)
}

pub fn check_declaration(variable: Variable, expression: Expression, context: TypeContext) -> TypeCheckResult {
    let description = format!("declaration of variable '{}'", variable.name);

    let variable_type = variable.variable_type;
    let expression_type = check_expression(expression, context)?;

    if variable_type == expression_type {
        Ok(Type::None)
    } else {
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[variable_type]),
                    vec![expression_type],
                    description,
                )
            ]
        )
    }
}

pub fn check_assignment(left_hand_side: LeftHandSide, expression: Expression, context: TypeContext) -> TypeCheckResult {
    match left_hand_side {
        LeftHandSide::Variable(variable) => check_assignment_to_variable(variable, expression, context),
        LeftHandSide::Reference(_, _) => Err(vec![CompilerError::NotImplemented]),
        LeftHandSide::Dereference(left_hand_side) => check_assignment_to_dereference(*left_hand_side, expression, context),
        LeftHandSide::None => Err(vec![CompilerError::NotImplemented]),
    }
}

pub fn check_assignment_to_variable(variable: Variable, expression: Expression, context: TypeContext) -> TypeCheckResult {
    let description = format!("assignment of variable '{}'", variable.name);

    let expression_type = match check_expression(expression, context.clone()) {
        Ok(t) => t,
        Err(errors) => return Err(
            vec![
                CompilerError::ContentError(
                    description,
                    errors
                )
            ]
        ),
    };
    let variable_type: Type;

    match context.clone().find_variable_type(variable.name) {
        Ok(actual_type) => variable_type = actual_type,
        Err(error) => return Err(
            vec![
                CompilerError::ContentError(
                    description,
                    vec![error],
                )
            ]
        ),
    };

    if variable_type == expression_type {
        Ok(Type::None)
    } else {
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[variable_type]),
                    vec![expression_type],
                    description,
                )
            ]
        )
    }
}

pub fn check_assignment_to_dereference(left_hand_side: LeftHandSide, expression: Expression, context: TypeContext) -> TypeCheckResult {
    let description = "assignment to a dereference".to_string();
    let expression_type = match check_expression(expression, context.clone()) {
        Ok(t) => t,
        Err(errors) => return Err(
            vec![
                CompilerError::ContentError(
                    description,
                    errors
                )
            ]
        ),
    };

    let left_type = match check_dereference(left_hand_side.into(), context) {
        Ok(t) => t,
        Err(errors) => return Err(
            vec![
                CompilerError::ContentError(
                    description,
                    errors
                )
            ]
        )
    };

    if left_type == expression_type {
        Ok(Type::None)
    } else {
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[left_type]),
                    vec![expression_type],
                    description,
                )
            ]
        )
    }
}

pub fn check_while(while_block: While, context: TypeContext) -> TypeCheckResult {
    let mut type_errors: Vec<CompilerError> = vec![];

    let condition = while_block.condition;
    let condition_type = match check_expression(condition, context.clone()) {
        Ok(condition_type) => condition_type,
        Err(errors) => {
            add_errors_from_statement_content(&mut type_errors, errors, String::from("while condition"));
            Type::Unknown
        }
    };

    if condition_type != Type::Boolean {
        let description = String::from("while condition");

        type_errors.push(
            CompilerError::BadTypes(
                to_set(&[Type::Boolean]),
                vec![condition_type],
                description,
            )
        )
    }

    let instructions = while_block.instructions;
    let result = check_block(instructions, context.clone());
    match result {
        Ok(return_type) => if return_type != Type::None {
            let description = String::from("while block");

            type_errors.push(CompilerError::BadTypes(
                to_set(&[Type::None]),
                vec![return_type],
                description,
            ))
        },
        Err(errors) => add_errors_from_statement_content(&mut type_errors, errors, String::from("while block")),
    };

    if !type_errors.is_empty() {
        return Err(type_errors);
    }
    return Ok(Type::None);
}

pub fn check_if(if_block: If, context: TypeContext) -> TypeCheckResult {
    let mut type_errors: Vec<CompilerError> = vec![];

    let condition = if_block.condition;
    let condition_type = match check_expression(condition, context.clone()) {
        Ok(condition_type) => condition_type,
        Err(errors) => {
            add_errors_from_statement_content(&mut type_errors, errors, String::from("if condition"));
            Type::Unknown
        }
    };

    if condition_type != Type::Boolean {
        let description = String::from("if condition");

        type_errors.push(
            CompilerError::BadTypes(
                to_set(&[Type::Boolean]),
                vec![condition_type],
                description,
            )
        )
    }

    let true_instructions = if_block.true_instructions;
    let result = check_block(true_instructions, context.clone());
    let mut true_return_type = Type::None;
    match result {
        Ok(return_type) => true_return_type = return_type,
        Err(errors) => add_errors_from_statement_content(&mut type_errors, errors, String::from("if block, true instructions")),
    };

    let false_instructions = if_block.false_instructions;
    match false_instructions {
        None => {
            if true_return_type != Type::None {
                let description = String::from("if block, true block can't have a return statement without false block");

                type_errors.push(CompilerError::BadTypes(
                    to_set(&[true_return_type.clone()]),
                    vec![Type::None],
                    description,
                ))
            }
        }
        Some(false_instructions) => {
            let result = check_block(false_instructions, context.clone());
            match result {
                Ok(return_type) => if return_type != true_return_type {
                    let description = String::from("if block, true and false block don't have the same return type");

                    type_errors.push(CompilerError::BadTypes(
                        to_set(&[true_return_type.clone()]),
                        vec![return_type],
                        description,
                    ))
                },
                Err(errors) => add_errors_from_statement_content(&mut type_errors, errors, String::from("if block, false instructions")),
            };
        }
    }

    if !type_errors.is_empty() {
        return Err(type_errors);
    }
    return Ok(true_return_type);
}

pub fn check_statement(statement: Statement, context: TypeContext) -> TypeCheckResult {
    match statement {
        Statement::Declaration(variable, expression) => check_declaration(variable, expression, context),
        Statement::Assignment(variable, expression) => check_assignment(variable, expression, context),
        Statement::Expression(expression) => check_expression(expression, context),
        Statement::While(while_block) => check_while(*while_block, context),
        Statement::If(if_block) => check_if(*if_block, context),
        Statement::None => Err(vec![CompilerError::NotImplemented]),
    }
}

pub fn check_function_call(function_call: FunctionCall, context: TypeContext) -> TypeCheckResult {
    let description = format!("call of function '{}'", function_call.clone().name);

    let parameters_types: Vec<Type>;

    match context.find_function_paarmeters_type(function_call.clone().name) {
        Ok(types) => parameters_types = types,
        Err(error) => {
            return Err(
                vec![
                    CompilerError::ContentError(
                        description,
                        vec![error],
                    )
                ]
            );
        }
    }

    let arguments_number = function_call.clone().arguments.len();
    let parameters_number = parameters_types.len();
    if arguments_number != parameters_number {
        return Err(
            vec![
                CompilerError::ContentError(
                    description,
                    vec![CompilerError::Other(format!("Expected {} arguments, found {}", parameters_number, arguments_number))],
                )
            ]
        );
    }

    let results: Vec<TypeCheckResult> = function_call.clone().arguments.iter().map(
        |argument| check_expression(argument.clone(), context.clone())
    ).collect();

    let mut arguments_types = vec![];
    let mut errors_in_arguments = vec![];
    for i in 0..results.len() {
        let result = results.get(i).unwrap();
        match result.clone() {
            Ok(argument_type) => arguments_types.push(argument_type),
            Err(error) => errors_in_arguments.push(
                CompilerError::ContentError(
                    format!("argument {} for function '{}'", i + 1, function_call.name),
                    error,
                )
            ),
        }
    }

    if !errors_in_arguments.is_empty() {
        return Err(
            vec![
                CompilerError::ContentError(
                    description,
                    errors_in_arguments,
                )
            ]
        );
    }

    let mut incorrect_arguments_errors = vec![];
    for i in 0..arguments_types.len() {
        let argument_type = arguments_types.get(i).unwrap();
        let parameter_type = parameters_types.get(i).unwrap();

        if parameter_type.clone() != Type::Any && argument_type.clone() != parameter_type.clone() {
            incorrect_arguments_errors.push(
                CompilerError::BadTypes(
                    to_set(&[parameter_type.clone()]),
                    vec![argument_type.clone()],
                    format!("argument {} for function '{}'", i + 1, function_call.clone().name),
                )
            );
        }
    }

    if !incorrect_arguments_errors.is_empty() {
        return Err(
            vec![
                CompilerError::ContentError(
                    description,
                    incorrect_arguments_errors,
                )
            ]
        );
    }

    Ok(context.find_function_return_type(function_call.clone().name)?)
}

pub fn check_function(function: Function, context: TypeContext) -> TypeCheckResult {
    let description = format!("function '{}'", function.clone().name);

    let mut context = context.clone();
    for parameter in function.clone().parameters {
        context.add_variable(parameter);
    }

    let instructions = function.clone().instructions;
    let instructions_return_type: Type;
    match check_block(instructions, context) {
        Ok(return_type) => instructions_return_type = return_type,
        Err(error) => return Err(
            vec![
                CompilerError::ContentError(
                    description,
                    error,
                )
            ]
        ),
    }

    if instructions_return_type != function.clone().return_type {
        return Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[function.clone().return_type]),
                    vec![instructions_return_type],
                    format!("return type for function '{}' does not match its real return type", function.clone().name),
                )
            ]
        );
    }

    Ok(function.clone().return_type)
}

pub fn check_file(file: File, context: TypeContext) -> TypeCheckResult {
    let mut context = context.clone();
    for function in file.clone().functions {
        context.add_function(function);
    }

    let mut functions_errors = vec![];
    for function in file.clone().functions {
        match check_function(function, context.clone()) {
            Ok(_) => {}
            Err(errors) => {
                for error in errors {
                    functions_errors.push(error);
                }
            }
        }
    };

    if !functions_errors.is_empty() {
        return Err(functions_errors);
    }

    Ok(Type::None)
}