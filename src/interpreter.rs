use crate::tokens::*;
use crate::error::*;
use crate::print::*;
use crate::context::{
    Context,
    MemoryLocation
};
use std::collections::HashMap;

// region Operators overloading
enum Position {
    LEFT,
    RIGHT,
    NONE,    // For unary operators
}

impl Position {
    fn get_position_name<'a>(self) -> &'a str {
        match self {
            Position::LEFT => "left",
            Position::RIGHT => "right",
            Position::NONE => "",
        }
    }
}

impl Expression {
    fn get_int_value(self, context: &Context, position: Position, operator: Box<dyn Operator>) -> i32 {
        let position = position.get_position_name();
        let operator = operator.get_sign();
        match self {
            Expression::Number(value) => value,
            Expression::Variable(variable) => {
                let variable_name = variable.name;
                let known_value = context.clone().find_variable_value(variable_name.clone()).expect(format!("Variable `{}` not known", variable_name).as_str());
                match known_value {
                    Expression::Number(value) => value,
                    _ => panic!("Bad type for {} operand of '{}' operation!", position, operator)
                }
            }
            _ => panic!("Bad type for {} operand of '{}' operation!", position, operator)
        }
    }
    fn get_boolean_value(self, context: &Context, position: Position, operator: Box<dyn Operator>) -> bool {
        let position = position.get_position_name();
        let operator = operator.get_sign();
        match self {
            Expression::Boolean(value) => value,
            Expression::Variable(variable) => {
                let variable_name = variable.name;
                let known_value = context.clone().find_variable_value(variable_name.clone()).expect(format!("Variable `{}` not known", variable_name).as_str());
                match known_value {
                    Expression::Boolean(value) => value,
                    _ => panic!("Bad type for {} operand of '{}' operation!", position, operator)
                }
            }
            _ => panic!("Bad type for {} operand of '{}' operation!", position, operator)
        }
    }

    fn add(left: Expression, right: Expression, context: &Context) -> Expression {
        let left = left.get_int_value(context, Position::LEFT, Box::from(BinaryOperator::Plus));
        let right = right.get_int_value(context, Position::RIGHT, Box::from(BinaryOperator::Plus));
        Expression::Number(left + right)
    }
    fn sub(left: Expression, right: Expression, context: &Context) -> Expression {
        let left = left.get_int_value(context, Position::LEFT, Box::from(BinaryOperator::Minus));
        let right = right.get_int_value(context, Position::RIGHT, Box::from(BinaryOperator::Minus));
        Expression::Number(left - right)
    }
    fn mul(left: Expression, right: Expression, context: &Context) -> Expression {
        let left = left.get_int_value(context, Position::LEFT, Box::from(BinaryOperator::Times));
        let right = right.get_int_value(context, Position::RIGHT, Box::from(BinaryOperator::Times));
        Expression::Number(left * right)
    }
    fn div(left: Expression, right: Expression, context: &Context) -> Expression {
        let left = left.get_int_value(context, Position::LEFT, Box::from(BinaryOperator::Divide));
        let right = right.get_int_value(context, Position::RIGHT, Box::from(BinaryOperator::Divide));
        Expression::Number(left / right)
    }
    fn rem(left: Expression, right: Expression, context: &Context) -> Expression {
        let left = left.get_int_value(context, Position::LEFT, Box::from(BinaryOperator::Modulo));
        let right = right.get_int_value(context, Position::RIGHT, Box::from(BinaryOperator::Modulo));
        Expression::Number(left % right)
    }
    fn pow(left: Expression, right: Expression, context: &Context) -> Expression {
        let left = left.get_int_value(context, Position::LEFT, Box::from(BinaryOperator::Power));
        let right = right.get_int_value(context, Position::RIGHT, Box::from(BinaryOperator::Power));
        Expression::Number(left.pow(right as u32))
    }
    fn lt(left: Expression, right: Expression, context: &Context) -> Expression {
        let left = left.get_int_value(context, Position::LEFT, Box::from(BinaryOperator::Power));
        let right = right.get_int_value(context, Position::RIGHT, Box::from(BinaryOperator::Power));
        Expression::Boolean(left < right)
    }
    fn and(left: Expression, right: Expression, context: &Context) -> Expression {
        let left = left.get_boolean_value(context, Position::LEFT, Box::from(BinaryOperator::And));
        let right = right.get_boolean_value(context, Position::RIGHT, Box::from(BinaryOperator::And));
        Expression::Boolean(left && right)
    }
    fn or(left: Expression, right: Expression, context: &Context) -> Expression {
        let left = left.get_boolean_value(context, Position::LEFT, Box::from(BinaryOperator::Or));
        let right = right.get_boolean_value(context, Position::RIGHT, Box::from(BinaryOperator::Or));
        Expression::Boolean(left || right)
    }

    fn neg(expression: Expression, context: &Context) -> Expression {
        let value = expression.get_int_value(context, Position::NONE, Box::from(UnaryOperator::Minus));
        Expression::Number(-value)
    }
    fn not(expression: Expression, context: &Context) -> Expression {
        let value = expression.get_boolean_value(context, Position::NONE, Box::from(UnaryOperator::Not));
        Expression::Boolean(!value)
    }
}
// endregion


pub fn evaluate_unary_operation(operator: UnaryOperator, expression: Expression, context: &mut Context) -> Result<Expression, CompilerError> {
    let expression = evaluate_expression(expression, context)?;
    match operator {
        UnaryOperator::Minus => Ok(Expression::neg(expression, context)),
        UnaryOperator::Not => Ok(Expression::not(expression, context)),
        UnaryOperator::None => Err(CompilerError::NoOperator),
    }
}

pub fn evaluate_binary_operation(left: Expression, operator: BinaryOperator, right: Expression, context: &mut Context) -> Result<Expression, CompilerError> {
    let left = evaluate_expression(left, context)?;
    let right = evaluate_expression(right, context)?;

    let expression = match operator {
        BinaryOperator::Plus => Expression::add(left, right, context),
        BinaryOperator::Minus => Expression::sub(left, right, context),
        BinaryOperator::Times => Expression::mul(left, right, context),
        BinaryOperator::Divide => {
            if right.clone().get_int_value(context, Position::RIGHT, Box::from(BinaryOperator::Divide)) == 0 {
                return Err(CompilerError::Other("Can't divide by 0".to_string()));
            }
            Expression::div(left, right, context)
        },
        BinaryOperator::Modulo => Expression::rem(left, right, context),
        BinaryOperator::And => Expression::and(left, right, context),
        BinaryOperator::Or => Expression::or(left, right, context),
        BinaryOperator::None => Expression::None,
        BinaryOperator::Equal => Expression::Boolean(left == right),
        BinaryOperator::Different => Expression::Boolean(left != right),
        BinaryOperator::LessThan => Expression::lt(left, right, context),
        BinaryOperator::Power => Expression::pow(left, right, context)
    };

    match expression {
        Expression::None => Err(CompilerError::NoOperator),
        expression => Ok(expression)
    }
}

pub fn evaluate_expression(expression: Expression, context: &mut Context) -> Result<Expression, CompilerError> {
    let expression = match expression {
        Expression::Number(_) => expression,
        Expression::Boolean(_) => expression,
        Expression::Reference(reference_target, _) => evaluate_reference(reference_target, context)?,
        Expression::Dereference(expression) => evaluate_dereference(*expression, context)?,
        Expression::Variable(variable) => evaluate_variable(variable, context)?,
        Expression::UnaryOperation(operator, expression) => evaluate_unary_operation(operator, *expression, context)?,
        Expression::BinaryOperation(left, operator, right) => evaluate_binary_operation(*left, operator, *right, context)?,
        Expression::FunctionCall(function_call) => evaluate_function_call(function_call, context)?,
        Expression::Block(block) => evaluate_block(*block, context, false)?,
        Expression::If(if_block) => evaluate_if(*if_block, context)?,
        Expression::ParenthizedExpression(expression) => evaluate_expression(*expression, context)?,
        Expression::None => expression,
    };

    Ok(expression)
}

fn evaluate_variable(variable: Variable, context: &Context) -> Result<Expression, CompilerError> {
    let variable_name = variable.name;
    Ok(context.clone().find_variable_value(variable_name.clone()).expect(format!("Variable `{}` not known", variable_name).as_str()))
}

fn evaluate_reference(reference: ReferenceTarget, context: &mut Context) -> Result<Expression, CompilerError> {
    match reference {
        ReferenceTarget::Expression(expression) => evaluate_expression(*expression, context),
        ReferenceTarget::Variable(variable) => evaluate_variable(variable, context),
    }
}

fn evaluate_dereference(dereference: Expression, context: &mut Context) -> Result<Expression, CompilerError> {
    match dereference.clone() {
        Expression::Reference(reference, _) => evaluate_reference(reference, context),
        Expression::Variable(variable) => {
            let (variable, _) = context.find_variable(variable.name)?;
            match variable.clone().variable_type {
                Type::Reference(_, _) => evaluate_variable(variable, context),
                _ => Err(
                    CompilerError::BadTypes(
                        to_set(
                            &[
                                Type::Reference
                                    (
                                        Box::from(Type::Any),
                                        Mutable::Any,
                                    )
                            ]
                        ),
                        vec![
                            Type::Unknown
                        ],
                        format!("can't dereference the non-reference variable {:?}", variable),
                    )
                )
            }
        },
        _ => Err(
            CompilerError::BadTypes(
                to_set(
                    &[
                        Type::Reference
                            (
                                Box::from(Type::Any),
                                Mutable::Any,
                            )
                    ]
                ),
                vec![
                    Type::Unknown
                ],
                format!("can't dereference the non-reference expression {:?}", dereference),
            )
        )
    }
}

pub fn evaluate_function_call(function_call: FunctionCall, context: &mut Context) -> Result<Expression, CompilerError> {
    let result = common_functions_call(function_call.clone(), context);
    if result.is_ok() {
        return result;
    }

    let error = result.err().unwrap();
    match error {
        CompilerError::NotFound => {
            let function_name = function_call.name;
            let function = context.clone().find_function(function_name.clone()).expect(format!("Function `{}` not known", function_name).as_str());

            let arguments_number = function_call.arguments.len();
            let parameters_number = function.parameters.len();

            if arguments_number != parameters_number {
                return Err(CompilerError::Interpretation(format!("Bad number of arguments for {} : expected {} found {}", function_name, parameters_number, arguments_number)));
            }

            let mut new_context = Context::new_for_function(context.clone());
            for i in 0..arguments_number {
                let argument = function_call.arguments.get(i).unwrap();
                let parameter = function.parameters.get(i).unwrap();
                match argument.clone() {
                    Expression::Reference(reference_target, _) => {
                        match reference_target.clone() {
                            ReferenceTarget::Expression(expression) => {
                                let result = evaluate_expression(*expression, context)?;
                                context.add_variable(parameter.clone(), result)?;
                            },
                            ReferenceTarget::Variable(referencee) => {
                                let (_, location) = context.find_variable(referencee.name)?;
                                new_context.add_reference(parameter.clone(), MemoryLocation {
                                    layer: location.layer + 1,
                                    location: location.location
                                })?;
                            },
                        }
                    },
                    Expression::Variable(variable) => {
                        let (variable, location) = context.find_variable(variable.name)?;
                        match variable.variable_type {
                            Type::Reference(_, _) => {
                                new_context.add_reference(parameter.clone(), MemoryLocation {
                                    layer: location.layer + 1,
                                    location: location.location
                                })?;
                            },
                            _ => {
                                new_context.add_variable(parameter.clone(), evaluate_expression(argument.clone(), context)?)?;
                            }
                        };
                    }
                    _ => {
                        new_context.add_variable(parameter.clone(), evaluate_expression(argument.clone(), context)?)?;
                    }
                };
            }

            let result = evaluate_function(function, &mut new_context);

            match new_context.super_context {
                None => {}
                Some(super_context) => {
                    super_context.replace_into_original_context(context)?;
                }
            };

            return result;
        }
        _ => Err(error),
    }
}

pub fn evaluate_function(function: Function, context: &mut Context) -> Result<Expression, CompilerError> {
    evaluate_block(function.instructions, context, true)
}

pub fn evaluate_assignment(left_hand_side: LeftHandSide, expression: Expression, context: &mut Context) -> Result<Expression, CompilerError> {
    match left_hand_side {
        LeftHandSide::Variable(variable) => evaluate_assignment_to_variable(variable, expression, context),
        LeftHandSide::Reference(_, _) => Err(CompilerError::NotImplemented),
        LeftHandSide::Dereference(left_hand_side) => evaluate_assignment_to_dereference(*left_hand_side, expression, context),
        LeftHandSide::None => Err(CompilerError::NotImplemented),
    }
}

pub fn evaluate_assignment_to_variable(variable: Variable, expression: Expression, context: &mut Context) -> Result<Expression, CompilerError> {
    let result = evaluate_expression(expression, context)?;

    let context_clone = &mut context.clone();
    let (variable, _) = context_clone.find_variable(variable.clone().name)?;
    context.replace_variable(variable.clone(), result)?;

    Ok(Expression::None)
}

pub fn evaluate_assignment_to_dereference(left_hand_side: LeftHandSide, expression: Expression, context: &mut Context) -> Result<Expression, CompilerError> {
    match left_hand_side {
        LeftHandSide::Variable(variable) => evaluate_assignment_to_variable(variable, expression, context),
        LeftHandSide::Reference(target, _) => {
            let referencee = evaluate_reference(target, context)?;
            match referencee.clone() {
                Expression::Variable(variable) => evaluate_assignment_to_variable(variable, expression, context),
                _ => Err(CompilerError::Other(format!("Cannot assign to expression '{:?}'", referencee)))
            }
        },
        LeftHandSide::Dereference(dereference) => evaluate_assignment_to_dereference(*dereference, expression, context),
        LeftHandSide::None => Err(CompilerError::NotImplemented),
    }
}

pub fn evaluate_statement(statement: Statement, context: &mut Context) -> Result<Expression, CompilerError> {
    let mut return_value: Expression = Expression::None;
    match statement {
        Statement::Declaration(variable, expression) => {
            match expression.clone() {
                Expression::Reference(reference_target, _) => {
                    match reference_target.clone() {
                        ReferenceTarget::Expression(expression) => {
                            let result = evaluate_expression(*expression, context)?;
                            context.add_variable(variable, result)?;
                        },
                        ReferenceTarget::Variable(referencee) => {
                            let (_, location) = context.find_variable(referencee.name)?;
                            context.add_reference(variable, location)?;
                        },
                    }
                },
                _ => {
                    let result = evaluate_expression(expression, context)?;
                    context.add_variable(variable, result)?;
                }
            };
        }
        Statement::Assignment(variable, expression) => {
            evaluate_assignment(variable, expression, context)?;
        }
        Statement::Expression(expression) => return_value = evaluate_expression(expression, context)?,
        Statement::While(while_block) => {
            evaluate_while(*while_block, context)?;
        }
        Statement::If(if_block) => {
            evaluate_if(*if_block, context)?;
        }
        Statement::None => {}  // A statement can be empty
    };

    Ok(return_value)
}

pub fn evaluate_block(block: Block, context: &mut Context, function_block: bool) -> Result<Expression, CompilerError> {
    let (non_returning_statements, returning_statement) = match block {
        Block::NonReturning(non_returning) => (non_returning, None),
        Block::Returning(non_returning, returning) => (non_returning, Some(returning)),
    };

    if function_block {
        for statement in non_returning_statements.iter() {
            evaluate_statement(statement.clone(), context)?;
        }
        let return_value = match returning_statement {
            None => Ok(Expression::None),
            Some(returning_statement) => evaluate_statement(returning_statement, context),
        };

        return_value
    } else {
        let mut block_context = Context::new_from_parent_context(context.clone());
        for statement in non_returning_statements.iter() {
            let result = evaluate_statement(statement.clone(), &mut block_context);
            result?;
        }
        let result = match returning_statement {
            None => Ok(Expression::None),
            Some(returning_statement) => {
                evaluate_statement(returning_statement, &mut block_context)
            }
        };

        match block_context.super_context {
            None => {}
            Some(super_context) => {
                super_context.replace_into_original_context(context)?;
            }
        };

        result
    }
}

pub fn evaluate_if(if_block: If, context: &mut Context) -> Result<Expression, CompilerError> {
    let condition = evaluate_expression(if_block.condition, context)?;
    let result = match condition.clone() {
        Expression::Boolean(v) => v,
        e => return Err(CompilerError::Interpretation(format!("Bad type for {:?} expected `Expression::Boolean`, found {:?}", condition, e).to_string()))
    };

    if result {
        return evaluate_block(if_block.true_instructions, context, false);
    } else {
        if if_block.false_instructions.is_some() {
            return evaluate_block(if_block.false_instructions.unwrap(), context, false);
        }
    }

    Ok(Expression::None)
}

pub fn evaluate_while(while_block: While, context: &mut Context) -> Result<Expression, CompilerError> {
    let unevaluated_condition = while_block.condition;
    let condition = evaluate_expression(unevaluated_condition.clone(), context)?;
    let mut result = match condition.clone() {
        Expression::Boolean(v) => v,
        e => return Err(CompilerError::Interpretation(format!("Bad type for {:?} expected `Expression::Boolean`, found {:?}", condition, e).to_string()))
    };

    while result {
        evaluate_block(while_block.instructions.clone(), context, false)?;

        result = match evaluate_expression(unevaluated_condition.clone(), context)? {
            Expression::Boolean(v) => v,
            e => return Err(CompilerError::Interpretation(format!("Bad type for {:?} expected `Expression::Boolean`, found {:?}", condition, e).to_string()))
        };
    }

    Ok(Expression::None)
}

pub fn evaluate_file(file: File) -> Result<Expression, CompilerError> {
    let mut context = Context::new();
    let functions = file.functions;
    for function in functions.iter() {
        context.add_function(function.clone());
    }

    let main_function = context.clone().find_function(String::from("main"))?;

    evaluate_function(main_function, &mut context)
}

// region Common functions
fn println(arguments: Vec<Expression>, context: &mut Context) -> Result<Expression, CompilerError> {
    if arguments.len() != 1 {
        return Err(CompilerError::Interpretation(format!("Arguments length error: expected 1 found {}", arguments.len())));
    }

    for expression in arguments.iter() {
        let value = evaluate_expression(expression.clone(), context)?;
        match value {
            Expression::Number(i) => print(format!("{:#?}", i)),
            Expression::Boolean(b) => print(format!("{:#?}", b)),
            x => print(format!("{:#?}", x))
        }
    }

    Ok(Expression::None)
}

fn common_functions_call(function_call: FunctionCall, context: &mut Context) -> Result<Expression, CompilerError> {
    let name = function_call.name;
    match name.as_str() {
        "println" => println(function_call.arguments, context),
        _ => Err(CompilerError::NotFound)
    }
}
// endregion