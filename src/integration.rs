use std::fs;

use crate::tokens::*;
use crate::error::CompilerError;
use crate::parser::parse_file;
use crate::interpreter::evaluate_file;
use crate::type_checker::{
    check_file,
    TypeContext
};
use crate::borrow_checker::{borrow_check_file, BorrowContext};

fn get_global_type_context() -> TypeContext {
    let mut context = TypeContext::new();

    context.add_function(
        Function {
            name: "println".to_string(),
            parameters: vec![
                Variable {
                    name: "p".to_string(),
                    variable_type: Type::Any,
                    mutable: false,
                }
            ],
            return_type: Type::None,
            instructions: Block::NonReturning(
                vec![]
            ),
        }
    );

    context
}

pub fn parse_check_interpret(file: &str) -> Result<Expression, CompilerError> {
    let file_content = fs::read_to_string(file)?;
    let input = format!("{}\n", file_content.as_str());
    let input = input.as_str();
    let ast = parse_file(input);
    let result = match ast {
        Ok(result) => {
            if result.0.is_empty() {
                result.1
            } else {
                return Err(CompilerError::FileNotEmpty(result.0.to_string()));
            }
        }
        Err(error) => match error {
            nom::Err::Incomplete(_) => return Err(CompilerError::Other("Need more data".to_string())),
            nom::Err::Error(e) => return Err(e.1),
            nom::Err::Failure(e) => return Err(e.1),
        },
    };

    let test = std::env::args().len() > 2;
    if test {
        println!("{:#?}", result.clone());
    }

    match check_file(result.clone(), get_global_type_context()) {
        Ok(_) => {}
        Err(errors) => return Err(
            CompilerError::ContentError(
                String::from("Type errors in file"),
                errors
            )
        ),
    };

    match borrow_check_file(result.clone(), &mut BorrowContext::new()) {
        Ok(_) => {}
        Err(errors) => return Err(
            CompilerError::ContentError(
                String::from("Borrow errors in file"),
                errors
            )
        ),
    };

    evaluate_file(result.clone())
}