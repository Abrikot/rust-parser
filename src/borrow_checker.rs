use crate::tokens::*;
use crate::error::*;
use std::collections::HashMap;
use crate::error::CompilerError::BorrowError;

type BorrowCheckResult = Result<BorrowMarker, Vec<CompilerError>>;

#[derive(Clone)]
pub struct BorrowContext {
    variables: HashMap<String, BorrowMarker>,
}

impl BorrowContext {
    pub fn new() -> BorrowContext {
        BorrowContext {
            variables: Default::default(),
        }
    }

    pub fn new_from_parent_context(parent_context: BorrowContext) -> BorrowContext {
        BorrowContext {
            variables: parent_context.variables.clone(),
        }
    }

    pub fn add_variable(&mut self, variable_name: String) {
        self.variables.insert(variable_name.clone(), BorrowMarker::None);
    }
    pub fn find_variable_marker(&self, name: String) -> Result<BorrowMarker, CompilerError> {
        let variable = self.variables.get(&name);
        match variable {
            None => Err(CompilerError::NotFound),
            Some(marker) => Ok(marker.clone()),
        }
    }

    pub fn change_borow_marker(&mut self, name: String, new_marker: BorrowMarker) {
        self.variables.insert(name, new_marker);
    }

    pub fn check_variable(&mut self, name: String, new_marker: BorrowMarker) -> BorrowCheckResult {
        let description = format!("variable '{}'", name.clone());

        let current_marker = match self.find_variable_marker(name.clone()) {
            Ok(marker) => marker,
            Err(error) => return Err(
                vec![
                    CompilerError::ContentError(
                        description,
                        vec![error],
                    )
                ]
            ),
        };

        if current_marker != BorrowMarker::None {
            if current_marker == BorrowMarker::Immutable && new_marker == BorrowMarker::Mutable
                || current_marker == BorrowMarker::Mutable && new_marker == BorrowMarker::Immutable
                || current_marker == BorrowMarker::Mutable && new_marker == BorrowMarker::Mutable {
                return Err(
                    vec![
                        CompilerError::ContentError(
                            description,
                            vec![
                                CompilerError::BorrowError(
                                    current_marker,
                                    new_marker,
                                )
                            ],
                        )
                    ]
                );
            }
        };

        Ok(new_marker)
    }
}

pub fn borrow_variable(variable_name: String, new_marker: BorrowMarker, context: &mut BorrowContext) -> BorrowCheckResult {
    context.check_variable(variable_name.clone(), new_marker.clone())?;
    context.change_borow_marker(variable_name.clone(), new_marker.clone());

    Ok(new_marker)
}

pub fn borrow_check_reference(reference: ReferenceTarget, mutable: Mutable, context: &mut BorrowContext) -> BorrowCheckResult {
    let new_marker = match mutable {
        Mutable::Mutable => BorrowMarker::Mutable,
        Mutable::Immutable => BorrowMarker::Immutable,
        Mutable::Any => BorrowMarker::None,
    };

    match reference {
        ReferenceTarget::Expression(expression) => Ok(BorrowMarker::None),
        ReferenceTarget::Variable(variable) => borrow_variable(variable.name, new_marker, context)
    }
}

pub fn borrow_check_binary_operation(left: Expression, operator: BinaryOperator, right: Expression, context: &mut BorrowContext) -> BorrowCheckResult {
    let description = format!("binary operation '{}'", operator.get_sign());

    let mut borrow_errors: Vec<CompilerError> = vec![];
    let left_type = match borrow_check_expression(left, context) {
        Ok(current_marker) => current_marker,
        Err(errors) => {
            add_errors_from_statement_content(&mut borrow_errors, errors, description.clone());
            BorrowMarker::None
        }
    };
    let right_type = match borrow_check_expression(right, context) {
        Ok(current_marker) => current_marker,
        Err(errors) => {
            add_errors_from_statement_content(&mut borrow_errors, errors, description.clone());
            BorrowMarker::None
        }
    };

    if !borrow_errors.is_empty() {
        return Err(borrow_errors);
    }

    Ok(BorrowMarker::None)
}

pub fn borrow_check_function_call(function_call: FunctionCall, context: &mut BorrowContext) -> BorrowCheckResult {
    let description = format!("call of function '{}'", function_call.clone().name);

    let mut arguments = function_call.arguments;
    let mut errors_in_arguments = vec![];
    for i in 0..arguments.len() {
        let argument = arguments.get(i).unwrap();
        match borrow_check_expression(argument.clone(), context) {
            Ok(_) => (),
            Err(error) => errors_in_arguments.push(
                CompilerError::ContentError(
                    format!("argument {} for function '{}'", i + 1, function_call.name),
                    error,
                )
            ),
        }
    }

    if !errors_in_arguments.is_empty() {
        return Err(
            vec![
                CompilerError::ContentError(
                    description,
                    errors_in_arguments,
                )
            ]
        );
    }

    Ok(BorrowMarker::None)
}

pub fn borrow_check_if(if_block: If, context: &mut BorrowContext) -> BorrowCheckResult {
    let description = "If block".to_string();

    let mut errors = vec![];
    match borrow_check_expression(if_block.condition, context) {
        Ok(_) => {}
        Err(error) => errors.push(
            CompilerError::ContentError(
                "Error in condition".to_string(),
                error,
            )
        ),
    };

    match borrow_check_block(if_block.true_instructions, context, false) {
        Ok(_) => {}
        Err(error) => errors.push(
            CompilerError::ContentError(
                "Error in 'true' block".to_string(),
                error,
            )
        ),
    };

    match if_block.false_instructions {
        None => {}
        Some(block) => match borrow_check_block(block, context, false) {
            Ok(_) => {}
            Err(error) => errors.push(
                CompilerError::ContentError(
                    "Error in 'false' block".to_string(),
                    error,
                )
            ),
        },
    };

    if !errors.is_empty() {
        return Err(
            vec![
                CompilerError::ContentError(
                    description,
                    errors,
                )
            ]
        );
    }

    Ok(BorrowMarker::None)
}

pub fn borrow_check_while(while_block: While, context: &mut BorrowContext) -> BorrowCheckResult {
    let description = "While block".to_string();

    let mut errors = vec![];
    match borrow_check_expression(while_block.condition, context) {
        Ok(_) => {}
        Err(error) => errors.push(
            CompilerError::ContentError(
                "Error in condition".to_string(),
                error,
            )
        ),
    };

    match borrow_check_block(while_block.instructions, context, false) {
        Ok(_) => {}
        Err(error) => errors.push(
            CompilerError::ContentError(
                "Error in block".to_string(),
                error,
            )
        ),
    };

    if !errors.is_empty() {
        return Err(
            vec![
                CompilerError::ContentError(
                    description,
                    errors,
                )
            ]
        );
    }

    Ok(BorrowMarker::None)
}

pub fn borrow_check_block(block: Block, context: &mut BorrowContext, function_block: bool) -> BorrowCheckResult {
    let description = "Block".to_string();

    let context: &mut BorrowContext = &mut if function_block {
        context.clone()
    } else {
        BorrowContext::new_from_parent_context(context.clone())
    };

    let non_returning_statements = match block.clone() {
        Block::NonReturning(non_returning) => non_returning,
        Block::Returning(non_returning, _) => non_returning,
    };

    let returning_statements = match block.clone() {
        Block::NonReturning(_) => None,
        Block::Returning(_, returning) => Some(returning),
    };

    let mut errors = vec![];
    for statement in non_returning_statements {
        match borrow_check_statement(statement, context) {
            Ok(_) => {}
            Err(error) => errors.push(
                CompilerError::ContentError(
                    "Error in statement".to_string(),
                    error,
                )
            ),
        };
    }

    let marker = match block.clone() {
        Block::NonReturning(_) => BorrowMarker::None,
        Block::Returning(_, statement) => match borrow_check_statement(statement, context) {
            Ok(inner_marker) => inner_marker,
            Err(error) => {
                errors.push(
                    CompilerError::ContentError(
                        "Error in returning statement".to_string(),
                        error,
                    )
                );
                BorrowMarker::None
            }
        },
    };

    if !errors.is_empty() {
        return Err(
            vec![
                CompilerError::ContentError(
                    description,
                    errors,
                )
            ]
        );
    }

    Ok(marker)
}

pub fn borrow_check_expression(expression: Expression, context: &mut BorrowContext) -> BorrowCheckResult {
    let marker = match expression {
        Expression::Number(_) => BorrowMarker::None,
        Expression::Boolean(_) => BorrowMarker::None,
        Expression::Reference(reference, mutable) => borrow_check_reference(reference, mutable, context)?,
        Expression::Dereference(expression) => borrow_check_expression(*expression, context)?,
        Expression::Variable(variable) => context.check_variable(variable.name, BorrowMarker::Immutable)?,
        Expression::UnaryOperation(operator, expression) => borrow_check_expression(*expression, context)?,
        Expression::BinaryOperation(left, operator, right) => borrow_check_binary_operation(*left, operator, *right, context)?,
        Expression::FunctionCall(function_call) => borrow_check_function_call(function_call, context)?,
        Expression::Block(block) => borrow_check_block(*block, context, false)?,
        Expression::If(if_block) => borrow_check_if(*if_block, context)?,
        Expression::ParenthizedExpression(expression) => borrow_check_expression(*expression, context)?,
        Expression::None => BorrowMarker::None,
    };

    Ok(marker)
}

pub fn borrow_check_statement(statement: Statement, context: &mut BorrowContext) -> BorrowCheckResult {
    let marker = match statement {
        Statement::Declaration(variable, expression) => {
            let inner_marker = borrow_check_expression(expression, context)?;
            context.add_variable(variable.name);
            inner_marker
        },
        Statement::Assignment(_, expression) => borrow_check_expression(expression, context)?,
        Statement::Expression(expression) => borrow_check_expression(expression, context)?,
        Statement::While(while_block) => borrow_check_while(*while_block, context)?,
        Statement::If(if_block) => borrow_check_if(*if_block, context)?,
        Statement::None => BorrowMarker::None,
    };

    Ok(marker)
}

pub fn borrow_check_function(function: Function, context: &mut BorrowContext) -> BorrowCheckResult {
    let description = format!("function '{}'", function.clone().name);

    let mut context = &mut BorrowContext::new();
    for parameter in function.parameters {
        context.add_variable(parameter.name);
    }

    let mut errors = vec![];
    match borrow_check_block(function.instructions, context, true) {
        Ok(_) => {}
        Err(error) => errors.push(
            CompilerError::ContentError(
                "Error in block".to_string(),
                error,
            )
        ),
    };

    if !errors.is_empty() {
        return Err(
            vec![
                CompilerError::ContentError(
                    description,
                    errors,
                )
            ]
        );
    }

    Ok(BorrowMarker::None)
}

pub fn borrow_check_file(file: File, context: &mut BorrowContext) -> BorrowCheckResult {
    let description = format!("File");

    let mut errors = vec![];
    for function in file.functions {
        match borrow_check_function(function.clone(), context) {
            Ok(_) => {}
            Err(error) => errors.push(
                CompilerError::ContentError(
                    format!("Error in function '{}'", function.clone().name).to_string(),
                    error,
                )
            ),
        };
    }

    if !errors.is_empty() {
        return Err(
            vec![
                CompilerError::ContentError(
                    description,
                    errors,
                )
            ]
        );
    }

    Ok(BorrowMarker::None)
}