#![allow(dead_code)]

extern crate nom;
extern crate clap;

use clap::{Arg, App, SubCommand};

mod parser;
mod interpreter;
mod tokens;
mod tests;
mod error;
mod print;
mod integration;
mod type_checker;
mod borrow_checker;
mod context;

use crate::integration::parse_check_interpret;

fn main() {
    let matches = App::new("Maxence Cornaton's Rust subset parser")
        .version("1.0")
        .author("Maxence Cornaton <cornatonmaxence@gmail.com>")
        .about("Parse, check and interpret a subset of Rust.")
        .arg(Arg::with_name("INPUT")
            .help("Sets the input file to use")
            .required(true)
            .index(1))
        .get_matches();

    let file = matches.value_of("INPUT").unwrap();
    match parse_check_interpret(file) {
        Ok(result) => (),
        Err(error) => panic!("Error: {:#?}", error)
    };
}