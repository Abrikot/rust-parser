use std::collections::HashSet;

#[derive(Debug, Eq, PartialEq, Clone, Hash)]
pub enum Type {
    Number,
    Boolean,
    Reference(Box<Type>, Mutable),
    Tuple(Vec<Type>),
    Any,
    Unknown,
    None,
}

#[derive(Debug, Eq, PartialEq, Clone, Hash)]
pub struct Variable {
    pub name: String,
    pub variable_type: Type,
    pub mutable: bool,
}

// region Operators
pub trait Operator {
    fn get_sign(&self) -> &str;
    fn get_allowed_types(&self) -> HashSet<Type>;
}

#[derive(Debug, PartialEq, Clone)]
pub enum BinaryOperator {
    Plus,
    Minus,
    Times,
    Divide,
    Modulo,
    And,
    Or,
    None,
    Equal,
    Different,
    LessThan,
    Power,
}

impl BinaryOperator {
    pub fn list<'a>() -> Vec<&'a str> {
        vec!["+", "-", "*", "/", "%", "&&", "||", "==", "^", "<", "!="]
    }

    pub fn get(sign: &str) -> BinaryOperator {
        match sign {
            "+" => BinaryOperator::Plus,
            "-" => BinaryOperator::Minus,
            "*" => BinaryOperator::Times,
            "/" => BinaryOperator::Divide,
            "%" => BinaryOperator::Modulo,
            "&&" => BinaryOperator::And,
            "||" => BinaryOperator::Or,
            "==" => BinaryOperator::Equal,
            "<" => BinaryOperator::LessThan,
            "!=" => BinaryOperator::Different,
            "^" => BinaryOperator::Power,
            _ => BinaryOperator::None
        }
    }

    pub fn get_return_type(&self, left: Type, right: Type) -> Type {
        match self {
            BinaryOperator::Plus => {
                if left == Type::Number && right == Type::Number {
                    return Type::Number;
                } else {
                    return Type::Unknown;
                }
            }
            BinaryOperator::Minus => {
                if left == Type::Number && right == Type::Number {
                    return Type::Number;
                } else {
                    return Type::Unknown;
                }
            }
            BinaryOperator::Times => {
                if left == Type::Number && right == Type::Number {
                    return Type::Number;
                } else {
                    return Type::Unknown;
                }
            }
            BinaryOperator::Divide => {
                if left == Type::Number && right == Type::Number {
                    return Type::Number;
                } else {
                    return Type::Unknown;
                }
            }
            BinaryOperator::Modulo => {
                if left == Type::Number && right == Type::Number {
                    return Type::Number;
                } else {
                    return Type::Unknown;
                }
            }
            BinaryOperator::And => {
                if left == Type::Boolean && right == Type::Boolean {
                    return Type::Boolean;
                } else {
                    return Type::Unknown;
                }
            }
            BinaryOperator::Or => {
                if left == Type::Boolean && right == Type::Boolean {
                    return Type::Boolean;
                } else {
                    return Type::Unknown;
                }
            }
            BinaryOperator::Equal => {
                if left == right {
                    return Type::Boolean;
                } else {
                    return Type::Unknown;
                }
            }
            BinaryOperator::Different => {
                if left == right {
                    return Type::Boolean;
                } else {
                    return Type::Unknown;
                }
            }
            BinaryOperator::LessThan => {
                if left == Type::Number && right == Type::Number {
                    return Type::Boolean;
                } else {
                    return Type::Unknown;
                }
            }
            BinaryOperator::Power => {
                if left == Type::Number && right == Type::Number {
                    return Type::Number;
                } else {
                    return Type::Unknown;
                }
            }
            BinaryOperator::None => Type::Unknown,
        }
    }

    pub fn get_precedence(&self) -> i32 {
        match self {
            BinaryOperator::Plus => 3,
            BinaryOperator::Minus => 3,
            BinaryOperator::Times => 4,
            BinaryOperator::Divide => 4,
            BinaryOperator::Modulo => 4,
            BinaryOperator::And => 1,
            BinaryOperator::Or => 1,
            BinaryOperator::Equal => 2,
            BinaryOperator::LessThan => 1,
            BinaryOperator::Power => 5,
            BinaryOperator::Different => 2,
            BinaryOperator::None => 0,
        }
    }
    pub fn get_associativity(&self) -> Associativity {
        match self {
            BinaryOperator::Plus => Associativity::Left,
            BinaryOperator::Minus => Associativity::Left,
            BinaryOperator::Times => Associativity::Left,
            BinaryOperator::Divide => Associativity::Left,
            BinaryOperator::Modulo => Associativity::Left,
            BinaryOperator::And => Associativity::Left,
            BinaryOperator::Or => Associativity::Left,
            BinaryOperator::Equal => Associativity::Left,
            BinaryOperator::Different => Associativity::Left,
            BinaryOperator::LessThan => Associativity::Left,
            BinaryOperator::Power => Associativity::Right,
            BinaryOperator::None => Associativity::Left,
        }
    }
}

impl Operator for BinaryOperator {
    fn get_sign(&self) -> &str {
        match self {
            BinaryOperator::Plus => "+",
            BinaryOperator::Minus => "-",
            BinaryOperator::Times => "*",
            BinaryOperator::Divide => "/",
            BinaryOperator::Modulo => "%",
            BinaryOperator::And => "&&",
            BinaryOperator::Or => "||",
            BinaryOperator::Equal => "==",
            BinaryOperator::Different => "!=",
            BinaryOperator::LessThan => "<",
            BinaryOperator::Power => "^",
            BinaryOperator::None => "",
        }
    }

    fn get_allowed_types(&self) -> HashSet<Type> {
        match self {
            BinaryOperator::Plus => [Type::Tuple(vec![Type::Number, Type::Number])].iter().cloned().collect(),
            BinaryOperator::Minus => [Type::Tuple(vec![Type::Number, Type::Number])].iter().cloned().collect(),
            BinaryOperator::Times => [Type::Tuple(vec![Type::Number, Type::Number])].iter().cloned().collect(),
            BinaryOperator::Divide => [Type::Tuple(vec![Type::Number, Type::Number])].iter().cloned().collect(),
            BinaryOperator::Modulo => [Type::Tuple(vec![Type::Number, Type::Number])].iter().cloned().collect(),
            BinaryOperator::And => [Type::Tuple(vec![Type::Boolean, Type::Boolean])].iter().cloned().collect(),
            BinaryOperator::Or => [Type::Tuple(vec![Type::Boolean, Type::Boolean])].iter().cloned().collect(),
            BinaryOperator::Equal => [Type::Tuple(vec![Type::Number, Type::Number]), Type::Tuple(vec![Type::Boolean, Type::Boolean])].iter().cloned().collect(),
            BinaryOperator::Different => [Type::Tuple(vec![Type::Number, Type::Number]), Type::Tuple(vec![Type::Boolean, Type::Boolean])].iter().cloned().collect(),
            BinaryOperator::LessThan => [Type::Tuple(vec![Type::Number, Type::Number])].iter().cloned().collect(),
            BinaryOperator::Power => [Type::Tuple(vec![Type::Number, Type::Number])].iter().cloned().collect(),
            BinaryOperator::None => [].iter().cloned().collect(),
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum Associativity {
    Right,
    Left,
}

#[derive(Debug, PartialEq, Clone)]
pub enum UnaryOperator {
    Minus,
    Not,
    None,
}

impl UnaryOperator {
    pub fn list<'a>() -> Vec<&'a str> {
        vec!["-", "!"]
    }

    pub fn get(sign: &str) -> UnaryOperator {
        match sign {
            "-" => UnaryOperator::Minus,
            "!" => UnaryOperator::Not,
            _ => UnaryOperator::None,
        }
    }

    pub fn get_return_type(&self, expression: Type) -> Type {
        match self {
            UnaryOperator::Minus => {
                if expression == Type::Number {
                    return Type::Number;
                } else {
                    return Type::Unknown;
                }
            }
            UnaryOperator::Not => {
                if expression == Type::Boolean {
                    return Type::Boolean;
                } else {
                    return Type::Unknown;
                }
            }
            UnaryOperator::None => Type::Unknown,
        }
    }
}

impl Operator for UnaryOperator {
    fn get_sign(&self) -> &str {
        match self {
            UnaryOperator::Minus => "-",
            UnaryOperator::Not => "!",
            UnaryOperator::None => "",
        }
    }

    fn get_allowed_types(&self) -> HashSet<Type> {
        match self {
            UnaryOperator::Minus => [Type::Number].iter().cloned().collect(),
            UnaryOperator::Not => [Type::Boolean].iter().cloned().collect(),
            UnaryOperator::None => [].iter().cloned().collect(),
        }
    }
}
// endregion


#[derive(Debug, PartialEq, Clone)]
pub enum ReferenceTarget {
    Expression(Box<Expression>),
    Variable(Variable),
}

#[derive(Debug, Eq, PartialEq, Clone, Hash)]
pub enum Mutable {
    Mutable,
    Immutable,
    Any,
}

#[derive(Debug, Eq, PartialEq, Clone, Hash)]
pub enum BorrowMarker {
    Immutable,  // An immutable reference exists
    Mutable,    // A mutable reference exists
    None        // No reference exists
}

impl std::convert::From<bool> for Mutable {
    fn from(mutable: bool) -> Self {
        match mutable {
            true => Mutable::Mutable,
            false => Mutable::Immutable
        }
    }
}

impl std::convert::From<Mutable> for bool {
    fn from(mutable: Mutable) -> Self {
        match mutable {
            Mutable::Mutable => true,
            Mutable::Immutable => false,
            Mutable::Any => false
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum Expression {
    Number(i32),
    Boolean(bool),
    Reference(ReferenceTarget, Mutable),
    Dereference(Box<Expression>),
    Variable(Variable),
    UnaryOperation(UnaryOperator, Box<Expression>),
    BinaryOperation(Box<Expression>, BinaryOperator, Box<Expression>),
    FunctionCall(FunctionCall),
    Block(Box<Block>),
    If(Box<If>),
    ParenthizedExpression(Box<Expression>),
    None,
}

#[derive(Debug, PartialEq, Clone)]
pub enum LeftHandSide {
    Variable(Variable),
    Reference(ReferenceTarget, Mutable),
    Dereference(Box<LeftHandSide>),
    None,
}

impl LeftHandSide {
    pub fn to_expression(&self) -> Expression {
        match self.clone() {
            LeftHandSide::Variable(variable) => Expression::Variable(variable),
            LeftHandSide::Reference(reference, mutable) => Expression::Reference(reference, mutable),
            LeftHandSide::Dereference(left_hand_side) => Expression::Dereference(
                Box::from(
                    left_hand_side.to_expression()
                )
            ),
            LeftHandSide::None => Expression::None,
        }
    }
}

impl std::convert::From<LeftHandSide> for Expression {
    fn from(left_hand_side: LeftHandSide) -> Self {
        left_hand_side.to_expression()
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum Statement {
    Declaration(Variable, Expression),
    Assignment(LeftHandSide, Expression),
    Expression(Expression),
    While(Box<While>),
    If(Box<If>),
    None,
}

#[derive(Debug, PartialEq, Clone)]
pub enum Block {
    NonReturning(Vec<Statement>),
    Returning(Vec<Statement>, Statement),
}

#[derive(Debug, PartialEq, Clone)]
pub struct Function {
    pub name: String,
    pub parameters: Vec<Variable>,
    pub return_type: Type,
    pub instructions: Block,
}

#[derive(Debug, PartialEq, Clone)]
pub struct FunctionCall {
    pub name: String,
    pub arguments: Vec<Expression>,
}

#[derive(Debug, PartialEq, Clone)]
pub struct File {
    pub functions: Vec<Function>
}

#[derive(Debug, PartialEq, Clone)]
pub struct If {
    pub condition: Expression,
    pub true_instructions: Block,
    pub false_instructions: Option<Block>,
}

#[derive(Debug, PartialEq, Clone)]
pub struct While {
    pub condition: Expression,
    pub instructions: Block,
}