use crate::tokens::{Expression, Type};

impl Expression {
    pub fn get_number() -> Expression {
        Expression::Number(42)
    }
    pub fn get_boolean() -> Expression {
        Expression::Boolean(true)
    }
    pub fn get_boxed_number() -> Box<Expression> {
        Box::from(Expression::get_number())
    }
    pub fn get_boxed_boolean() -> Box<Expression> {
        Box::from(Expression::get_boolean())
    }
}

impl Type {
    pub fn get_tuple_number_number() -> Type {
        Type::Tuple(vec![Type::Number, Type::Number])
    }
    pub fn get_tuple_boolean_boolean() -> Type {
        Type::Tuple(vec![Type::Boolean, Type::Boolean])
    }
    pub fn get_tuple_number_boolean() -> Type {
        Type::Tuple(vec![Type::Number, Type::Boolean])
    }
    pub fn get_tuple_boolean_number() -> Type {
        Type::Tuple(vec![Type::Boolean, Type::Number])
    }
}