use crate::{
    parser::*,
    tokens::*,
    context::*,
};

#[test]
fn t_parse_i32() {
    assert_eq!(
        parse_i32("42"),
        Ok((
            "",
            Expression::Number(42)
        ))
    )
}

#[test]
fn t_parse_boolean_true() {
    assert_eq!(
        parse_boolean("true"),
        Ok((
            "",
            Expression::Boolean(true)
        ))
    )
}

#[test]
fn t_parse_boolean_false() {
    assert_eq!(
        parse_boolean("false"),
        Ok((
            "",
            Expression::Boolean(false)
        ))
    )
}

#[test]
fn t_parse_terminal() {
    assert_eq!(
        parse_terminal("42"),
        Ok((
            "",
            Expression::Number(42)
        ))
    )
}

#[test]
fn t_parse_type() {
    assert_eq!(
        parse_type("i32"),
        Ok((
            "",
            Type::Number
        ))
    )
}

#[test]
fn t_parse_binary_operation() {
    assert_eq!(
        parse_binary_operation("1 + 2"),
        Ok((
            "",
            Expression::BinaryOperation(
                Box::from(Expression::Number(1)),
                BinaryOperator::Plus,
                Box::from(Expression::Number(2)),
            )
        ))
    )
}

#[test]
fn t_parse_parenthesis() {
    assert_eq!(
        parse_parenthesis("(1*2)"),
        Ok((
            "",
            Expression::ParenthizedExpression(
                Box::from(
                    Expression::BinaryOperation(
                        Box::from(Expression::Number(1)),
                        BinaryOperator::Times,
                        Box::from(Expression::Number(2)),
                    )
                )
            )
        ))
    )
}

#[test]
fn t_parse_unary_operation() {
    assert_eq!(
        parse_unary_operation("!2"),
        Ok((
            "",
            Expression::UnaryOperation(
                UnaryOperator::Not,
                Box::from(Expression::Number(2)),
            )
        ))
    )
}

#[test]
fn t_parse_expression() {
    assert_eq!(
        parse_expression("!-1+2"),
        Ok((
            "",
            Expression::BinaryOperation(
                Box::from(Expression::UnaryOperation(
                    UnaryOperator::Not,
                    Box::from(Expression::UnaryOperation(
                        UnaryOperator::Minus,
                        Box::from(Expression::Number(1)),
                    )),
                )),
                BinaryOperator::Plus,
                Box::from(Expression::Number(2)),
            )
        ))
    )
}

#[test]
fn t_parse_expression_with_parenthesis() {
    assert_eq!(
        parse_expression("(!1)+2"),
        Ok((
            "",
            Expression::BinaryOperation(
                Box::from(
                    Expression::ParenthizedExpression(
                        Box::from(Expression::UnaryOperation(
                            UnaryOperator::Not,
                            Box::from(Expression::Number(1)),
                        ))
                    )
                ),
                BinaryOperator::Plus,
                Box::from(Expression::Number(2)),
            )
        ))
    )
}

#[test]
fn t_parse_expression_with_block() {
    assert_eq!(
        parse_expression("1+{2/3;return 4-5;}"),
        Ok((
            "",
            Expression::BinaryOperation(
                Box::from(Expression::Number(1)),
                BinaryOperator::Plus,
                Box::from(
                    Expression::Block(
                        Box::from(
                            Block::Returning(
                                vec![
                                    Statement::Expression(
                                        Expression::BinaryOperation(
                                            Box::from(Expression::Number(2)),
                                            BinaryOperator::Divide,
                                            Box::from(Expression::Number(3)),
                                        )
                                    )
                                ],
                                Statement::Expression(
                                    Expression::BinaryOperation(
                                        Box::from(Expression::Number(4)),
                                        BinaryOperator::Minus,
                                        Box::from(Expression::Number(5)),
                                    )
                                ),
                            )
                        )
                    )
                ),
            )
        ))
    )
}

#[test]
fn t_parse_expression_with_if() {
    assert_eq!(
        parse_expression("1+if (1+2)==3 {\
        let hello_there : i32 = 3+1;\
        hello_there;return 2+2;\
        } else {}"),
        Ok((
            "",
            Expression::BinaryOperation(
                Box::from(Expression::Number(1)),
                BinaryOperator::Plus,
                Box::from(
                    Expression::If(
                        Box::from(
                            If {
                                condition: Expression::BinaryOperation(
                                    Box::from(
                                        Expression::ParenthizedExpression(
                                            Box::from(
                                                Expression::BinaryOperation(
                                                    Box::from(Expression::Number(1)),
                                                    BinaryOperator::Plus,
                                                    Box::from(Expression::Number(2)),
                                                )
                                            )
                                        )
                                    ),
                                    BinaryOperator::Equal,
                                    Box::from(Expression::Number(3)),
                                ),
                                true_instructions: Block::Returning(
                                    vec![
                                        Statement::Declaration(
                                            Variable {
                                                name: String::from("hello_there"),
                                                variable_type: Type::Number,
                                                mutable: false,
                                            },
                                            Expression::BinaryOperation(
                                                Box::from(Expression::Number(3)),
                                                BinaryOperator::Plus,
                                                Box::from(Expression::Number(1)),
                                            ),
                                        ),
                                        Statement::Expression(
                                            Expression::Variable(
                                                Variable {
                                                    name: String::from("hello_there"),
                                                    variable_type: Type::Unknown,
                                                    mutable: false,
                                                }
                                            )
                                        )
                                    ],
                                    Statement::Expression(
                                        Expression::BinaryOperation(
                                            Box::new(Expression::Number(2)),
                                            BinaryOperator::Plus,
                                            Box::new(Expression::Number(2)),
                                        )
                                    ),
                                ),
                                false_instructions: Some(
                                    Block::NonReturning(
                                        vec![]
                                    )
                                ),
                            }
                        )
                    )
                ),
            )
        ))
    )
}

#[test]
fn t_parse_variable_name() {
    assert_eq!(
        parse_variable("hello_there"),
        Ok((
            "",
            Expression::Variable(
                Variable {
                    name: String::from("hello_there"),
                    variable_type: Type::Unknown,
                    mutable: false,
                }
            )
        ))
    )
}

#[test]
fn t_parse_declaration_without_type() {
    assert_eq!(
        parse_declaration("let hello_there: i32 = 3+1"),
        Ok((
            "",
            Statement::Declaration(
                Variable {
                    name: String::from("hello_there"),
                    variable_type: Type::Number,
                    mutable: false,
                },
                Expression::BinaryOperation(
                    Box::from(Expression::Number(3)),
                    BinaryOperator::Plus,
                    Box::from(Expression::Number(1)),
                ),
            )
        ))
    )
}

#[test]
fn t_parse_declaration() {
    assert_eq!(
        parse_declaration("let hello_there:bool = 3+1"),
        Ok((
            "",
            Statement::Declaration(
                Variable {
                    name: String::from("hello_there"),
                    variable_type: Type::Boolean,
                    mutable: false,
                },
                Expression::BinaryOperation(
                    Box::from(Expression::Number(3)),
                    BinaryOperator::Plus,
                    Box::from(Expression::Number(1)),
                ),
            )
        ))
    )
}

#[test]
fn t_parse_reference_declaration() {
    assert_eq!(
        parse_declaration("let hello_there:&i32 = 3+1"),
        Ok((
            "",
            Statement::Declaration(
                Variable {
                    name: String::from("hello_there"),
                    variable_type: Type::Reference(Box::from(Type::Number), false.into()),
                    mutable: false,
                },
                Expression::BinaryOperation(
                    Box::from(Expression::Number(3)),
                    BinaryOperator::Plus,
                    Box::from(Expression::Number(1)),
                ),
            )
        ))
    )
}

#[test]
fn t_parse_mutable_reference_declaration() {
    assert_eq!(
        parse_declaration("let hello_there:&mut i32 = 3+1"),
        Ok((
            "",
            Statement::Declaration(
                Variable {
                    name: String::from("hello_there"),
                    variable_type: Type::Reference(Box::from(Type::Number), true.into()),
                    mutable: false,
                },
                Expression::BinaryOperation(
                    Box::from(Expression::Number(3)),
                    BinaryOperator::Plus,
                    Box::from(Expression::Number(1)),
                ),
            )
        ))
    )
}

#[test]
fn t_parse_declaration_mutable() {
    assert_eq!(
        parse_declaration("let mut hello_there:bool = 3+1"),
        Ok((
            "",
            Statement::Declaration(
                Variable {
                    name: String::from("hello_there"),
                    variable_type: Type::Boolean,
                    mutable: true,
                },
                Expression::BinaryOperation(
                    Box::from(Expression::Number(3)),
                    BinaryOperator::Plus,
                    Box::from(Expression::Number(1)),
                ),
            )
        ))
    )
}

#[test]
fn t_parse_assignment() {
    assert_eq!(
        parse_assignment("hello_there = 3+1"),
        Ok((
            "",
            Statement::Assignment(
                LeftHandSide::Variable(
                    Variable {
                        name: String::from("hello_there"),
                        variable_type: Type::Unknown,
                        mutable: false,
                    }
                ),
                Expression::BinaryOperation(
                    Box::from(Expression::Number(3)),
                    BinaryOperator::Plus,
                    Box::from(Expression::Number(1)),
                ),
            )
        ))
    )
}

#[test]
fn t_parse_non_returning_statement() {
    assert_eq!(
        parse_non_returning_statement("hello_there = 3+1;"),
        Ok((
            "",
            Statement::Assignment(
                LeftHandSide::Variable(
                    Variable {
                        name: String::from("hello_there"),
                        variable_type: Type::Unknown,
                        mutable: false,
                    }
                ),
                Expression::BinaryOperation(
                    Box::from(Expression::Number(3)),
                    BinaryOperator::Plus,
                    Box::from(Expression::Number(1)),
                ),
            )
        ))
    )
}

#[test]
fn t_parse_block() {
    assert_eq!(
        parse_block("{\
        let hello_there : i32 = 3+1;\
        hello_there;return 2+2;\
        }"),
        Ok((
            "",
            Block::Returning(
                vec![
                    Statement::Declaration(
                        Variable {
                            name: String::from("hello_there"),
                            variable_type: Type::Number,
                            mutable: false,
                        },
                        Expression::BinaryOperation(
                            Box::from(Expression::Number(3)),
                            BinaryOperator::Plus,
                            Box::from(Expression::Number(1)),
                        ),
                    ),
                    Statement::Expression(
                        Expression::Variable(
                            Variable {
                                name: String::from("hello_there"),
                                variable_type: Type::Unknown,
                                mutable: false,
                            }
                        )
                    )
                ],
                Statement::Expression(
                    Expression::BinaryOperation(
                        Box::new(Expression::Number(2)),
                        BinaryOperator::Plus,
                        Box::new(Expression::Number(2)),
                    )
                ),
            )
        ))
    )
}

#[test]
fn t_parse_function() {
    assert_eq!(
        parse_function("fn hello_world(var:i32 , boolean_var:bool) ->i32 {\
        let hello_there : i32 = 3+1;\
        hello_there;return 2+2;\
        }"),
        Ok((
            "",
            Function {
                name: String::from("hello_world"),
                parameters: vec![
                    Variable {
                        name: "var".to_string(),
                        variable_type: Type::Number,
                        mutable: false,
                    },
                    Variable {
                        name: "boolean_var".to_string(),
                        variable_type: Type::Boolean,
                        mutable: false,
                    }
                ],
                return_type: Type::Number,
                instructions: Block::Returning(
                    vec![
                        Statement::Declaration(
                            Variable {
                                name: String::from("hello_there"),
                                variable_type: Type::Number,
                                mutable: false,
                            },
                            Expression::BinaryOperation(
                                Box::from(Expression::Number(3)),
                                BinaryOperator::Plus,
                                Box::from(Expression::Number(1)),
                            ),
                        ),
                        Statement::Expression(
                            Expression::Variable(
                                Variable {
                                    name: String::from("hello_there"),
                                    variable_type: Type::Unknown,
                                    mutable: false,
                                }
                            )
                        )
                    ],
                    Statement::Expression(
                        Expression::BinaryOperation(
                            Box::new(Expression::Number(2)),
                            BinaryOperator::Plus,
                            Box::new(Expression::Number(2)),
                        )
                    ),
                ),
            }
        )
        )
    )
}

#[test]
fn t_parse_function_call() {
    assert_eq!(
        parse_function_call("hello_world(1+2,3)"),
        Ok((
            "",
            FunctionCall {
                name: String::from("hello_world"),
                arguments: vec![
                    Expression::BinaryOperation(
                        Box::from(Expression::Number(1)),
                        BinaryOperator::Plus,
                        Box::from(Expression::Number(2)),
                    ),
                    Expression::Number(3)
                ],
            }
        )
        )
    )
}

#[test]
fn t_parse_expression_with_function_call() {
    assert_eq!(
        parse_expression("abc+hello_world(1+2,3)"),
        Ok((
            "",
            Expression::BinaryOperation(
                Box::from(Expression::Variable(
                    Variable {
                        name: String::from("abc"),
                        variable_type: Type::Unknown,
                        mutable: false,
                    })),
                BinaryOperator::Plus,
                Box::from(
                    Expression::FunctionCall(
                        FunctionCall {
                            name: String::from("hello_world"),
                            arguments: vec![
                                Expression::BinaryOperation(
                                    Box::from(Expression::Number(1)),
                                    BinaryOperator::Plus,
                                    Box::from(Expression::Number(2)),
                                ),
                                Expression::Number(3)
                            ],
                        }
                    )
                ),
            ),
        )
        )
    )
}

#[test]
fn t_parse_file() {
    assert_eq!(
        parse_file("fn hello_world(var:i32 , boolean_var:bool) ->i32 {\
        let hello_there : i32 = 3+1;\
        hello_there;return 2+2;\
        } \
        fn world_hello() {\
        } \
        "),
        Ok((
            "",
            File {
                functions: vec![
                    Function {
                        name: String::from("hello_world"),
                        parameters: vec![
                            Variable {
                                name: "var".to_string(),
                                variable_type: Type::Number,
                                mutable: false,
                            },
                            Variable {
                                name: "boolean_var".to_string(),
                                variable_type: Type::Boolean,
                                mutable: false,
                            }
                        ],
                        return_type: Type::Number,
                        instructions: Block::Returning(
                            vec![
                                Statement::Declaration(
                                    Variable {
                                        name: String::from("hello_there"),
                                        variable_type: Type::Number,
                                        mutable: false,
                                    },
                                    Expression::BinaryOperation(
                                        Box::from(Expression::Number(3)),
                                        BinaryOperator::Plus,
                                        Box::from(Expression::Number(1)),
                                    ),
                                ),
                                Statement::Expression(
                                    Expression::Variable(
                                        Variable {
                                            name: String::from("hello_there"),
                                            variable_type: Type::Unknown,
                                            mutable: false,
                                        }
                                    )
                                )
                            ],
                            Statement::Expression(
                                Expression::BinaryOperation(
                                    Box::new(Expression::Number(2)),
                                    BinaryOperator::Plus,
                                    Box::new(Expression::Number(2)),
                                )
                            ),
                        ),
                    },
                    Function {
                        name: String::from("world_hello"),
                        parameters: vec![],
                        return_type: Type::None,
                        instructions: Block::NonReturning(
                            vec![]
                        ),
                    }
                ]
            }
        ))
    )
}

#[test]
fn t_parse_if() {
    assert_eq!(
        parse_if("if (1+2)==3 {\
        let hello_there : i32 = 3+1;\
        hello_there;return 2+2;\
        }"),
        Ok((
            "",
            If {
                condition: Expression::BinaryOperation(
                    Box::from(
                        Expression::ParenthizedExpression(
                            Box::from(
                                Expression::BinaryOperation(
                                    Box::from(Expression::Number(1)),
                                    BinaryOperator::Plus,
                                    Box::from(Expression::Number(2)),
                                )
                            )
                        )
                    ),
                    BinaryOperator::Equal,
                    Box::from(Expression::Number(3)),
                ),
                true_instructions: Block::Returning(
                    vec![
                        Statement::Declaration(
                            Variable {
                                name: String::from("hello_there"),
                                variable_type: Type::Number,
                                mutable: false,
                            },
                            Expression::BinaryOperation(
                                Box::from(Expression::Number(3)),
                                BinaryOperator::Plus,
                                Box::from(Expression::Number(1)),
                            ),
                        ),
                        Statement::Expression(
                            Expression::Variable(
                                Variable {
                                    name: String::from("hello_there"),
                                    variable_type: Type::Unknown,
                                    mutable: false,
                                }
                            )
                        )
                    ],
                    Statement::Expression(
                        Expression::BinaryOperation(
                            Box::new(Expression::Number(2)),
                            BinaryOperator::Plus,
                            Box::new(Expression::Number(2)),
                        )
                    ),
                ),
                false_instructions: None,
            }
        ))
    )
}

#[test]
fn t_parse_if_else() {
    assert_eq!(
        parse_if("if (1+2)==3 {\
        let hello_there : i32 = 3+1;\
        hello_there;return 2+2;\
        } else {}"),
        Ok((
            "",
            If {
                condition: Expression::BinaryOperation(
                    Box::from(
                        Expression::ParenthizedExpression(
                            Box::from(
                                Expression::BinaryOperation(
                                    Box::from(Expression::Number(1)),
                                    BinaryOperator::Plus,
                                    Box::from(Expression::Number(2)),
                                )
                            )
                        )
                    ),
                    BinaryOperator::Equal,
                    Box::from(Expression::Number(3)),
                ),
                true_instructions: Block::Returning(
                    vec![
                        Statement::Declaration(
                            Variable {
                                name: String::from("hello_there"),
                                variable_type: Type::Number,
                                mutable: false,
                            },
                            Expression::BinaryOperation(
                                Box::from(Expression::Number(3)),
                                BinaryOperator::Plus,
                                Box::from(Expression::Number(1)),
                            ),
                        ),
                        Statement::Expression(
                            Expression::Variable(
                                Variable {
                                    name: String::from("hello_there"),
                                    variable_type: Type::Unknown,
                                    mutable: false,
                                }
                            )
                        )
                    ],
                    Statement::Expression(
                        Expression::BinaryOperation(
                            Box::new(Expression::Number(2)),
                            BinaryOperator::Plus,
                            Box::new(Expression::Number(2)),
                        )
                    ),
                ),
                false_instructions: Some(
                    Block::NonReturning(
                        vec![]
                    )
                ),
            }
        ))
    )
}

#[test]
fn t_parse_while() {
    assert_eq!(
        parse_while("while (1+2)<3 {\
        let hello_there : i32 = 3+1;\
        hello_there;return 2+2;\
        }"),
        Ok((
            "",
            While {
                condition: Expression::BinaryOperation(
                    Box::from(
                        Expression::ParenthizedExpression(
                            Box::from(
                                Expression::BinaryOperation(
                                    Box::from(Expression::Number(1)),
                                    BinaryOperator::Plus,
                                    Box::from(Expression::Number(2)),
                                )
                            )
                        )
                    ),
                    BinaryOperator::LessThan,
                    Box::from(Expression::Number(3)),
                ),
                instructions: Block::Returning(
                    vec![
                        Statement::Declaration(
                            Variable {
                                name: String::from("hello_there"),
                                variable_type: Type::Number,
                                mutable: false,
                            },
                            Expression::BinaryOperation(
                                Box::from(Expression::Number(3)),
                                BinaryOperator::Plus,
                                Box::from(Expression::Number(1)),
                            ),
                        ),
                        Statement::Expression(
                            Expression::Variable(
                                Variable {
                                    name: String::from("hello_there"),
                                    variable_type: Type::Unknown,
                                    mutable: false,
                                }
                            )
                        )
                    ],
                    Statement::Expression(
                        Expression::BinaryOperation(
                            Box::new(Expression::Number(2)),
                            BinaryOperator::Plus,
                            Box::new(Expression::Number(2)),
                        )
                    ),
                ),
            }
        ))
    )
}

#[test]
fn t_parse_expression_with_precedence_climbing() {
    let (_, result) = parse_expression("1 + 2 * 3 ^ 4 * 5").unwrap();
    assert_eq!(
        reorder_expr(result),
        Expression::BinaryOperation(
            Box::from(Expression::Number(1)),
            BinaryOperator::Plus,
            Box::from(
                Expression::BinaryOperation(
                    Box::from(
                        Expression::BinaryOperation(
                            Box::from(Expression::Number(2)),
                            BinaryOperator::Times,
                            Box::from(
                                Expression::BinaryOperation(
                                    Box::from(Expression::Number(3)),
                                    BinaryOperator::Power,
                                    Box::from(Expression::Number(4)),
                                )
                            ),
                        )
                    ),
                    BinaryOperator::Times,
                    Box::from(Expression::Number(5)),
                )
            ),
        )
    )
}

#[test]
fn t_parse_reference() {
    assert_eq!(
        parse_expression("&2"),
        Ok((
            "",
            Expression::Reference(
                ReferenceTarget::Expression(
                    Box::from(
                        Expression::Number(
                            2
                        )
                    )
                ),
                Mutable::Immutable,
            )
        ))
    )
}

#[test]
fn t_parse_mutable_reference() {
    assert_eq!(
        parse_expression("& mut 2"),
        Ok((
            "",
            Expression::Reference(
                ReferenceTarget::Expression(
                    Box::from(
                        Expression::Number(
                            2
                        )
                    )
                ),
                Mutable::Mutable,
            )
        ))
    )
}

#[test]
fn t_parse_declaration_mutable_reference() {
    assert_eq!(
        parse_declaration("let variable: &mut i32 = & mut 2"),
        Ok((
            "",
            Statement::Declaration(
                Variable {
                    name: "variable".to_string(),
                    variable_type: Type::Reference(
                        Box::from(Type::Number),
                        Mutable::Mutable,
                    ),
                    mutable: false,
                },
                Expression::Reference(
                    ReferenceTarget::Expression(
                        Box::from(
                            Expression::Number(
                                2
                            )
                        )
                    ),
                    Mutable::Mutable,
                ),
            )
        ))
    )
}


#[test]
fn t_parse_dereference() {
    assert_eq!(
        parse_expression("* & mut 2"),
        Ok((
            "",
            Expression::Dereference(
                Box::from(
                    Expression::Reference(
                        ReferenceTarget::Expression(
                            Box::from(
                                Expression::Number(
                                    2
                                )
                            )
                        ),
                        Mutable::Mutable,
                    )
                )
            )
        ))
    )
}