// Tests should be run with the option "-- --test-threads=1".
// Otherwise the behaviour would be unknown: tests output can overwrite themselves.


#[path="parser.rs"]
mod t_parser;
#[path="interpreter.rs"]
mod t_interpreter;
#[path="integration.rs"]
mod t_integration;
#[path="type_checker.rs"]
mod t_type_checker;
#[path="borrow_checker.rs"]
mod t_borrow_checker;
#[path="expression.rs"]
mod t_expression;

use std::fs;

const TEST_FILE_NAME: &str = "test.output";
const TEST_RESULTS_FOLDER: &str = "D:\\Documents\\Rust\\parser\\tests";
const DELETE_TEST_FILE: bool = true;

fn get_expected_result_file_path(filename: &str) -> String {
    [TEST_RESULTS_FOLDER, filename].join("\\")
}

pub fn get_test_file_path() -> String {
    [TEST_RESULTS_FOLDER, TEST_FILE_NAME].join("\\")
}

fn create_test_file() {
    let _ = fs::File::create(get_test_file_path());
}

fn delete_test_file() {
    let _ = fs::remove_file(get_test_file_path());
}

fn read_test_file() -> String {
    fs::read_to_string(get_test_file_path()).expect("Error when reading file")
}

fn read_file(filename: &str) -> String {
    fs::read_to_string(get_expected_result_file_path(filename)).expect("Error when reading file")
}

fn test_output(test_function: fn () -> (), expected_output_file: &str) {
    let expected_output = read_file(expected_output_file);

    create_test_file();

    test_function();

    let output = read_test_file();
    if output != expected_output {
        println!("Different outputs.\nExpected:\n{:#?}\n\nReal:\n{:#?}", expected_output, output);
        panic!("Different outputs");
    }

    if DELETE_TEST_FILE {
        delete_test_file();
    }
}