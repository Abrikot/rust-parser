use crate::integration::*;
use crate::tokens::{
    Expression,
    Type
};
use crate::error::{
    CompilerError,
    to_set
};

const INTEGRATION_FOLDER: &str = "integration";

fn test_output(test_function: fn() -> (), expected_output_file: &str) {
    let expected_output_file = [INTEGRATION_FOLDER, expected_output_file].join("\\");
    let expected_output_file = expected_output_file.as_str();
    super::test_output(test_function, expected_output_file);
}

#[test]
fn t_single_function() {
    test_output(
        || assert_eq!(
            parse_check_interpret("D:\\Documents\\Rust\\parser\\tests\\integration\\single_function.rs"),
            Ok(Expression::None)
        ),
        "t_single_function"
    );
}

#[test]
fn t_function_call() {
    test_output(
        || assert_eq!(
            parse_check_interpret("D:\\Documents\\Rust\\parser\\tests\\integration\\function_call.rs"),
            Ok(Expression::None)
        ),
        "t_function_call"
    );
}

#[test]
fn t_function_call_with_reference() {
    test_output(
        || assert_eq!(
            parse_check_interpret("D:\\Documents\\Rust\\parser\\tests\\integration\\function_call_with_reference.rs"),
            Ok(Expression::None)
        ),
        "t_function_call_with_reference"
    );
}

#[test]
fn t_shadowing() {
    test_output(
        || assert_eq!(
            parse_check_interpret("D:\\Documents\\Rust\\parser\\tests\\integration\\shadowing.rs"),
            Ok(Expression::None)
        ),
        "t_shadowing"
    );
}

#[test]
fn t_while() {
    test_output(
        || assert_eq!(
            parse_check_interpret("D:\\Documents\\Rust\\parser\\tests\\integration\\while.rs"),
            Ok(Expression::None)
        ),
        "t_while"
    );
}

#[test]
fn t_references() {
    test_output(
        || assert_eq!(
            parse_check_interpret("D:\\Documents\\Rust\\parser\\tests\\integration\\references.rs"),
            Ok(Expression::None)
        ),
        "t_references"
    );
}

#[test]
fn t_types() {
    assert_eq!(
        parse_check_interpret("D:\\Documents\\Rust\\parser\\tests\\integration\\type_errors.rs"),
        Err(
            CompilerError::ContentError(
                String::from("Type errors in file"),
                vec![
                    CompilerError::ContentError(
                        String::from("function 'main'"),
                        vec![
                            CompilerError::ContentError(
                                String::from("block content"),
                                vec![
                                    CompilerError::BadTypes(
                                        to_set(&[Type::Number]),
                                        vec![Type::Boolean],
                                        String::from("declaration of variable 'x'")
                                    ),
                                    CompilerError::ContentError(
                                        String::from("call of function 'test'"),
                                        vec![
                                            CompilerError::BadTypes(
                                                to_set(&[Type::Number]),
                                                vec![Type::Boolean],
                                                String::from("argument 1 for function 'test'")
                                            )
                                        ]
                                    )
                                ]
                            )
                        ]
                    ),
                    CompilerError::ContentError(
                        String::from("function 'test'"),
                        vec![
                            CompilerError::ContentError(
                                String::from("block content"),
                                vec![
                                    CompilerError::BadTypes(
                                        to_set(&[Type::Boolean]),
                                        vec![Type::Number],
                                        String::from("declaration of variable 'x'")
                                    )
                                ]
                            )
                        ]
                    )
                ]
            )
        )
    )
}