use crate::{
    tokens::*,
    borrow_checker::*,
    error::*,
};

#[test]
fn t_borrow_none_immutable() {
    let mut context = BorrowContext::new();
    context.add_variable(String::from("a"));

    assert_eq!(
        borrow_variable(
            String::from("a"),
            BorrowMarker::Immutable,
            &mut context,
        ),
        Ok(
            BorrowMarker::Immutable
        )
    )
}

#[test]
fn t_borrow_none_mutable() {
    let mut context = BorrowContext::new();
    context.add_variable(String::from("a"));

    assert_eq!(
        borrow_variable(
            String::from("a"),
            BorrowMarker::Mutable,
            &mut context,
        ),
        Ok(
            BorrowMarker::Mutable
        )
    )
}

#[test]
fn t_borrow_mutable_mutable() {
    let mut context = BorrowContext::new();
    context.add_variable(String::from("a"));
    context.change_borow_marker(String::from("a"), BorrowMarker::Mutable);

    assert_eq!(
        borrow_variable(
            String::from("a"),
            BorrowMarker::Mutable,
            &mut context,
        ),
        Err(
            vec![
                CompilerError::ContentError(
                    String::from("variable 'a'"),
                    vec![
                        CompilerError::BorrowError(
                            BorrowMarker::Mutable,
                            BorrowMarker::Mutable,
                        )
                    ],
                )
            ]
        )
    )
}

#[test]
fn t_borrow_immutable_mutable() {
    let mut context = BorrowContext::new();
    context.add_variable(String::from("a"));
    context.change_borow_marker(String::from("a"), BorrowMarker::Immutable);

    assert_eq!(
        borrow_variable(
            String::from("a"),
            BorrowMarker::Mutable,
            &mut context,
        ),
        Err(
            vec![
                CompilerError::ContentError(
                    String::from("variable 'a'"),
                    vec![
                        CompilerError::BorrowError(
                            BorrowMarker::Immutable,
                            BorrowMarker::Mutable,
                        )
                    ],
                )
            ]
        )
    )
}

#[test]
fn t_borrow_mutable_immutable() {
    let mut context = BorrowContext::new();
    context.add_variable(String::from("a"));
    context.change_borow_marker(String::from("a"), BorrowMarker::Mutable);

    assert_eq!(
        borrow_variable(
            String::from("a"),
            BorrowMarker::Immutable,
            &mut context,
        ),
        Err(
            vec![
                CompilerError::ContentError(
                    String::from("variable 'a'"),
                    vec![
                        CompilerError::BorrowError(
                            BorrowMarker::Mutable,
                            BorrowMarker::Immutable,
                        )
                    ],
                )
            ]
        )
    )
}

#[test]
fn t_borrow_immutable_immutable() {
    let mut context = BorrowContext::new();
    context.add_variable(String::from("a"));
    context.change_borow_marker(String::from("a"), BorrowMarker::Immutable);

    assert_eq!(
        borrow_variable(
            String::from("a"),
            BorrowMarker::Immutable,
            &mut context,
        ),
        Ok(
            BorrowMarker::Immutable
        )
    );
}

#[test]
fn t_borrow_check_none_immutable() {
    let mut context = BorrowContext::new();
    context.add_variable(String::from("a"));

    assert_eq!(
        borrow_check_expression(
            Expression::Reference(
                ReferenceTarget::Variable(
                    Variable {
                        name: String::from("a"),
                        variable_type: Type::None,
                        mutable: true,
                    }
                ),
                Mutable::Immutable,
            ),
            &mut context,
        ),
        Ok(
            BorrowMarker::Immutable
        )
    )
}

#[test]
fn t_borrow_check_none_mutable() {
    let mut context = BorrowContext::new();
    context.add_variable(String::from("a"));

    assert_eq!(
        borrow_check_expression(
            Expression::Reference(
                ReferenceTarget::Variable(
                    Variable {
                        name: String::from("a"),
                        variable_type: Type::None,
                        mutable: true,
                    }
                ),
                Mutable::Mutable,
            ),
            &mut context,
        ),
        Ok(
            BorrowMarker::Mutable
        )
    )
}

#[test]
fn t_borrow_check_mutable_mutable() {
    let mut context = BorrowContext::new();
    context.add_variable(String::from("a"));
    context.change_borow_marker(String::from("a"), BorrowMarker::Mutable);

    assert_eq!(
        borrow_check_expression(
            Expression::Reference(
                ReferenceTarget::Variable(
                    Variable {
                        name: String::from("a"),
                        variable_type: Type::None,
                        mutable: true,
                    }
                ),
                Mutable::Mutable,
            ),
            &mut context,
        ),
        Err(
            vec![
                CompilerError::ContentError(
                    String::from("variable 'a'"),
                    vec![
                        CompilerError::BorrowError(
                            BorrowMarker::Mutable,
                            BorrowMarker::Mutable,
                        )
                    ],
                )
            ]
        )
    )
}

#[test]
fn t_borrow_check_immutable_mutable() {
    let mut context = BorrowContext::new();
    context.add_variable(String::from("a"));
    context.change_borow_marker(String::from("a"), BorrowMarker::Immutable);

    assert_eq!(
        borrow_check_expression(
            Expression::Reference(
                ReferenceTarget::Variable(
                    Variable {
                        name: String::from("a"),
                        variable_type: Type::None,
                        mutable: true,
                    }
                ),
                Mutable::Mutable,
            ),
            &mut context,
        ),
        Err(
            vec![
                CompilerError::ContentError(
                    String::from("variable 'a'"),
                    vec![
                        CompilerError::BorrowError(
                            BorrowMarker::Immutable,
                            BorrowMarker::Mutable,
                        )
                    ],
                )
            ]
        )
    )
}

#[test]
fn t_borrow_check_mutable_immutable() {
    let mut context = BorrowContext::new();
    context.add_variable(String::from("a"));
    context.change_borow_marker(String::from("a"), BorrowMarker::Mutable);

    assert_eq!(
        borrow_check_expression(
            Expression::Reference(
                ReferenceTarget::Variable(
                    Variable {
                        name: String::from("a"),
                        variable_type: Type::None,
                        mutable: true,
                    }
                ),
                Mutable::Immutable,
            ),
            &mut context,
        ),
        Err(
            vec![
                CompilerError::ContentError(
                    String::from("variable 'a'"),
                    vec![
                        CompilerError::BorrowError(
                            BorrowMarker::Mutable,
                            BorrowMarker::Immutable,
                        )
                    ],
                )
            ]
        )
    )
}

#[test]
fn t_borrow_check_immutable_immutable() {
    let mut context = BorrowContext::new();
    context.add_variable(String::from("a"));
    context.change_borow_marker(String::from("a"), BorrowMarker::Immutable);

    assert_eq!(
        borrow_check_expression(
            Expression::Reference(
                ReferenceTarget::Variable(
                    Variable {
                        name: String::from("a"),
                        variable_type: Type::None,
                        mutable: true,
                    }
                ),
                Mutable::Immutable,
            ),
            &mut context,
        ),
        Ok(
            BorrowMarker::Immutable
        )
    );
}

#[test]
fn t_borrow_check_unary_operation() {
    let mut context = BorrowContext::new();

    assert_eq!(
        borrow_check_expression(
            Expression::Reference(
                ReferenceTarget::Expression(
                    Box::from(
                        Expression::UnaryOperation(
                            UnaryOperator::Minus,
                            Box::from(
                                Expression::Number(42)
                            ),
                        )
                    )
                ),
                Mutable::Immutable,
            ),
            &mut context,
        ),
        Ok(
            BorrowMarker::None
        )
    );
}

#[test]
fn t_borrow_check_binary_operation_immutable_immutable() {
    let mut context = BorrowContext::new();
    context.add_variable(String::from("a"));
    context.change_borow_marker(String::from("a"), BorrowMarker::Immutable);

    assert_eq!(
        borrow_check_expression(
            Expression::BinaryOperation(
                Box::from(
                    Expression::Reference(
                        ReferenceTarget::Variable(
                            Variable {
                                name: "a".to_string(),
                                variable_type: Type::Number,
                                mutable: false,
                            }
                        ),
                        Mutable::Immutable,
                    )
                ),
                BinaryOperator::Minus,
                Box::from(
                    Expression::Variable(
                        Variable {
                            name: "a".to_string(),
                            variable_type: Type::Number,
                            mutable: false,
                        }
                    )
                ),
            ),
            &mut context,
        ),
        Ok(
            BorrowMarker::None
        )
    );
}

#[test]
fn t_borrow_check_binary_operation_immutable_mutable() {
    let mut context = BorrowContext::new();
    context.add_variable(String::from("a"));
    context.change_borow_marker(String::from("a"), BorrowMarker::Immutable);

    assert_eq!(
        borrow_check_expression(
            Expression::BinaryOperation(
                Box::from(
                    Expression::Reference(
                        ReferenceTarget::Variable(
                            Variable {
                                name: "a".to_string(),
                                variable_type: Type::Number,
                                mutable: false,
                            }
                        ),
                        Mutable::Mutable,
                    )
                ),
                BinaryOperator::Minus,
                Box::from(
                    Expression::Variable(
                        Variable {
                            name: "a".to_string(),
                            variable_type: Type::Number,
                            mutable: false,
                        }
                    )
                ),
            ),
            &mut context,
        ),
        Err(
            vec![
                CompilerError::ContentError(
                    "binary operation '-'".to_string(),
                    vec![
                        CompilerError::ContentError(
                            "variable 'a'".to_string(),
                            vec![
                                CompilerError::BorrowError(
                                    BorrowMarker::Immutable,
                                    BorrowMarker::Mutable,
                                )
                            ],
                        )
                    ],
                )
            ]
        )
    );
}

#[test]
fn t_borrow_check_binary_operation_none_mutable_immutable() {
    let mut context = BorrowContext::new();
    context.add_variable(String::from("a"));

    assert_eq!(
        borrow_check_expression(
            Expression::BinaryOperation(
                Box::from(
                    Expression::Reference(
                        ReferenceTarget::Variable(
                            Variable {
                                name: "a".to_string(),
                                variable_type: Type::Number,
                                mutable: false,
                            }
                        ),
                        Mutable::Mutable,
                    )
                ),
                BinaryOperator::Minus,
                Box::from(
                    Expression::Reference(
                        ReferenceTarget::Variable(
                            Variable {
                                name: "a".to_string(),
                                variable_type: Type::Number,
                                mutable: false,
                            }
                        ),
                        Mutable::Immutable,
                    )
                ),
            ),
            &mut context,
        ),
        Err(
            vec![
                CompilerError::ContentError(
                    "binary operation '-'".to_string(),
                    vec![
                        CompilerError::ContentError(
                            "variable 'a'".to_string(),
                            vec![
                                CompilerError::BorrowError(
                                    BorrowMarker::Mutable,
                                    BorrowMarker::Immutable,
                                )
                            ],
                        )
                    ],
                )
            ]
        )
    );
}

#[test]
fn t_borrow_check_binary_operation_none_immutable_immutable() {
    let mut context = BorrowContext::new();
    context.add_variable(String::from("a"));

    assert_eq!(
        borrow_check_expression(
            Expression::BinaryOperation(
                Box::from(
                    Expression::Reference(
                        ReferenceTarget::Variable(
                            Variable {
                                name: "a".to_string(),
                                variable_type: Type::Number,
                                mutable: false,
                            }
                        ),
                        Mutable::Immutable,
                    )
                ),
                BinaryOperator::Minus,
                Box::from(
                    Expression::Reference(
                        ReferenceTarget::Variable(
                            Variable {
                                name: "a".to_string(),
                                variable_type: Type::Number,
                                mutable: false,
                            }
                        ),
                        Mutable::Immutable,
                    )
                ),
            ),
            &mut context,
        ),
        Ok(BorrowMarker::None)
    );
}

#[test]
fn t_borrow_check_function_call() {
    let mut context = BorrowContext::new();
    context.add_variable(String::from("a"));

    assert_eq!(
        borrow_check_expression(
            Expression::FunctionCall(
                FunctionCall {
                    name: "test".to_string(),
                    arguments: vec![
                        Expression::Reference(
                            ReferenceTarget::Variable(
                                Variable {
                                    name: "a".to_string(),
                                    variable_type: Type::Number,
                                    mutable: false,
                                }
                            ),
                            Mutable::Immutable,
                        ),
                        Expression::Reference(
                            ReferenceTarget::Variable(
                                Variable {
                                    name: "a".to_string(),
                                    variable_type: Type::Number,
                                    mutable: false,
                                }
                            ),
                            Mutable::Immutable,
                        )
                    ]
                }
            ),
            &mut context,
        ),
        Ok(BorrowMarker::None)
    );
}

#[test]
fn t_borrow_check_function_call_immutable_mutable() {
    let mut context = BorrowContext::new();
    context.add_variable(String::from("a"));

    assert_eq!(
        borrow_check_expression(
            Expression::FunctionCall(
                FunctionCall {
                    name: "test".to_string(),
                    arguments: vec![
                        Expression::Reference(
                            ReferenceTarget::Variable(
                                Variable {
                                    name: "a".to_string(),
                                    variable_type: Type::Number,
                                    mutable: false,
                                }
                            ),
                            Mutable::Immutable,
                        ),
                        Expression::Reference(
                            ReferenceTarget::Variable(
                                Variable {
                                    name: "a".to_string(),
                                    variable_type: Type::Number,
                                    mutable: false,
                                }
                            ),
                            Mutable::Mutable,
                        )
                    ]
                }
            ),
            &mut context,
        ),
        Err(
            vec![
                CompilerError::ContentError(
                    "call of function 'test'".to_string(),
                    vec![
                        CompilerError::ContentError(
                            "argument 2 for function 'test'".to_string(),
                            vec![
                                CompilerError::ContentError(
                                    "variable 'a'".to_string(),
                                    vec![
                                        CompilerError::BorrowError(
                                            BorrowMarker::Immutable,
                                            BorrowMarker::Mutable,
                                        )
                                    ]
                                )
                            ]
                        )
                    ]
                )
            ]
        )
    );
}

