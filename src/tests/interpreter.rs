use crate::interpreter::*;
use crate::tokens::*;
use crate::error::*;
use crate::context::*;
use std::collections::HashMap;

const INTERPRETER_FOLDER: &str = "interpreter";

fn test_output(test_function: fn() -> (), expected_output_file: &str) {
    let expected_output_file = [INTERPRETER_FOLDER, expected_output_file].join("\\");
    let expected_output_file = expected_output_file.as_str();
    super::test_output(test_function, expected_output_file);
}

#[test]
fn t_add() {
    let mut context = Context::new();
    assert_eq!(
        evaluate_expression(
            Expression::BinaryOperation(
                Box::from(
                    Expression::Number(1)
                ),
                BinaryOperator::Plus,
                Box::from(
                    Expression::Number(2)
                ),
            ),
            &mut context,
        ),
        Ok(Expression::Number(3))
    )
}

#[test]
fn t_add_variable() {
    let mut context = Context::new();
    let _ = context.add_variable(
        Variable {
            name: "hello_world".to_string(),
            variable_type: Type::Number,
            mutable: false,
        },
        Expression::Number(5),
    );
    let _ = context.add_variable(
        Variable {
            name: "hello_there".to_string(),
            variable_type: Type::Number,
            mutable: false,
        },
        Expression::Number(8),
    );

    assert_eq!(
        evaluate_expression(
            Expression::BinaryOperation(
                Box::from(
                    Expression::Variable(
                        Variable {
                            name: "hello_world".to_string(),
                            variable_type: Type::Number,
                            mutable: false,
                        }
                    )
                ),
                BinaryOperator::Plus,
                Box::from(
                    Expression::Variable(
                        Variable {
                            name: "hello_there".to_string(),
                            variable_type: Type::Number,
                            mutable: false,
                        }
                    )
                ),
            ),
            &mut context,
        ),
        Ok(Expression::Number(13))
    )
}

#[test]
#[should_panic]
fn t_add_variable_panic() {
    let mut context = Context::new();
    let _ = context.add_variable(
        Variable {
            name: "hello_world".to_string(),
            variable_type: Type::Boolean,
            mutable: false,
        },
        Expression::Boolean(true),
    );
    let _ = context.add_variable(
        Variable {
            name: "hello_there".to_string(),
            variable_type: Type::Number,
            mutable: false,
        },
        Expression::Number(8),
    );
    assert_eq!(
        evaluate_expression(
            Expression::BinaryOperation(
                Box::from(
                    Expression::Variable(
                        Variable {
                            name: "hello_world".to_string(),
                            variable_type: Type::Number,
                            mutable: false,
                        }
                    )
                ),
                BinaryOperator::Plus,
                Box::from(
                    Expression::Variable(
                        Variable {
                            name: "hello_there".to_string(),
                            variable_type: Type::Number,
                            mutable: false,
                        }
                    )
                ),
            ),
            &mut context,
        ),
        Ok(Expression::Number(13))
    )
}

#[test]
fn t_min() {
    let mut context = Context::new();
    assert_eq!(
        evaluate_expression(
            Expression::BinaryOperation(
                Box::from(
                    Expression::Number(1)
                ),
                BinaryOperator::Minus,
                Box::from(
                    Expression::Number(2)
                ),
            ),
            &mut context,
        ),
        Ok(Expression::Number(-1))
    )
}

#[test]
fn t_mul() {
    let mut context = Context::new();
    assert_eq!(
        evaluate_expression(
            Expression::BinaryOperation(
                Box::from(
                    Expression::Number(3)
                ),
                BinaryOperator::Times,
                Box::from(
                    Expression::Number(2)
                ),
            ),
            &mut context,
        ),
        Ok(Expression::Number(6))
    )
}

#[test]
fn t_div() {
    let mut context = Context::new();
    assert_eq!(
        evaluate_expression(
            Expression::BinaryOperation(
                Box::from(
                    Expression::Number(4)
                ),
                BinaryOperator::Divide,
                Box::from(
                    Expression::Number(2)
                ),
            ),
            &mut context,
        ),
        Ok(Expression::Number(2))
    )
}

#[test]
fn t_mod() {
    let mut context = Context::new();
    assert_eq!(
        evaluate_expression(
            Expression::BinaryOperation(
                Box::from(
                    Expression::Number(5)
                ),
                BinaryOperator::Modulo,
                Box::from(
                    Expression::Number(2)
                ),
            ),
            &mut context,
        ),
        Ok(Expression::Number(1))
    )
}

#[test]
fn t_true_and_false() {
    let mut context = Context::new();
    assert_eq!(
        evaluate_expression(
            Expression::BinaryOperation(
                Box::from(
                    Expression::Boolean(true)
                ),
                BinaryOperator::And,
                Box::from(
                    Expression::Boolean(false)
                ),
            ),
            &mut context,
        ),
        Ok(Expression::Boolean(false))
    )
}

#[test]
fn t_false_and_false() {
    let mut context = Context::new();
    assert_eq!(
        evaluate_expression(
            Expression::BinaryOperation(
                Box::from(
                    Expression::Boolean(false)
                ),
                BinaryOperator::And,
                Box::from(
                    Expression::Boolean(false)
                ),
            ),
            &mut context,
        ),
        Ok(Expression::Boolean(false))
    )
}

#[test]
fn t_true_and_true() {
    let mut context = Context::new();
    assert_eq!(
        evaluate_expression(
            Expression::BinaryOperation(
                Box::from(
                    Expression::Boolean(true)
                ),
                BinaryOperator::And,
                Box::from(
                    Expression::Boolean(true)
                ),
            ),
            &mut context,
        ),
        Ok(Expression::Boolean(true))
    )
}

#[test]
fn t_true_or_false() {
    let mut context = Context::new();
    assert_eq!(
        evaluate_expression(
            Expression::BinaryOperation(
                Box::from(
                    Expression::Boolean(true)
                ),
                BinaryOperator::Or,
                Box::from(
                    Expression::Boolean(false)
                ),
            ),
            &mut context,
        ),
        Ok(Expression::Boolean(true))
    )
}

#[test]
fn t_false_or_false() {
    let mut context = Context::new();
    assert_eq!(
        evaluate_expression(
            Expression::BinaryOperation(
                Box::from(
                    Expression::Boolean(false)
                ),
                BinaryOperator::Or,
                Box::from(
                    Expression::Boolean(false)
                ),
            ),
            &mut context,
        ),
        Ok(Expression::Boolean(false))
    )
}

#[test]
fn t_true_or_true() {
    let mut context = Context::new();
    assert_eq!(
        evaluate_expression(
            Expression::BinaryOperation(
                Box::from(
                    Expression::Boolean(true)
                ),
                BinaryOperator::Or,
                Box::from(
                    Expression::Boolean(true)
                ),
            ),
            &mut context,
        ),
        Ok(Expression::Boolean(true))
    )
}

#[test]
fn t_eq() {
    let mut context = Context::new();
    assert_eq!(
        evaluate_expression(
            Expression::BinaryOperation(
                Box::from(
                    Expression::Number(1)
                ),
                BinaryOperator::Equal,
                Box::from(
                    Expression::Number(2)
                ),
            ),
            &mut context,
        ),
        Ok(Expression::Boolean(false))
    )
}

#[test]
fn t_pow() {
    let mut context = Context::new();
    assert_eq!(
        evaluate_expression(
            Expression::BinaryOperation(
                Box::from(
                    Expression::Number(3)
                ),
                BinaryOperator::Power,
                Box::from(
                    Expression::Number(2)
                ),
            ),
            &mut context,
        ),
        Ok(Expression::Number(9))
    )
}

#[test]
fn t_none() {
    let mut context = Context::new();
    assert_eq!(
        evaluate_expression(
            Expression::BinaryOperation(
                Box::from(
                    Expression::Number(1)
                ),
                BinaryOperator::None,
                Box::from(
                    Expression::Number(2)
                ),
            ),
            &mut context,
        ),
        Err(CompilerError::NoOperator)
    )
}

#[test]
fn t_neg() {
    let mut context = Context::new();
    assert_eq!(
        evaluate_expression(
            Expression::UnaryOperation(
                UnaryOperator::Minus,
                Box::from(
                    Expression::Number(2)
                ),
            ),
            &mut context,
        ),
        Ok(Expression::Number(-2))
    )
}

#[test]
fn t_not() {
    let mut context = Context::new();
    assert_eq!(
        evaluate_expression(
            Expression::UnaryOperation(
                UnaryOperator::Not,
                Box::from(
                    Expression::Boolean(false)
                ),
            ),
            &mut context,
        ),
        Ok(Expression::Boolean(true))
    )
}

#[test]
fn t_compound_add() {
    let mut context = Context::new();
    assert_eq!(
        evaluate_expression(
            Expression::BinaryOperation(
                Box::from(
                    Expression::BinaryOperation(
                        Box::from(
                            Expression::Number(1)
                        ),
                        BinaryOperator::Plus,
                        Box::from(
                            Expression::Number(2)
                        ),
                    )
                ),
                BinaryOperator::Plus,
                Box::from(
                    Expression::BinaryOperation(
                        Box::from(
                            Expression::Number(1)
                        ),
                        BinaryOperator::Plus,
                        Box::from(
                            Expression::Number(2)
                        ),
                    )
                ),
            ),
            &mut context,
        ),
        Ok(Expression::Number(6))
    )
}

#[test]
fn t_println() {
    test_output(
        || {
            let mut context = Context::new();
            assert_eq!(
                evaluate_function_call(
                    FunctionCall {
                        name: "println".to_string(),
                        arguments: vec![
                            Expression::BinaryOperation(
                                Box::from(
                                    Expression::Number(1)
                                ),
                                BinaryOperator::Plus,
                                Box::from(
                                    Expression::Number(23)
                                ),
                            )
                        ],
                    },
                    &mut context,
                ),
                Ok(Expression::None)
            )
        },
        "t_println",
    )
}

#[test]
fn t_declaration() {
    let mut context = Context::new();
    assert_eq!(
        evaluate_statement(
            Statement::Declaration(
                Variable {
                    name: "hello_there".to_string(),
                    variable_type: Type::Number,
                    mutable: false,
                },
                Expression::UnaryOperation(
                    UnaryOperator::Minus,
                    Box::from(Expression::Number(42)),
                ),
            ),
            &mut context,
        ),
        Ok(Expression::None)
    );

    let mut expected_context = Context::new();
    let _ = expected_context.add_variable(
        Variable {
            name: "hello_there".to_string(),
            variable_type: Type::Number,
            mutable: false,
        },
        Expression::Number(-42),
    );

    assert_eq!(
        context,
        expected_context
    );
}

#[test]
fn t_assignment() {
    let mut context = Context::new();
    let _ = context.add_variable(
        Variable {
            name: "hello_there".to_string(),
            variable_type: Type::Number,
            mutable: true,
        },
        Expression::Number(12),
    );

    assert_eq!(
        evaluate_statement(
            Statement::Assignment(
                LeftHandSide::Variable(
                    Variable {
                        name: "hello_there".to_string(),
                        variable_type: Type::Number,
                        mutable: true,
                    }
                ),
                Expression::UnaryOperation(
                    UnaryOperator::Minus,
                    Box::from(Expression::Number(42)),
                ),
            ),
            &mut context,
        ),
        Ok(Expression::None)
    );

    let mut expected_context = Context::new();
    let _ = expected_context.add_variable(
        Variable {
            name: "hello_there".to_string(),
            variable_type: Type::Number,
            mutable: true,
        },
        Expression::Number(-42),
    );

    assert_eq!(
        context,
        expected_context
    )
}

#[test]
fn t_expression_as_statement() {
    let mut context = Context::new();
    assert_eq!(
        evaluate_statement(
            Statement::Expression(
                Expression::BinaryOperation(
                    Box::from(
                        Expression::Number(1)
                    ),
                    BinaryOperator::Plus,
                    Box::from(
                        Expression::Number(2)
                    ),
                )
            ),
            &mut context,
        ),
        Ok(Expression::Number(3))
    )
}

#[test]
fn t_non_returning_block() {
    test_output(
        || {
            let mut context = Context::new();
            assert_eq!(
                evaluate_block(
                    Block::NonReturning(
                        vec![
                            Statement::Declaration(
                                Variable {
                                    name: "hello_there".to_string(),
                                    variable_type: Type::Number,
                                    mutable: true,
                                },
                                Expression::UnaryOperation(
                                    UnaryOperator::Minus,
                                    Box::from(Expression::Number(42)),
                                ),
                            ),
                            Statement::Assignment(
                                LeftHandSide::Variable(
                                    Variable {
                                        name: "hello_there".to_string(),
                                        variable_type: Type::Number,
                                        mutable: false,
                                    }
                                ),
                                Expression::UnaryOperation(
                                    UnaryOperator::Minus,
                                    Box::from(Expression::Number(33)),
                                ),
                            ),
                            Statement::Declaration(
                                Variable {
                                    name: "toto".to_string(),
                                    variable_type: Type::Boolean,
                                    mutable: false,
                                },
                                Expression::UnaryOperation(
                                    UnaryOperator::Not,
                                    Box::from(Expression::Boolean(true)),
                                ),
                            ),
                            Statement::Expression(
                                Expression::FunctionCall(
                                    FunctionCall {
                                        name: "println".to_string(),
                                        arguments: vec![
                                            Expression::BinaryOperation(
                                                Box::from(
                                                    Expression::Variable(
                                                        Variable {
                                                            name: "hello_there".to_string(),
                                                            variable_type: Type::Number,
                                                            mutable: false,
                                                        }
                                                    )
                                                ),
                                                BinaryOperator::Plus,
                                                Box::from(
                                                    Expression::Number(23)
                                                ),
                                            )
                                        ],
                                    }
                                )
                            )
                        ]
                    ),
                    &mut context,
                    true,
                ),
                Ok(Expression::None)
            )
        },
        "t_non_returning_block",
    );
}

#[test]
fn t_returning_block() {
    let mut context = Context::new();
    let _ = context.add_variable(
        Variable {
            name: "global".to_string(),
            variable_type: Type::Number,
            mutable: false,
        },
        Expression::Number(5),
    );

    assert_eq!(
        evaluate_block(
            Block::Returning(
                vec![
                    Statement::Declaration(
                        Variable {
                            name: "hello_there".to_string(),
                            variable_type: Type::Number,
                            mutable: true,
                        },
                        Expression::UnaryOperation(
                            UnaryOperator::Minus,
                            Box::from(Expression::Number(42)),
                        ),
                    ),
                    Statement::Assignment(
                        LeftHandSide::Variable(
                            Variable {
                                name: "hello_there".to_string(),
                                variable_type: Type::Number,
                                mutable: false,
                            }
                        ),
                        Expression::UnaryOperation(
                            UnaryOperator::Minus,
                            Box::from(Expression::Number(33)),
                        ),
                    ),
                    Statement::Declaration(
                        Variable {
                            name: "toto".to_string(),
                            variable_type: Type::Boolean,
                            mutable: false,
                        },
                        Expression::UnaryOperation(
                            UnaryOperator::Not,
                            Box::from(Expression::Boolean(true)),
                        ),
                    )
                ],
                Statement::Expression(
                    Expression::BinaryOperation(
                        Box::from(
                            Expression::Variable(
                                Variable {
                                    name: "hello_there".to_string(),
                                    variable_type: Type::Number,
                                    mutable: false,
                                }
                            )
                        ),
                        BinaryOperator::Plus,
                        Box::from(
                            Expression::Variable(
                                Variable {
                                    name: "global".to_string(),
                                    variable_type: Type::Number,
                                    mutable: false,
                                }
                            )
                        ),
                    )
                ),
            ),
            &mut context,
            true,
        ),
        Ok(Expression::Number(-28))
    )
}

#[test]
fn t_if_true() {
    let mut context = Context::new();
    assert_eq!(
        evaluate_if(
            If {
                condition: Expression::BinaryOperation(
                    Box::from(Expression::BinaryOperation(
                        Box::from(Expression::Number(6)),
                        BinaryOperator::Times,
                        Box::from(Expression::Number(7)),
                    )),
                    BinaryOperator::Equal,
                    Box::from(
                        Expression::Number(42)
                    ),
                ),
                true_instructions: Block::Returning(
                    vec![
                        Statement::Declaration(
                            Variable {
                                name: "hello_there".to_string(),
                                variable_type: Type::Number,
                                mutable: false,
                            },
                            Expression::UnaryOperation(
                                UnaryOperator::Minus,
                                Box::from(Expression::Number(42)),
                            ),
                        ),
                        Statement::Declaration(
                            Variable {
                                name: "hello_world".to_string(),
                                variable_type: Type::Number,
                                mutable: false,
                            },
                            Expression::UnaryOperation(
                                UnaryOperator::Minus,
                                Box::from(Expression::Number(33)),
                            ),
                        ),
                    ],
                    Statement::Expression(
                        Expression::BinaryOperation(
                            Box::from(
                                Expression::Variable(
                                    Variable {
                                        name: "hello_there".to_string(),
                                        variable_type: Type::Number,
                                        mutable: false,
                                    }
                                )
                            ),
                            BinaryOperator::Minus,
                            Box::from(
                                Expression::Variable(
                                    Variable {
                                        name: "hello_world".to_string(),
                                        variable_type: Type::Number,
                                        mutable: false,
                                    }
                                )
                            ),
                        )
                    ),
                ),
                false_instructions: None,
            },
            &mut context,
        ),
        Ok(Expression::Number(-9))
    )
}

#[test]
fn t_if_false_no_else() {
    let mut context = Context::new();
    assert_eq!(
        evaluate_if(
            If {
                condition: Expression::BinaryOperation(
                    Box::from(Expression::BinaryOperation(
                        Box::from(Expression::Number(6)),
                        BinaryOperator::Divide,
                        Box::from(Expression::Number(7)),
                    )),
                    BinaryOperator::Equal,
                    Box::from(
                        Expression::Number(42)
                    ),
                ),
                true_instructions: Block::Returning(
                    vec![
                        Statement::Declaration(
                            Variable {
                                name: "hello_there".to_string(),
                                variable_type: Type::Number,
                                mutable: false,
                            },
                            Expression::UnaryOperation(
                                UnaryOperator::Minus,
                                Box::from(Expression::Number(42)),
                            ),
                        ),
                        Statement::Declaration(
                            Variable {
                                name: "hello_world".to_string(),
                                variable_type: Type::Number,
                                mutable: false,
                            },
                            Expression::UnaryOperation(
                                UnaryOperator::Minus,
                                Box::from(Expression::Number(33)),
                            ),
                        ),
                    ],
                    Statement::Expression(
                        Expression::BinaryOperation(
                            Box::from(
                                Expression::Variable(
                                    Variable {
                                        name: "hello_there".to_string(),
                                        variable_type: Type::Number,
                                        mutable: false,
                                    }
                                )
                            ),
                            BinaryOperator::Minus,
                            Box::from(
                                Expression::Variable(
                                    Variable {
                                        name: "hello_world".to_string(),
                                        variable_type: Type::Number,
                                        mutable: false,
                                    }
                                )
                            ),
                        )
                    ),
                ),
                false_instructions: None,
            },
            &mut context,
        ),
        Ok(Expression::None)
    )
}

#[test]
fn t_if_false_with_else() {
    let mut context = Context::new();
    assert_eq!(
        evaluate_if(
            If {
                condition: Expression::BinaryOperation(
                    Box::from(Expression::BinaryOperation(
                        Box::from(Expression::Number(6)),
                        BinaryOperator::Divide,
                        Box::from(Expression::Number(7)),
                    )),
                    BinaryOperator::Equal,
                    Box::from(
                        Expression::Number(42)
                    ),
                ),
                true_instructions: Block::Returning(
                    vec![
                        Statement::Declaration(
                            Variable {
                                name: "hello_there".to_string(),
                                variable_type: Type::Number,
                                mutable: false,
                            },
                            Expression::UnaryOperation(
                                UnaryOperator::Minus,
                                Box::from(Expression::Number(42)),
                            ),
                        ),
                        Statement::Declaration(
                            Variable {
                                name: "hello_world".to_string(),
                                variable_type: Type::Number,
                                mutable: false,
                            },
                            Expression::UnaryOperation(
                                UnaryOperator::Minus,
                                Box::from(Expression::Number(33)),
                            ),
                        ),
                    ],
                    Statement::Expression(
                        Expression::BinaryOperation(
                            Box::from(
                                Expression::Variable(
                                    Variable {
                                        name: "hello_there".to_string(),
                                        variable_type: Type::Number,
                                        mutable: false,
                                    }
                                )
                            ),
                            BinaryOperator::Minus,
                            Box::from(
                                Expression::Variable(
                                    Variable {
                                        name: "hello_world".to_string(),
                                        variable_type: Type::Number,
                                        mutable: false,
                                    }
                                )
                            ),
                        )
                    ),
                ),
                false_instructions: Some(
                    Block::Returning(
                        vec![
                            Statement::Declaration(
                                Variable {
                                    name: "hello_there".to_string(),
                                    variable_type: Type::Number,
                                    mutable: false,
                                },
                                Expression::Number(42),
                            ),
                            Statement::Declaration(
                                Variable {
                                    name: "hello_world".to_string(),
                                    variable_type: Type::Number,
                                    mutable: false,
                                },
                                Expression::UnaryOperation(
                                    UnaryOperator::Minus,
                                    Box::from(Expression::Number(33)),
                                ),
                            ),
                        ],
                        Statement::Expression(
                            Expression::BinaryOperation(
                                Box::from(
                                    Expression::Variable(
                                        Variable {
                                            name: "hello_there".to_string(),
                                            variable_type: Type::Number,
                                            mutable: false,
                                        }
                                    )
                                ),
                                BinaryOperator::Minus,
                                Box::from(
                                    Expression::Variable(
                                        Variable {
                                            name: "hello_world".to_string(),
                                            variable_type: Type::Number,
                                            mutable: false,
                                        }
                                    )
                                ),
                            )
                        ),
                    )
                ),
            },
            &mut context,
        ),
        Ok(Expression::Number(75))
    )
}

#[test]
fn t_while() {
    test_output(
        || {
            assert_eq!(
                evaluate_statement(
                    Statement::While(
                        Box::from(
                            While {
                                condition: Expression::BinaryOperation(
                                    Box::from(
                                        Expression::Variable(
                                            Variable {
                                                name: "counter".to_string(),
                                                variable_type: Type::Number,
                                                mutable: true,
                                            }
                                        )
                                    ),
                                    BinaryOperator::LessThan,
                                    Box::from(
                                        Expression::Number(
                                            10
                                        )
                                    ),
                                ),
                                instructions: Block::NonReturning(
                                    vec![
                                        Statement::Expression(
                                            Expression::FunctionCall(
                                                FunctionCall {
                                                    name: "println".to_string(),
                                                    arguments: vec![
                                                        Expression::Variable(
                                                            Variable {
                                                                name: "counter".to_string(),
                                                                variable_type: Type::Number,
                                                                mutable: true,
                                                            }
                                                        )
                                                    ],
                                                }
                                            )
                                        ),
                                        Statement::Assignment(
                                            LeftHandSide::Variable(
                                                Variable {
                                                    name: "counter".to_string(),
                                                    variable_type: Type::Number,
                                                    mutable: true,
                                                }
                                            ),
                                            Expression::BinaryOperation(
                                                Box::from(
                                                    Expression::Variable(
                                                        Variable {
                                                            name: "counter".to_string(),
                                                            variable_type: Type::Number,
                                                            mutable: true,
                                                        }
                                                    )
                                                ),
                                                BinaryOperator::Plus,
                                                Box::from(
                                                    Expression::Variable(
                                                        Variable {
                                                            name: "counter".to_string(),
                                                            variable_type: Type::Number,
                                                            mutable: true,
                                                        }
                                                    )
                                                ),
                                            ),
                                        )
                                    ]
                                ),
                            }
                        )
                    ),
                    &mut {
                        let mut context = Context::new();
                        let _ = context.add_variable(
                            Variable {
                                name: "counter".to_string(),
                                variable_type: Type::Number,
                                mutable: true,
                            },
                            Expression::Number(1),
                        );
                        context
                    },
                ),
                Ok(Expression::None)
            )
        },
        "t_while",
    )
}