use crate::{
    tokens::*,
    type_checker::*,
    error::*,
};

// region Terminals
#[test]
fn t_check_number() {
    assert_eq!(
        check_expression(
            Expression::get_number(),
            TypeContext::new(),
        ),
        Ok(
            Type::Number
        )
    )
}

#[test]
fn t_check_boolean() {
    assert_eq!(
        check_expression(
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Ok(
            Type::Boolean
        )
    )
}

#[test]
fn t_check_variable() {
    let mut context = TypeContext::new();
    context.add_variable(
        Variable {
            name: "toto".to_string(),
            variable_type: Type::Number,
            mutable: true,
        }
    );
    assert_eq!(
        check_expression(
            Expression::Variable(
                Variable {
                    name: "toto".to_string(),
                    variable_type: Type::Number,
                    mutable: false,
                }
            ),
            context,
        ),
        Ok(
            Type::Number
        )
    )
}

#[test]
fn t_check_variable_not_found() {
    assert_eq!(
        check_expression(
            Expression::Variable(
                Variable {
                    name: "toto".to_string(),
                    variable_type: Type::Number,
                    mutable: false,
                }
            ),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::ContentError(
                    String::from("variable 'toto'"),
                    vec![
                        CompilerError::NotFound
                    ],
                )
            ]
        )
    )
}
// endregion

// region Operators
#[test]
fn t_check_minus_number() {
    assert_eq!(
        check_unary_operation(
            UnaryOperator::Minus,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Ok(
            Type::Number
        )
    )
}

#[test]
fn t_check_minus_boolean() {
    assert_eq!(
        check_unary_operation(
            UnaryOperator::Minus,
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::Number]),
                    vec![Type::Boolean],
                    String::from("unary operation '-'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_minus_minus_number() {
    assert_eq!(
        check_unary_operation(
            UnaryOperator::Minus,
            Expression::UnaryOperation(
                UnaryOperator::Minus,
                Box::from(Expression::get_number()),
            ),
            TypeContext::new(),
        ),
        Ok(
            Type::Number
        )
    )
}

#[test]
fn t_check_not_number() {
    assert_eq!(
        check_unary_operation(
            UnaryOperator::Not,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::Boolean]),
                    vec![Type::Number],
                    String::from("unary operation '!'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_not_boolean() {
    assert_eq!(
        check_unary_operation(
            UnaryOperator::Not,
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Ok(
            Type::Boolean
        )
    )
}

#[test]
fn t_check_number_plus_number() {
    assert_eq!(
        check_binary_operation(
            Expression::get_number(),
            BinaryOperator::Minus,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Ok(
            Type::Number
        )
    )
}

#[test]
fn t_check_number_plus_boolean() {
    assert_eq!(
        check_binary_operation(
            Expression::get_number(),
            BinaryOperator::Plus,
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::get_tuple_number_number()]),
                    vec![
                        Type::get_tuple_number_boolean()
                    ],
                    String::from("binary operation '+'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_boolean_plus_boolean() {
    assert_eq!(
        check_binary_operation(
            Expression::get_boolean(),
            BinaryOperator::Plus,
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::get_tuple_number_number()]),
                    vec![
                        Type::get_tuple_boolean_boolean()
                    ],
                    String::from("binary operation '+'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_boolean_plus_number() {
    assert_eq!(
        check_binary_operation(
            Expression::get_boolean(),
            BinaryOperator::Plus,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::get_tuple_number_number()]),
                    vec![
                        Type::get_tuple_boolean_number()
                    ],
                    String::from("binary operation '+'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_number_minus_number() {
    assert_eq!(
        check_binary_operation(
            Expression::get_number(),
            BinaryOperator::Minus,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Ok(
            Type::Number
        )
    )
}

#[test]
fn t_check_number_minus_boolean() {
    assert_eq!(
        check_binary_operation(
            Expression::get_number(),
            BinaryOperator::Minus,
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::get_tuple_number_number()]),
                    vec![
                        Type::get_tuple_number_boolean()
                    ],
                    String::from("binary operation '-'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_boolean_minus_boolean() {
    assert_eq!(
        check_binary_operation(
            Expression::get_boolean(),
            BinaryOperator::Minus,
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::get_tuple_number_number()]),
                    vec![
                        Type::get_tuple_boolean_boolean()
                    ],
                    String::from("binary operation '-'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_boolean_minus_number() {
    assert_eq!(
        check_binary_operation(
            Expression::get_boolean(),
            BinaryOperator::Minus,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::get_tuple_number_number()]),
                    vec![
                        Type::get_tuple_boolean_number()
                    ],
                    String::from("binary operation '-'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_number_times_number() {
    assert_eq!(
        check_binary_operation(
            Expression::get_number(),
            BinaryOperator::Times,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Ok(
            Type::Number
        )
    )
}

#[test]
fn t_check_number_times_boolean() {
    assert_eq!(
        check_binary_operation(
            Expression::get_number(),
            BinaryOperator::Times,
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::get_tuple_number_number()]),
                    vec![
                        Type::get_tuple_number_boolean()
                    ],
                    String::from("binary operation '*'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_boolean_times_boolean() {
    assert_eq!(
        check_binary_operation(
            Expression::get_boolean(),
            BinaryOperator::Times,
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::get_tuple_number_number()]),
                    vec![
                        Type::get_tuple_boolean_boolean()
                    ],
                    String::from("binary operation '*'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_boolean_times_number() {
    assert_eq!(
        check_binary_operation(
            Expression::get_boolean(),
            BinaryOperator::Times,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::get_tuple_number_number()]),
                    vec![
                        Type::get_tuple_boolean_number()
                    ],
                    String::from("binary operation '*'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_number_divide_number() {
    assert_eq!(
        check_binary_operation(
            Expression::get_number(),
            BinaryOperator::Divide,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Ok(
            Type::Number
        )
    )
}

#[test]
fn t_check_number_divide_boolean() {
    assert_eq!(
        check_binary_operation(
            Expression::get_number(),
            BinaryOperator::Divide,
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::get_tuple_number_number()]),
                    vec![
                        Type::get_tuple_number_boolean()
                    ],
                    String::from("binary operation '/'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_boolean_divide_boolean() {
    assert_eq!(
        check_binary_operation(
            Expression::get_boolean(),
            BinaryOperator::Divide,
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::get_tuple_number_number()]),
                    vec![
                        Type::get_tuple_boolean_boolean()
                    ],
                    String::from("binary operation '/'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_boolean_divide_number() {
    assert_eq!(
        check_binary_operation(
            Expression::get_boolean(),
            BinaryOperator::Divide,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::get_tuple_number_number()]),
                    vec![
                        Type::get_tuple_boolean_number()
                    ],
                    String::from("binary operation '/'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_boolean_and_boolean() {
    assert_eq!(
        check_binary_operation(
            Expression::get_boolean(),
            BinaryOperator::And,
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Ok(
            Type::Boolean
        )
    )
}

#[test]
fn t_check_number_and_boolean() {
    assert_eq!(
        check_binary_operation(
            Expression::get_number(),
            BinaryOperator::And,
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::get_tuple_boolean_boolean()]),
                    vec![
                        Type::get_tuple_number_boolean()
                    ],
                    String::from("binary operation '&&'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_number_and_number() {
    assert_eq!(
        check_binary_operation(
            Expression::get_number(),
            BinaryOperator::And,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::get_tuple_boolean_boolean()]),
                    vec![
                        Type::get_tuple_number_number()
                    ],
                    String::from("binary operation '&&'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_boolean_and_number() {
    assert_eq!(
        check_binary_operation(
            Expression::get_boolean(),
            BinaryOperator::And,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::get_tuple_boolean_boolean()]),
                    vec![
                        Type::get_tuple_boolean_number()
                    ],
                    String::from("binary operation '&&'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_boolean_or_boolean() {
    assert_eq!(
        check_binary_operation(
            Expression::get_boolean(),
            BinaryOperator::Or,
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Ok(
            Type::Boolean
        )
    )
}

#[test]
fn t_check_number_or_boolean() {
    assert_eq!(
        check_binary_operation(
            Expression::get_number(),
            BinaryOperator::Or,
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::get_tuple_boolean_boolean()]),
                    vec![
                        Type::get_tuple_number_boolean()
                    ],
                    String::from("binary operation '||'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_number_or_number() {
    assert_eq!(
        check_binary_operation(
            Expression::get_number(),
            BinaryOperator::Or,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::get_tuple_boolean_boolean()]),
                    vec![
                        Type::get_tuple_number_number()
                    ],
                    String::from("binary operation '||'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_boolean_or_number() {
    assert_eq!(
        check_binary_operation(
            Expression::get_boolean(),
            BinaryOperator::Or,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::get_tuple_boolean_boolean()]),
                    vec![
                        Type::get_tuple_boolean_number()
                    ],
                    String::from("binary operation '||'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_number_equal_number() {
    assert_eq!(
        check_binary_operation(
            Expression::get_number(),
            BinaryOperator::Equal,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Ok(
            Type::Boolean
        )
    )
}

#[test]
fn t_check_number_equal_boolean() {
    assert_eq!(
        check_binary_operation(
            Expression::get_number(),
            BinaryOperator::Equal,
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[
                        Type::get_tuple_number_number(),
                        Type::get_tuple_boolean_boolean()
                    ]),
                    vec![
                        Type::get_tuple_number_boolean()
                    ],
                    String::from("binary operation '=='"),
                )
            ]
        )
    )
}

#[test]
fn t_check_boolean_equal_number() {
    assert_eq!(
        check_binary_operation(
            Expression::get_boolean(),
            BinaryOperator::Equal,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[
                        Type::get_tuple_number_number(),
                        Type::get_tuple_boolean_boolean()
                    ]),
                    vec![
                        Type::get_tuple_boolean_number()
                    ],
                    String::from("binary operation '=='"),
                )
            ]
        )
    )
}

#[test]
fn t_check_boolean_equal_boolean() {
    assert_eq!(
        check_binary_operation(
            Expression::get_boolean(),
            BinaryOperator::Equal,
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Ok(
            Type::Boolean
        )
    )
}

#[test]
fn t_check_number_less_than_number() {
    assert_eq!(
        check_binary_operation(
            Expression::get_number(),
            BinaryOperator::LessThan,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Ok(
            Type::Boolean
        )
    )
}

#[test]
fn t_check_number_less_than_boolean() {
    assert_eq!(
        check_binary_operation(
            Expression::get_number(),
            BinaryOperator::LessThan,
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::get_tuple_number_number()]),
                    vec![
                        Type::get_tuple_number_boolean()
                    ],
                    String::from("binary operation '<'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_boolean_less_than_boolean() {
    assert_eq!(
        check_binary_operation(
            Expression::get_boolean(),
            BinaryOperator::LessThan,
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::get_tuple_number_number()]),
                    vec![
                        Type::get_tuple_boolean_boolean()
                    ],
                    String::from("binary operation '<'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_boolean_less_than_number() {
    assert_eq!(
        check_binary_operation(
            Expression::get_boolean(),
            BinaryOperator::LessThan,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::get_tuple_number_number()]),
                    vec![
                        Type::get_tuple_boolean_number()
                    ],
                    String::from("binary operation '<'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_number_different_number() {
    assert_eq!(
        check_binary_operation(
            Expression::get_number(),
            BinaryOperator::Different,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Ok(
            Type::Boolean
        )
    )
}

#[test]
fn t_check_boolean_different_boolean() {
    assert_eq!(
        check_binary_operation(
            Expression::get_boolean(),
            BinaryOperator::Different,
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Ok(
            Type::Boolean
        )
    )
}

#[test]
fn t_check_number_different_boolean() {
    assert_eq!(
        check_binary_operation(
            Expression::get_number(),
            BinaryOperator::Different,
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[
                        Type::get_tuple_number_number(),
                        Type::get_tuple_boolean_boolean()
                    ]),
                    vec![
                        Type::get_tuple_number_boolean()
                    ],
                    String::from("binary operation '!='"),
                )
            ]
        )
    )
}

#[test]
fn t_check_boolean_different_number() {
    assert_eq!(
        check_binary_operation(
            Expression::get_boolean(),
            BinaryOperator::Different,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[
                        Type::get_tuple_number_number(),
                        Type::get_tuple_boolean_boolean()
                    ]),
                    vec![
                        Type::get_tuple_boolean_number()
                    ],
                    String::from("binary operation '!='"),
                )
            ]
        )
    )
}

#[test]
fn t_check_number_power_number() {
    assert_eq!(
        check_binary_operation(
            Expression::get_number(),
            BinaryOperator::Power,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Ok(
            Type::Number
        )
    )
}

#[test]
fn t_check_number_power_boolean() {
    assert_eq!(
        check_binary_operation(
            Expression::get_number(),
            BinaryOperator::Power,
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::get_tuple_number_number()]),
                    vec![
                        Type::get_tuple_number_boolean()
                    ],
                    String::from("binary operation '^'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_boolean_power_boolean() {
    assert_eq!(
        check_binary_operation(
            Expression::get_boolean(),
            BinaryOperator::Power,
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::get_tuple_number_number()]),
                    vec![
                        Type::get_tuple_boolean_boolean()
                    ],
                    String::from("binary operation '^'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_boolean_power_number() {
    assert_eq!(
        check_binary_operation(
            Expression::get_boolean(),
            BinaryOperator::Power,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::get_tuple_number_number()]),
                    vec![
                        Type::get_tuple_boolean_number()
                    ],
                    String::from("binary operation '^'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_number_less_than_number_different_number_less_than_number() {
    assert_eq!(
        check_binary_operation(
            Expression::BinaryOperation(
                Box::from(Expression::get_number()),
                BinaryOperator::LessThan,
                Box::from(Expression::get_number()),
            ),
            BinaryOperator::Different,
            Expression::BinaryOperation(
                Box::from(Expression::get_number()),
                BinaryOperator::LessThan,
                Box::from(Expression::get_number()),
            ),
            TypeContext::new(),
        ),
        Ok(
            Type::Boolean
        )
    )
}

// endregion

// region Blocks
#[test]
fn t_check_non_returning_block() {
    assert_eq!(
        check_block(
            Block::NonReturning(
                vec![
                    Statement::Expression(
                        Expression::get_number()
                    )
                ]
            ),
            TypeContext::new(),
        ),
        Ok(
            Type::None
        )
    )
}

#[test]
fn t_check_non_returning_block_with_errors() {
    assert_eq!(
        check_block(
            Block::NonReturning(
                vec![
                    Statement::Expression(
                        Expression::BinaryOperation(
                            Box::from(Expression::get_number()),
                            BinaryOperator::Plus,
                            Box::from(Expression::get_boolean()),
                        )
                    ),
                    Statement::Expression(
                        Expression::BinaryOperation(
                            Box::from(Expression::get_number()),
                            BinaryOperator::And,
                            Box::from(Expression::get_boolean()),
                        )
                    )
                ]
            ),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::ContentError(
                    String::from("block content"),
                    vec![
                        CompilerError::BadTypes(
                            to_set(&[
                                Type::get_tuple_number_number()
                            ]),
                            vec![
                                Type::get_tuple_number_boolean()
                            ],
                            String::from("binary operation '+'"),
                        ),
                        CompilerError::BadTypes(
                            to_set(&[
                                Type::get_tuple_boolean_boolean()
                            ]),
                            vec![
                                Type::get_tuple_number_boolean()
                            ],
                            String::from("binary operation '&&'"),
                        )
                    ],
                )
            ]
        )
    )
}

#[test]
fn t_check_returning_block() {
    assert_eq!(
        check_block(
            Block::Returning(
                vec![
                    Statement::Expression(
                        Expression::get_number()
                    )
                ],
                Statement::Expression(
                    Expression::get_number()
                ),
            ),
            TypeContext::new(),
        ),
        Ok(
            Type::Number
        )
    )
}

#[test]
fn t_check_block_with_declaration() {
    assert_eq!(
        check_block(
            Block::NonReturning(
                vec![
                    Statement::Declaration(
                        Variable {
                            name: "toto".to_string(),
                            variable_type: Type::Number,
                            mutable: true,
                        },
                        Expression::get_number(),
                    ),
                    Statement::Assignment(
                        LeftHandSide::Variable(
                            Variable {
                                name: "toto".to_string(),
                                variable_type: Type::Number,
                                mutable: true,
                            }
                        ),
                        Expression::get_number(),
                    )
                ]
            ),
            TypeContext::new(),
        ),
        Ok(
            Type::None
        )
    )
}

#[test]
fn t_check_block_with_declaration_bad_assignment() {
    assert_eq!(
        check_block(
            Block::NonReturning(
                vec![
                    Statement::Declaration(
                        Variable {
                            name: "toto".to_string(),
                            variable_type: Type::Number,
                            mutable: true,
                        },
                        Expression::get_number(),
                    ),
                    Statement::Assignment(
                        LeftHandSide::Variable(
                            Variable {
                                name: "toto".to_string(),
                                variable_type: Type::Number,
                                mutable: true,
                            }
                        ),
                        Expression::get_boolean(),
                    )
                ]
            ),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::ContentError(
                    String::from("block content"),
                    vec![
                        CompilerError::BadTypes(
                            to_set(&[Type::Number]),
                            vec![Type::Boolean],
                            String::from("assignment of variable 'toto'"),
                        )
                    ],
                )
            ]
        )
    )
}
// endregion

// region Declarations
#[test]
fn t_check_declaration_number_number() {
    assert_eq!(
        check_declaration(
            Variable {
                name: "toto".to_string(),
                variable_type: Type::Number,
                mutable: false,
            }
            ,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Ok(
            Type::None
        )
    )
}

#[test]
fn t_check_declaration_boolean_boolean() {
    assert_eq!(
        check_declaration(
            Variable {
                name: "toto".to_string(),
                variable_type: Type::Boolean,
                mutable: false,
            }
            ,
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Ok(
            Type::None
        )
    )
}

#[test]
fn t_check_declaration_number_boolean() {
    assert_eq!(
        check_declaration(
            Variable {
                name: "toto".to_string(),
                variable_type: Type::Number,
                mutable: false,
            }
            ,
            Expression::get_boolean(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::Number]),
                    vec![Type::Boolean],
                    String::from("declaration of variable 'toto'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_declaration_boolean_number() {
    assert_eq!(
        check_declaration(
            Variable {
                name: "toto".to_string(),
                variable_type: Type::Boolean,
                mutable: false,
            }
            ,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::Boolean]),
                    vec![Type::Number],
                    String::from("declaration of variable 'toto'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_declaration_no_type_number() {
    assert_eq!(
        check_declaration(
            Variable {
                name: "toto".to_string(),
                variable_type: Type::Unknown,
                mutable: false,
            }
            ,
            Expression::get_number(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::Unknown]),
                    vec![Type::Number],
                    String::from("declaration of variable 'toto'"),
                )
            ]
        )
    )
}
// endregion

// region Assignment
#[test]
fn t_check_assignment_number_number() {
    let mut context = TypeContext::new();
    context.add_variable(
        Variable {
            name: "toto".to_string(),
            variable_type: Type::Number,
            mutable: true,
        }
    );

    assert_eq!(
        check_assignment(
            LeftHandSide::Variable(
                Variable {
                    name: "toto".to_string(),
                    variable_type: Type::Number,
                    mutable: true,
                }
            ),
            Expression::get_number(),
            context,
        ),
        Ok(
            Type::None
        )
    )
}

#[test]
fn t_check_assignment_boolean_boolean() {
    let mut context = TypeContext::new();
    context.add_variable(
        Variable {
            name: "toto".to_string(),
            variable_type: Type::Boolean,
            mutable: true,
        }
    );

    assert_eq!(
        check_assignment(
            LeftHandSide::Variable(
                Variable {
                    name: "toto".to_string(),
                    variable_type: Type::Boolean,
                    mutable: false,
                }
            ),
            Expression::get_boolean(),
            context,
        ),
        Ok(
            Type::None
        )
    )
}

#[test]
fn t_check_assignment_number_boolean() {
    let mut context = TypeContext::new();
    context.add_variable(
        Variable {
            name: "toto".to_string(),
            variable_type: Type::Number,
            mutable: true,
        }
    );

    assert_eq!(
        check_assignment(
            LeftHandSide::Variable(
                Variable {
                    name: "toto".to_string(),
                    variable_type: Type::Number,
                    mutable: false,
                }
            ),
            Expression::get_boolean(),
            context,
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::Number]),
                    vec![Type::Boolean],
                    String::from("assignment of variable 'toto'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_assignment_boolean_number() {
    let mut context = TypeContext::new();
    context.add_variable(
        Variable {
            name: "toto".to_string(),
            variable_type: Type::Boolean,
            mutable: true,
        }
    );

    assert_eq!(
        check_assignment(
            LeftHandSide::Variable(
                Variable {
                    name: "toto".to_string(),
                    variable_type: Type::Boolean,
                    mutable: false,
                }
            ),
            Expression::get_number(),
            context,
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::Boolean]),
                    vec![Type::Number],
                    String::from("assignment of variable 'toto'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_assignment_no_type_number() {
    let mut context = TypeContext::new();
    context.add_variable(
        Variable {
            name: "toto".to_string(),
            variable_type: Type::Unknown,
            mutable: true,
        }
    );

    assert_eq!(
        check_assignment(
            LeftHandSide::Variable(
                Variable {
                    name: "toto".to_string(),
                    variable_type: Type::Unknown,
                    mutable: false,
                }
            ),
            Expression::get_number(),
            context,
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::Unknown]),
                    vec![Type::Number],
                    String::from("assignment of variable 'toto'"),
                )
            ]
        )
    )
}

#[test]
fn t_check_assignment_not_found() {
    assert_eq!(
        check_assignment(
            LeftHandSide::Variable(
                Variable {
                    name: "toto".to_string(),
                    variable_type: Type::Unknown,
                    mutable: false,
                }
            ),
            Expression::get_number(),
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::ContentError(
                    String::from("assignment of variable 'toto'"),
                    vec![
                        CompilerError::NotFound
                    ],
                )
            ]
        )
    )
}

#[test]
fn t_check_assignment_to_dereference() {
    let mut context = TypeContext::new();
    context.add_variable(
        Variable {
            name: "toto".to_string(),
            variable_type: Type::Reference(
                Box::from(Type::Number),
                Mutable::Mutable
            ),
            mutable: true,
        }
    );
    assert_eq!(
        check_assignment(
            LeftHandSide::Dereference(
                Box::from(
                    LeftHandSide::Variable(
                        Variable {
                            name: "toto".to_string(),
                            variable_type: Type::Unknown,
                            mutable: false,
                        }
                    )
                )
            ),
            Expression::get_number(),
            context,
        ),
        Ok(Type::None)
    )
}

// endregion

// region While
#[test]
fn t_check_while_boolean_condition() {
    assert_eq!(
        check_while(
            While {
                condition: Expression::UnaryOperation(
                    UnaryOperator::Not,
                    Expression::get_boxed_boolean(),
                ),
                instructions: Block::NonReturning(
                    vec![
                        Statement::Expression(
                            Expression::get_number()
                        ),
                        Statement::Expression(
                            Expression::UnaryOperation(
                                UnaryOperator::Not,
                                Expression::get_boxed_boolean(),
                            )
                        )
                    ]
                ),
            },
            TypeContext::new(),
        ),
        Ok(
            Type::None
        )
    )
}

#[test]
fn t_check_while_number_condition() {
    assert_eq!(
        check_while(
            While {
                condition: Expression::UnaryOperation(
                    UnaryOperator::Minus,
                    Expression::get_boxed_number(),
                ),
                instructions: Block::NonReturning(
                    vec![
                        Statement::Expression(
                            Expression::get_number()
                        ),
                        Statement::Expression(
                            Expression::UnaryOperation(
                                UnaryOperator::Not,
                                Expression::get_boxed_boolean(),
                            )
                        )
                    ]
                ),
            },
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::Boolean]),
                    vec![Type::Number],
                    String::from("while condition"),
                )
            ]
        )
    )
}

#[test]
fn t_check_while_bad_instructions() {
    assert_eq!(
        check_while(
            While {
                condition: Expression::UnaryOperation(
                    UnaryOperator::Not,
                    Expression::get_boxed_boolean(),
                ),
                instructions: Block::NonReturning(
                    vec![
                        Statement::Declaration(
                            Variable {
                                name: "toto".to_string(),
                                variable_type: Type::Boolean,
                                mutable: false,
                            },
                            Expression::get_number(),
                        ),
                        Statement::Expression(
                            Expression::UnaryOperation(
                                UnaryOperator::Not,
                                Expression::get_boxed_number(),
                            )
                        )
                    ]
                ),
            },
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::ContentError(
                    String::from("while block"),
                    vec![
                        CompilerError::ContentError(
                            String::from("block content"),
                            vec![
                                CompilerError::BadTypes(
                                    to_set(&[Type::Boolean]),
                                    vec![Type::Number],
                                    String::from("declaration of variable 'toto'"),
                                ),
                                CompilerError::BadTypes(
                                    to_set(&[Type::Boolean]),
                                    vec![Type::Number],
                                    String::from("unary operation '!'"),
                                )
                            ],
                        ),
                    ],
                ),
            ]
        )
    )
}

#[test]
fn t_check_while_bad_conditions_bad_instructions() {
    assert_eq!(
        check_while(
            While {
                condition: Expression::UnaryOperation(
                    UnaryOperator::Minus,
                    Expression::get_boxed_number(),
                ),
                instructions: Block::NonReturning(
                    vec![
                        Statement::Declaration(
                            Variable {
                                name: "toto".to_string(),
                                variable_type: Type::Boolean,
                                mutable: false,
                            },
                            Expression::get_number(),
                        ),
                        Statement::Expression(
                            Expression::UnaryOperation(
                                UnaryOperator::Not,
                                Expression::get_boxed_number(),
                            )
                        )
                    ]
                ),
            },
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::Boolean]),
                    vec![Type::Number],
                    String::from("while condition"),
                ),
                CompilerError::ContentError(
                    String::from("while block"),
                    vec![
                        CompilerError::ContentError(
                            String::from("block content"),
                            vec![
                                CompilerError::BadTypes(
                                    to_set(&[Type::Boolean]),
                                    vec![Type::Number],
                                    String::from("declaration of variable 'toto'"),
                                ),
                                CompilerError::BadTypes(
                                    to_set(&[Type::Boolean]),
                                    vec![Type::Number],
                                    String::from("unary operation '!'"),
                                )
                            ],
                        ),
                    ],
                ),
            ]
        )
    )
}
// endregion

// region If
#[test]
fn t_check_if_boolean_condition() {
    assert_eq!(
        check_if(
            If {
                condition: Expression::UnaryOperation(
                    UnaryOperator::Not,
                    Expression::get_boxed_boolean(),
                ),
                true_instructions: Block::NonReturning(
                    vec![
                        Statement::Expression(
                            Expression::get_number()
                        ),
                        Statement::Expression(
                            Expression::UnaryOperation(
                                UnaryOperator::Not,
                                Expression::get_boxed_boolean(),
                            )
                        )
                    ]
                ),
                false_instructions: None,
            },
            TypeContext::new(),
        ),
        Ok(
            Type::None
        )
    )
}

#[test]
fn t_check_if_number_condition() {
    assert_eq!(
        check_if(
            If {
                condition: Expression::UnaryOperation(
                    UnaryOperator::Minus,
                    Expression::get_boxed_number(),
                ),
                true_instructions: Block::NonReturning(
                    vec![
                        Statement::Expression(
                            Expression::get_number()
                        ),
                        Statement::Expression(
                            Expression::UnaryOperation(
                                UnaryOperator::Not,
                                Expression::get_boxed_boolean(),
                            )
                        )
                    ]
                ),
                false_instructions: None,
            },
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::Boolean]),
                    vec![Type::Number],
                    String::from("if condition"),
                )
            ]
        )
    )
}

#[test]
fn t_check_if_bad_true_instructions() {
    assert_eq!(
        check_if(
            If {
                condition: Expression::UnaryOperation(
                    UnaryOperator::Not,
                    Expression::get_boxed_boolean(),
                ),
                true_instructions: Block::NonReturning(
                    vec![
                        Statement::Declaration(
                            Variable {
                                name: "toto".to_string(),
                                variable_type: Type::Boolean,
                                mutable: false,
                            },
                            Expression::get_number(),
                        ),
                        Statement::Expression(
                            Expression::UnaryOperation(
                                UnaryOperator::Not,
                                Expression::get_boxed_number(),
                            )
                        )
                    ]
                ),
                false_instructions: None,
            },
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::ContentError(
                    String::from("if block, true instructions"),
                    vec![
                        CompilerError::ContentError(
                            String::from("block content"),
                            vec![
                                CompilerError::BadTypes(
                                    to_set(&[Type::Boolean]),
                                    vec![Type::Number],
                                    String::from("declaration of variable 'toto'"),
                                ),
                                CompilerError::BadTypes(
                                    to_set(&[Type::Boolean]),
                                    vec![Type::Number],
                                    String::from("unary operation '!'"),
                                )
                            ],
                        ),
                    ],
                ),
            ]
        )
    )
}

#[test]
fn t_check_if_bad_false_instructions() {
    assert_eq!(
        check_if(
            If {
                condition: Expression::UnaryOperation(
                    UnaryOperator::Not,
                    Expression::get_boxed_boolean(),
                ),
                true_instructions: Block::NonReturning(
                    vec![]
                ),
                false_instructions: Some(
                    Block::NonReturning(
                        vec![
                            Statement::Declaration(
                                Variable {
                                    name: "toto".to_string(),
                                    variable_type: Type::Boolean,
                                    mutable: false,
                                },
                                Expression::get_number(),
                            ),
                            Statement::Expression(
                                Expression::UnaryOperation(
                                    UnaryOperator::Not,
                                    Expression::get_boxed_number(),
                                )
                            )
                        ]
                    )
                ),
            },
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::ContentError(
                    String::from("if block, false instructions"),
                    vec![
                        CompilerError::ContentError(
                            String::from("block content"),
                            vec![
                                CompilerError::BadTypes(
                                    to_set(&[Type::Boolean]),
                                    vec![Type::Number],
                                    String::from("declaration of variable 'toto'"),
                                ),
                                CompilerError::BadTypes(
                                    to_set(&[Type::Boolean]),
                                    vec![Type::Number],
                                    String::from("unary operation '!'"),
                                )
                            ],
                        ),
                    ],
                ),
            ]
        )
    )
}

#[test]
fn t_check_if_bad_true_false_instructions() {
    assert_eq!(
        check_if(
            If {
                condition: Expression::UnaryOperation(
                    UnaryOperator::Not,
                    Expression::get_boxed_boolean(),
                ),
                true_instructions: Block::NonReturning(
                    vec![
                        Statement::Declaration(
                            Variable {
                                name: "toto".to_string(),
                                variable_type: Type::Boolean,
                                mutable: false,
                            },
                            Expression::get_number(),
                        ),
                        Statement::Expression(
                            Expression::UnaryOperation(
                                UnaryOperator::Not,
                                Expression::get_boxed_number(),
                            )
                        )
                    ]
                ),
                false_instructions: Some(
                    Block::NonReturning(
                        vec![
                            Statement::Declaration(
                                Variable {
                                    name: "toto".to_string(),
                                    variable_type: Type::Boolean,
                                    mutable: false,
                                },
                                Expression::get_number(),
                            ),
                            Statement::Expression(
                                Expression::UnaryOperation(
                                    UnaryOperator::Not,
                                    Expression::get_boxed_number(),
                                )
                            )
                        ]
                    )
                ),
            },
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::ContentError(
                    String::from("if block, true instructions"),
                    vec![
                        CompilerError::ContentError(
                            String::from("block content"),
                            vec![
                                CompilerError::BadTypes(
                                    to_set(&[Type::Boolean]),
                                    vec![Type::Number],
                                    String::from("declaration of variable 'toto'"),
                                ),
                                CompilerError::BadTypes(
                                    to_set(&[Type::Boolean]),
                                    vec![Type::Number],
                                    String::from("unary operation '!'"),
                                )
                            ],
                        ),
                    ],
                ),
                CompilerError::ContentError(
                    String::from("if block, false instructions"),
                    vec![
                        CompilerError::ContentError(
                            String::from("block content"),
                            vec![
                                CompilerError::BadTypes(
                                    to_set(&[Type::Boolean]),
                                    vec![Type::Number],
                                    String::from("declaration of variable 'toto'"),
                                ),
                                CompilerError::BadTypes(
                                    to_set(&[Type::Boolean]),
                                    vec![Type::Number],
                                    String::from("unary operation '!'"),
                                )
                            ],
                        ),
                    ],
                ),
            ]
        )
    )
}

#[test]
fn t_check_if_bad_conditions_bad_instructions() {
    assert_eq!(
        check_if(
            If {
                condition: Expression::UnaryOperation(
                    UnaryOperator::Minus,
                    Expression::get_boxed_number(),
                ),
                true_instructions: Block::NonReturning(
                    vec![
                        Statement::Declaration(
                            Variable {
                                name: "toto".to_string(),
                                variable_type: Type::Boolean,
                                mutable: false,
                            },
                            Expression::get_number(),
                        ),
                        Statement::Expression(
                            Expression::UnaryOperation(
                                UnaryOperator::Not,
                                Expression::get_boxed_number(),
                            )
                        )
                    ]
                ),
                false_instructions: None,
            },
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::Boolean]),
                    vec![Type::Number],
                    String::from("if condition"),
                ),
                CompilerError::ContentError(
                    String::from("if block, true instructions"),
                    vec![
                        CompilerError::ContentError(
                            String::from("block content"),
                            vec![
                                CompilerError::BadTypes(
                                    to_set(&[Type::Boolean]),
                                    vec![Type::Number],
                                    String::from("declaration of variable 'toto'"),
                                ),
                                CompilerError::BadTypes(
                                    to_set(&[Type::Boolean]),
                                    vec![Type::Number],
                                    String::from("unary operation '!'"),
                                )
                            ],
                        ),
                    ],
                ),
            ]
        )
    )
}

#[test]
fn t_check_if_same_return_type() {
    assert_eq!(
        check_if(
            If {
                condition: Expression::UnaryOperation(
                    UnaryOperator::Not,
                    Expression::get_boxed_boolean(),
                ),
                true_instructions: Block::Returning(
                    vec![],
                    Statement::Expression(
                        Expression::get_number()
                    ),
                ),
                false_instructions: Some(
                    Block::Returning(
                        vec![],
                        Statement::Expression(
                            Expression::get_number()
                        ),
                    )
                ),
            },
            TypeContext::new(),
        ),
        Ok(
            Type::Number
        )
    )
}

#[test]
fn t_check_if_different_return_type() {
    assert_eq!(
        check_if(
            If {
                condition: Expression::UnaryOperation(
                    UnaryOperator::Not,
                    Expression::get_boxed_boolean(),
                ),
                true_instructions: Block::Returning(
                    vec![],
                    Statement::Expression(
                        Expression::get_number()
                    ),
                ),
                false_instructions: Some(
                    Block::Returning(
                        vec![],
                        Statement::Expression(
                            Expression::get_boolean()
                        ),
                    )
                ),
            },
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::Number]),
                    vec![Type::Boolean],
                    String::from("if block, true and false block don't have the same return type"),
                )
            ]
        )
    )
}

#[test]
fn t_check_if_return_type_without_false_block() {
    assert_eq!(
        check_if(
            If {
                condition: Expression::UnaryOperation(
                    UnaryOperator::Not,
                    Expression::get_boxed_boolean(),
                ),
                true_instructions: Block::Returning(
                    vec![],
                    Statement::Expression(
                        Expression::get_number()
                    ),
                ),
                false_instructions: None,
            },
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::Number]),
                    vec![Type::None],
                    String::from("if block, true block can't have a return statement without false block"),
                )
            ]
        )
    )
}

#[test]
fn t_check_if_returning_true_non_returning_false() {
    assert_eq!(
        check_if(
            If {
                condition: Expression::UnaryOperation(
                    UnaryOperator::Not,
                    Expression::get_boxed_boolean(),
                ),
                true_instructions: Block::Returning(
                    vec![],
                    Statement::Expression(
                        Expression::get_number()
                    ),
                ),
                false_instructions: Some(
                    Block::NonReturning(
                        vec![]
                    )
                ),
            },
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::Number]),
                    vec![Type::None],
                    String::from("if block, true and false block don't have the same return type"),
                )
            ]
        )
    )
}


// endregion

// region FunctionCalls
#[test]
fn t_check_function_call() {
    let mut context = TypeContext::new();
    context.add_function(
        Function {
            name: "function".to_string(),
            parameters: vec![
                Variable {
                    name: "toto".to_string(),
                    variable_type: Type::Number,
                    mutable: false,
                },
                Variable {
                    name: "tata".to_string(),
                    variable_type: Type::Boolean,
                    mutable: false,
                }
            ],
            return_type: Type::Number,
            instructions: Block::Returning(
                vec![],
                Statement::Expression(
                    Expression::get_boolean()
                ),
            ),
        }
    );

    assert_eq!(
        check_function_call(
            FunctionCall {
                name: "function".to_string(),
                arguments: vec![
                    Expression::get_number(),
                    Expression::get_boolean(),
                ],
            },
            context,
        ),
        Ok(
            Type::Number
        )
    )
}

#[test]
fn t_check_function_call_bad_arguments() {
    let mut context = TypeContext::new();
    context.add_function(
        Function {
            name: "function".to_string(),
            parameters: vec![
                Variable {
                    name: "toto".to_string(),
                    variable_type: Type::Number,
                    mutable: false,
                },
                Variable {
                    name: "tata".to_string(),
                    variable_type: Type::Boolean,
                    mutable: false,
                }
            ],
            return_type: Type::Number,
            instructions: Block::Returning(
                vec![],
                Statement::Expression(
                    Expression::get_boolean()
                ),
            ),
        }
    );

    assert_eq!(
        check_function_call(
            FunctionCall {
                name: "function".to_string(),
                arguments: vec![
                    Expression::UnaryOperation(
                        UnaryOperator::Minus,
                        Expression::get_boxed_boolean(),
                    ),
                    Expression::get_boolean(),
                ],
            },
            context,
        ),
        Err(
            vec![
                CompilerError::ContentError(
                    String::from("call of function 'function'"),
                    vec![
                        CompilerError::ContentError(
                            String::from("argument 1 for function 'function'"),
                            vec! {
                                CompilerError::BadTypes(
                                    to_set(&[Type::Number]),
                                    vec![Type::Boolean],
                                    String::from("unary operation '-'"),
                                )
                            },
                        )
                    ],
                )
            ]
        )
    )
}

#[test]
fn t_check_function_call_bad_number_of_arguments() {
    let mut context = TypeContext::new();
    context.add_function(
        Function {
            name: "function".to_string(),
            parameters: vec![
                Variable {
                    name: "toto".to_string(),
                    variable_type: Type::Number,
                    mutable: false,
                },
                Variable {
                    name: "tata".to_string(),
                    variable_type: Type::Boolean,
                    mutable: false,
                }
            ],
            return_type: Type::Number,
            instructions: Block::Returning(
                vec![],
                Statement::Expression(
                    Expression::get_boolean()
                ),
            ),
        }
    );

    assert_eq!(
        check_function_call(
            FunctionCall {
                name: "function".to_string(),
                arguments: vec![
                    Expression::get_number(),
                    Expression::get_boolean(),
                    Expression::get_boolean(),
                ],
            },
            context,
        ),
        Err(
            vec![
                CompilerError::ContentError(
                    String::from("call of function 'function'"),
                    vec![
                        CompilerError::Other(String::from("Expected 2 arguments, found 3"))
                    ],
                )
            ]
        )
    )
}

#[test]
fn t_check_function_call_bad_type_of_arguments() {
    let mut context = TypeContext::new();
    context.add_function(
        Function {
            name: "function".to_string(),
            parameters: vec![
                Variable {
                    name: "toto".to_string(),
                    variable_type: Type::Number,
                    mutable: false,
                },
                Variable {
                    name: "tata".to_string(),
                    variable_type: Type::Boolean,
                    mutable: false,
                }
            ],
            return_type: Type::Number,
            instructions: Block::Returning(
                vec![],
                Statement::Expression(
                    Expression::get_boolean()
                ),
            ),
        }
    );

    assert_eq!(
        check_function_call(
            FunctionCall {
                name: "function".to_string(),
                arguments: vec![
                    Expression::get_boolean(),
                    Expression::get_number(),
                ],
            },
            context,
        ),
        Err(
            vec![
                CompilerError::ContentError(
                    String::from("call of function 'function'"),
                    vec![
                        CompilerError::BadTypes(
                            to_set(&[Type::Number]),
                            vec![Type::Boolean],
                            String::from("argument 1 for function 'function'"),
                        ),
                        CompilerError::BadTypes(
                            to_set(&[Type::Boolean]),
                            vec![Type::Number],
                            String::from("argument 2 for function 'function'"),
                        )
                    ],
                )
            ]
        )
    )
}

#[test]
fn t_check_function_not_found() {
    let context = TypeContext::new();

    assert_eq!(
        check_function_call(
            FunctionCall {
                name: "function".to_string(),
                arguments: vec![
                    Expression::get_boolean(),
                    Expression::get_number(),
                ],
            },
            context,
        ),
        Err(
            vec![
                CompilerError::ContentError(
                    String::from("call of function 'function'"),
                    vec![CompilerError::NotFound],
                )
            ]
        )
    )
}
//endregion

// region Functions
#[test]
fn t_check_function() {
    assert_eq!(
        check_function(
            Function {
                name: "function".to_string(),
                parameters: vec![
                    Variable {
                        name: "toto".to_string(),
                        variable_type: Type::Number,
                        mutable: false,
                    }
                ],
                return_type: Type::Number,
                instructions: Block::Returning(
                    vec![
                        Statement::Expression(
                            Expression::BinaryOperation(
                                Expression::get_boxed_number(),
                                BinaryOperator::Plus,
                                Box::from(
                                    Expression::Variable(
                                        Variable {
                                            name: "toto".to_string(),
                                            variable_type: Type::Number,
                                            mutable: false,
                                        }
                                    )
                                ),
                            )
                        )
                    ],
                    Statement::Expression(
                        Expression::get_number()
                    ),
                ),
            },
            TypeContext::new(),
        ),
        Ok(
            Type::Number
        )
    )
}

#[test]
fn t_check_function_without_return() {
    assert_eq!(
        check_function(
            Function {
                name: "function".to_string(),
                parameters: vec![],
                return_type: Type::None,
                instructions: Block::NonReturning(
                    vec![]
                ),
            },
            TypeContext::new(),
        ),
        Ok(
            Type::None
        )
    )
}

#[test]
fn t_check_function_without_return_bad_return_type() {
    assert_eq!(
        check_function(
            Function {
                name: "function".to_string(),
                parameters: vec![],
                return_type: Type::Number,
                instructions: Block::NonReturning(
                    vec![]
                ),
            },
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::Number]),
                    vec![Type::None],
                    String::from("return type for function 'function' does not match its real return type"),
                )
            ]
        )
    )
}


#[test]
fn t_check_function_instructions_error() {
    assert_eq!(
        check_function(
            Function {
                name: "function".to_string(),
                parameters: vec![],
                return_type: Type::Number,
                instructions: Block::NonReturning(
                    vec![
                        Statement::Expression(
                            Expression::BinaryOperation(
                                Box::from(Expression::get_number()),
                                BinaryOperator::Plus,
                                Box::from(Expression::get_boolean()),
                            )
                        ),
                        Statement::Expression(
                            Expression::BinaryOperation(
                                Box::from(Expression::get_number()),
                                BinaryOperator::And,
                                Box::from(Expression::get_boolean()),
                            )
                        )
                    ]
                ),
            },
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::ContentError(
                    String::from("function 'function'"),
                    vec![
                        CompilerError::ContentError(
                            String::from("block content"),
                            vec![
                                CompilerError::BadTypes(
                                    to_set(&[
                                        Type::get_tuple_number_number()
                                    ]),
                                    vec![
                                        Type::get_tuple_number_boolean()
                                    ],
                                    String::from("binary operation '+'"),
                                ),
                                CompilerError::BadTypes(
                                    to_set(&[
                                        Type::get_tuple_boolean_boolean()
                                    ]),
                                    vec![
                                        Type::get_tuple_number_boolean()
                                    ],
                                    String::from("binary operation '&&'"),
                                )
                            ],
                        )
                    ],
                )
            ]
        )
    )
}

#[test]
fn t_check_function_return_type_error() {
    assert_eq!(
        check_function(
            Function {
                name: "function".to_string(),
                parameters: vec![],
                return_type: Type::Number,
                instructions: Block::Returning(
                    vec![],
                    Statement::Expression(
                        Expression::get_boolean()
                    ),
                ),
            },
            TypeContext::new(),
        ),
        Err(
            vec![
                CompilerError::BadTypes(
                    to_set(&[Type::Number]),
                    vec![Type::Boolean],
                    String::from("return type for function 'function' does not match its real return type"),
                )
            ]
        )
    )
}
// endregion