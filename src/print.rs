use std::fs;

use crate::tests::get_test_file_path;
use std::fs::OpenOptions;
use std::io::Write;

const DEBUG: bool = true;

pub fn print(string: String) {
    let test = std::env::args().len() > 2;

    if !DEBUG && !test {
        return;
    }

    if test {
        let mut file = OpenOptions::new()
            .write(true)
            .append(true)
            .open(get_test_file_path())
            .unwrap();

        let _ = file.write(string.as_bytes());
        let _ = file.write("\n".as_bytes());
    } else {
        print_to_console(string);
    }
}

pub fn print_to_console(string: String) {
    println!("{}",string);
}