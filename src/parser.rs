use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{alpha1, digit1, multispace0, multispace1},
    combinator::{map, map_res, opt},
    multi::{fold_many0, fold_many1, separated_list},
    error::context,
    sequence::{preceded, tuple},
};
use crate::tokens::*;
use crate::error::*;

// region Parsers
pub fn parse_i32(input: &str) -> ParseResult<&str, Expression> {
    map_res(digit1, |digit_str: &str| match digit_str.parse::<i32>() {
        Err(_) => Err(CompilerError::Parse(Type::Number)),
        Ok(x) => Ok(Expression::Number(x)),
    })(input)
}

pub fn parse_boolean(input: &str) -> ParseResult<&str, Expression> {
    context(
        "parse_boolean",
        alt(
            (
                map(
                    tag("true"),
                    |_| Expression::Boolean(true),
                ),
                map(
                    tag("false"),
                    |_| Expression::Boolean(false),
                )
            )
        ),
    )(input)
}

pub fn parse_terminal(input: &str) -> ParseResult<&str, Expression> {
    context(
        "parse_terminal",
        alt(
            (
                parse_i32,
                parse_boolean
            )
        ),
    )(input)
}

pub fn parse_type(input: &str) -> ParseResult<&str, Type> {
    context(
        "parse_type",
        preceded(
            multispace0,
            alt(
                (
                    map(
                        tuple(
                            (
                                tag("&"),
                                opt(
                                    preceded(
                                        multispace0,
                                        tag("mut"),
                                    )
                                ),
                                preceded(
                                    multispace0,
                                    parse_type,
                                )
                            )
                        ),
                        |(_, mutable, t)| Type::Reference(
                            Box::from(t),
                            mutable.is_some().into(),
                        ),
                    )
                    ,
                    map(
                        tag("i32"),
                        |_| Type::Number,
                    ),
                    map(
                        tag("bool"),
                        |_| Type::Boolean,
                    )
                )
            ),
        ),
    )(input)
}

pub fn parse_binary_operation(input: &str) -> ParseResult<&str, Expression> {
    fn to_binary_operation((left, operator, right): (Expression, &str, Expression)) -> Expression {
        Expression::BinaryOperation(
            Box::new(left),
            BinaryOperator::get(operator),
            Box::new(right),
        )
    }

    let operators_list = BinaryOperator::list();
    let mut result: ParseResult<&str, Expression> = Ok((input, Expression::None));
    for operator in operators_list {
        result = preceded(
            multispace0,
            map(
                tuple(
                    (
                        alt(
                            (
                                parse_parenthesis,
                                parse_unary_operation,
                                parse_terminal,
                                parse_variable
                            )
                        ),
                        preceded(multispace0, tag(operator)),
                        preceded(multispace0, parse_expression)
                    )
                ),
                to_binary_operation,
            ),
        )(input);
        match result {
            Ok(result) => return Ok(result),
            Err(_) => {}
        }
    }

    result
}

pub fn parse_unary_operation(input: &str) -> ParseResult<&str, Expression> {
    fn to_unary_operation((operator, right): (&str, Expression)) -> Expression {
        Expression::UnaryOperation(UnaryOperator::get(operator), Box::new(right))
    }

    let operators_list = UnaryOperator::list();
    let mut result: ParseResult<&str, Expression> = Ok((input, Expression::None));
    for operator in operators_list {
        result = preceded(
            multispace0,
            map(
                tuple(
                    (
                        tag(operator),
                        parse_expression
                    )
                ),
                to_unary_operation,
            ),
        )(input);
        match result {
            Ok(result) => return Ok(result),
            Err(_) => {}
        }
    }

    result
}

pub fn parse_reference(input: &str) -> ParseResult<&str, Expression> {
    context(
        "parse_reference",
        map(
            preceded(
                multispace0,
                tuple(
                    (
                        tag("&"),
                        alt(
                            (
                                tuple(
                                    (
                                        map(
                                            preceded(
                                                multispace0,
                                                tag("mut"),
                                            ),
                                            |_| Mutable::Mutable,
                                        ),
                                        preceded(
                                            multispace1,
                                            parse_expression,
                                        )
                                    )
                                ),
                                map(
                                    preceded(
                                        multispace0,
                                        parse_expression,
                                    ),
                                    |expresion| (Mutable::Immutable, expresion),
                                )
                            )
                        ),
                    )
                ),
            ),
            |(_, (mutable, expression))| Expression::Reference
                (
                    match expression.clone() {
                        Expression::Variable(variable) => {
                            ReferenceTarget::Variable(
                                variable
                            )
                        }
                        _ => ReferenceTarget::Expression(
                            Box::from(expression)
                        )
                    },
                    mutable,
                ),
        ),
    )(input)
}

pub fn parse_expression(input: &str) -> ParseResult<&str, Expression> {
    context(
        "parse_expression",
        map(
            preceded(
                multispace0,
                alt((
                    parse_reference,
                    map(
                        parse_block,
                        |block| Expression::Block(Box::from(block)),
                    ),
                    map(
                        parse_if,
                        |if_block| Expression::If(Box::from(if_block)),
                    ),
                    map(
                        parse_function_call,
                        |function_call| Expression::FunctionCall(function_call),
                    ),
                    parse_binary_operation,
                    parse_unary_operation,
                    parse_parenthesis,
                    parse_terminal,
                    parse_variable,
                    parse_reference,
                    map(
                        tuple(
                            (
                                tag("*"),
                                parse_expression
                            )
                        ),
                        |(_, expression)| Expression::Dereference(Box::from(expression)),
                    )
                )),
            ),
            |x| reorder_expr(x),
        ),
    )(input)
}

pub fn parse_parenthesis(input: &str) -> ParseResult<&str, Expression> {
    context(
        "parse_parenthesis",
        map(
            tuple(
                (
                    preceded(
                        multispace0,
                        tag("("),
                    ),
                    preceded(
                        multispace0,
                        parse_expression,
                    ),
                    preceded(
                        multispace0,
                        tag(")"),
                    )
                )
            ),
            |(_, expresion, _)| Expression::ParenthizedExpression(Box::from(expresion)),
        ),
    )(input)
}

pub fn parse_object_name(input: &str) -> ParseResult<&str, String> {
    context(
        "parse_object_name",
        map(
            fold_many1(
                alt(
                    (
                        alpha1,
                        tag("_")
                    )
                ),
                Vec::new(),
                |mut acc: Vec<&str>, item| {
                    acc.push(item);
                    acc
                },
            ),
            |name_parts: Vec<&str>| {
                let mut name: String = String::from("");
                for part in name_parts.iter() {
                    name.push_str(part)
                }
                name
            },
        ),
    )(input)
}

pub fn parse_variable(input: &str) -> ParseResult<&str, Expression> {
    context(
        "parse_variable",
        map(
            parse_object_name,
            |name: String| {
                Expression::Variable(
                    Variable {
                        name: name,
                        variable_type: Type::Unknown,
                        mutable: false,
                    }
                )
            },
        ),
    )(input)
}

pub fn parse_declaration(input: &str) -> ParseResult<&str, Statement> {
    context(
        "parse_declaration",
        map(
            tuple(
                (
                    tag("let"),
                    opt(
                        preceded(
                            multispace1,
                            tag("mut"),
                        )
                    ),
                    preceded(
                        multispace1,
                        parse_variable,
                    ),
                    preceded(
                        multispace0,
                        map(
                            tuple(
                                (
                                    tag(":"),
                                    preceded(
                                        multispace0,
                                        parse_type,
                                    )
                                )
                            ),
                            |(_, variable_type)| variable_type,
                        ),
                    ),
                    preceded(
                        multispace0,
                        tag("="),
                    ),
                    preceded(
                        multispace0,
                        parse_expression,
                    ),
                )
            ),
            |(_, mutable, variable, variable_type, _, expression)| {
                let mutable = mutable.is_some();
                match variable {
                    Expression::Variable(v) => {
                        Statement::Declaration(
                            Variable {
                                name: v.name,
                                variable_type,
                                mutable,
                            },
                            expression,
                        )
                    }
                    _ => Statement::None
                }
            },
        )
        ,
    )(input)
}

pub fn parse_left_hand_side(input: &str) -> ParseResult<&str, LeftHandSide> {
    context(
        "parse_left_hand_side",
        preceded(
            multispace0,
            alt(
                (
                    map(
                        parse_variable,
                        |variable| match variable {
                            Expression::Variable(variable) => LeftHandSide::Variable(variable),
                            _ => LeftHandSide::None,
                        },
                    ),
                    map(
                        tuple(
                            (
                                tag("&"),
                                opt(
                                    tag("mut")
                                ),
                                parse_left_hand_side
                            )
                        ),
                        |(_, mutable, expression)| {
                            let expression: Expression = expression.into();
                            let mutable = match mutable.is_some() {
                                true => Mutable::Mutable,
                                false => Mutable::Immutable
                            };
                            LeftHandSide::Reference(
                                ReferenceTarget::Expression(
                                    Box::from(
                                        expression
                                    )
                                ),
                                mutable,
                            )
                        },
                    )
                    ,
                    map(
                        tuple(
                            (
                                tag("*"),
                                parse_left_hand_side
                            )
                        ),
                        |(_, expression)| LeftHandSide::Dereference(Box::from(expression)),
                    )
                )
            ),
        ),
    )(input)
}

pub fn parse_assignment(input: &str) -> ParseResult<&str, Statement> {
    context(
        "parse_assignment",
        map(
            tuple(
                (
                    parse_left_hand_side,
                    preceded(
                        multispace1,
                        tag("="),
                    ),
                    preceded(
                        multispace1,
                        parse_expression,
                    ),
                )
            ),
            |(left_hand_side, _, expression)| Statement::Assignment(left_hand_side, expression),
        )
        ,
    )(input)
}

pub fn parse_non_returning_statement(input: &str) -> ParseResult<&str, Statement> {
    context(
        "parse_non_returning_statement",
        preceded(
            multispace0,
            map(
                tuple(
                    (
                        alt(
                            (
                                map(
                                    parse_while,
                                    |while_block| Statement::While(Box::from(while_block)),
                                ),
                                parse_declaration,
                                parse_assignment,
                                map(
                                    parse_expression,
                                    |expression| Statement::Expression(expression),
                                )
                            ),
                        ),
                        tag(";"),
                    )
                ),
                |(statement, _)| statement,
            ),
        ),
    )(input)
}

pub fn parse_returning_statement(input: &str) -> ParseResult<&str, Statement> {
    context(
        "parse_returning_statement",
        map(
            tuple(
                (
                    tag("return"),
                    preceded(
                        multispace1,
                        parse_expression,
                    ),
                    preceded(
                        multispace0,
                        tag(";"),
                    ),
                )
            ),
            |(_, expression, _)| Statement::Expression(expression),
        ),
    )(input)
}

pub fn parse_block(input: &str) -> ParseResult<&str, Block> {
    context(
        "parse_block",
        map(
            tuple(
                (
                    preceded(
                        multispace0,
                        tag("{"),
                    ),
                    opt(
                        preceded(
                            multispace0,
                            fold_many0(
                                parse_non_returning_statement,
                                Vec::new(),
                                |mut acc: Vec<Statement>, item| {
                                    acc.push(item);
                                    acc
                                },
                            ),
                        )
                    ),
                    opt(
                        preceded(
                            multispace0,
                            parse_returning_statement,
                        )
                    ),
                    preceded(
                        multispace0,
                        tag("}"),
                    )
                )
            ),
            |(_, non_returning_statements, returning_statement, _)| {
                let non_returning_statements = match non_returning_statements {
                    None => vec![],
                    _ => non_returning_statements.unwrap()
                };

                match returning_statement {
                    None => Block::NonReturning(non_returning_statements),
                    _ => {
                        let returning_statement = returning_statement.unwrap();
                        Block::Returning(non_returning_statements, returning_statement)
                    }
                }
            },
        )
        ,
    )(input)
}

fn parse_parameters_list(input: &str) -> ParseResult<&str, Vec<Variable>> {
    context(
        "parse_parameters_list",
        separated_list(
            preceded(
                multispace0,
                tag(","),
            ),
            map(
                tuple(
                    (
                        opt(
                            preceded(
                                multispace0,
                                tuple(
                                    (
                                        tag("mut"),
                                        multispace1
                                    )
                                ),
                            ),
                        ),
                        preceded(
                            multispace0,
                            parse_object_name,
                        ),
                        preceded(
                            multispace0,
                            map(
                                tuple(
                                    (
                                        tag(":"),
                                        preceded(
                                            multispace0,
                                            parse_type,
                                        )
                                    )
                                ),
                                |(_, variable_type)| variable_type,
                            ),
                        )
                    )
                ),
                |(mutable, name, variable_type)| Variable {
                    name,
                    variable_type: variable_type,
                    mutable: mutable.is_some(),
                },
            ),
        ),
    )(input)
}

fn parse_function_return_type(input: &str) -> ParseResult<&str, Type> {
    context(
        "parse_function_return_type",
        preceded(multispace0,
                 map(
                     opt(
                         tuple(
                             (
                                 tag("->"),
                                 preceded(
                                     multispace0,
                                     alt(
                                         (
                                             map(
                                                 tag("i32"),
                                                 |_| Type::Number,
                                             ),
                                             map(
                                                 tag("bool"),
                                                 |_| Type::Boolean,
                                             )
                                         )
                                     ),
                                 ),
                             )
                         )
                     ),
                     |optionnal_type| {
                         match optionnal_type {
                             None => Type::None,
                             Some((_, return_type)) => return_type,
                         }
                     },
                 ),
        ),
    )(input)
}

pub fn parse_function(input: &str) -> ParseResult<&str, Function> {
    context(
        "parse_function",
        map(
            tuple(
                (
                    tag("fn"),
                    preceded(
                        multispace1,
                        parse_object_name,
                    ),
                    preceded(
                        multispace0,
                        tag("("),
                    ),
                    preceded(
                        multispace0,
                        parse_parameters_list,
                    ),
                    preceded(
                        multispace0,
                        tag(")"),
                    ),
                    preceded(
                        multispace0,
                        parse_function_return_type,
                    ),
                    preceded(
                        multispace0,
                        parse_block,
                    ),
                )
            ),
            |(_, name, _, parameters, _, return_type, instructions)| Function {
                name,
                parameters,
                return_type,
                instructions,
            },
        )
        ,
    )(input)
}

fn parse_arguments_list(input: &str) -> ParseResult<&str, Vec<Expression>> {
    context(
        "parse_arguments_list",
        separated_list(
            preceded(
                multispace0,
                tag(","),
            ),
            parse_expression,
        ),
    )(input)
}

pub fn parse_function_call(input: &str) -> ParseResult<&str, FunctionCall> {
    context(
        "parse_function",
        map(
            tuple(
                (
                    preceded(
                        multispace0,
                        parse_object_name,
                    ),
                    preceded(
                        multispace0,
                        tag("("),
                    ),
                    preceded(
                        multispace0,
                        parse_arguments_list,
                    ),
                    preceded(
                        multispace0,
                        tag(")"),
                    ),
                )
            ),
            |(name, _, arguments, _)| FunctionCall {
                name,
                arguments,
            },
        )
        ,
    )(input)
}

pub fn parse_file(input: &str) -> ParseResult<&str, File> {
    context(
        "parse_file",
        map(
            tuple(
                (
                    multispace0,
                    separated_list(
                        multispace0,
                        parse_function,
                    ),
                    multispace0
                )
            ),
            |(_, functions, _)| File {
                functions
            },
        ),
    )(input)
}

pub fn parse_if(input: &str) -> ParseResult<&str, If> {
    context(
        "parse_if",
        map(
            tuple(
                (
                    preceded(
                        multispace0,
                        tag("if"),
                    ),
                    preceded(
                        multispace1,
                        parse_expression,
                    ),
                    preceded(
                        multispace0,
                        parse_block,
                    ),
                    opt(
                        preceded(
                            multispace0,
                            tag("else"),
                        )
                    ),
                    opt(
                        preceded(
                            multispace0,
                            parse_block,
                        )
                    ),
                )
            ),
            |(_, condition, true_instructions, _, false_instructions)| If {
                condition,
                true_instructions,
                false_instructions,
            },
        ),
    )(input)
}

pub fn parse_while(input: &str) -> ParseResult<&str, While> {
    context(
        "parse_while",
        preceded(
            multispace0,
            map(
                tuple(
                    (
                        tag("while"),
                        preceded(
                            multispace1,
                            parse_expression,
                        ),
                        preceded(
                            multispace0,
                            parse_block,
                        ),
                    )
                ),
                |(_, condition, instructions)| While {
                    condition,
                    instructions,
                },
            ),
        ),
    )(input)
}
//endregion

// region Precedence climbing
pub fn reorder_expr(expression: Expression) -> Expression {
    let expressions = compute_expression_vector(expression);
    compute_expr(expressions, &mut 0, 1)
}

fn compute_expression_vector(expression: Expression) -> Vec<Expression> {
    let mut expressions = vec![];

    match expression {
        Expression::BinaryOperation(left, operator, right) => {
            expressions.extend(compute_expression_vector(*left));
            expressions.push(Expression::BinaryOperation(
                Box::from(Expression::None),
                operator,
                Box::from(Expression::None),
            ));
            expressions.extend(compute_expression_vector(*right));
        }
        Expression::UnaryOperation(operator, expression) => {
            expressions.push(Expression::UnaryOperation(operator, Box::from(Expression::None)));
            expressions.extend(compute_expression_vector(*expression));
        }
        _ => expressions.push(expression)
    }

    expressions
}

fn compute_atom(option: Option<&Expression>, expressions: Vec<Expression>, index: &mut usize) -> Expression {
    match option {
        Some(expression) => match expression.clone() {
            Expression::UnaryOperation(operator, _) => {
                *index = *index + 1;
                let expression = compute_atom(expressions.get(*index), expressions.clone(), index);
                Expression::UnaryOperation(operator, Box::from(expression))
            }
            _ => expression.clone()
        },
        None => Expression::None
    }
}

fn compute_expr(expressions: Vec<Expression>, index: &mut usize, minimum_precedence: i32) -> Expression {
    let current_expression = expressions.get(*index);
    let mut left_atom = compute_atom(current_expression, expressions.clone(), index);
    *index = *index + 1;

    while true {
        let option = expressions.get(*index);
        let current_expression = match option {
            Some(expression) => expression,
            None => &Expression::None
        };

        let current_operator = match current_expression {
            Expression::BinaryOperation(_, operator, _) => operator,
            _ => &BinaryOperator::None
        };

        let current_precedence = current_operator.get_precedence();
        if current_precedence < minimum_precedence {
            break;
        }


        let (precedence, associativity) = (current_operator.get_precedence(), current_operator.get_associativity());
        let next_minimum_precedence = match associativity {
            Associativity::Left => precedence + 1,
            Associativity::Right => precedence,
        };

        *index = *index + 1;
        let right_atom = compute_expr(expressions.clone(), index, next_minimum_precedence);

        left_atom = match current_expression {
            Expression::BinaryOperation(_, operator, _) => Expression::BinaryOperation(Box::from(left_atom.clone()), operator.clone(), Box::from(right_atom)),
            Expression::UnaryOperation(operator, _) => Expression::UnaryOperation(operator.clone(), Box::from(left_atom)),
            _ => Expression::None
        };
//        left_atom = Expression::BinaryOperation(Box::from(left_atom.clone()), current_operator.clone(), Box::from(right_atom.clone()));
    }

    left_atom
}
// endregion