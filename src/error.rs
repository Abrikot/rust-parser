use nom::{
    error,
    Err,
};
use crate::tokens::{Type, BorrowMarker};
use std::collections::HashSet;

#[derive(Debug, PartialEq)]
pub struct Error<'a>(&'a str, pub CompilerError);

impl<'a> error::ParseError<&'a str> for Error<'a> {
    fn from_error_kind(input: &'a str, kind: error::ErrorKind) -> Self {
        Error(input, CompilerError::Nom(kind))
    }

    fn append(_: &str, _: error::ErrorKind, other: Self) -> Self {
        other
    }
}

pub type ParseResult<'a, I, O, E = Error<'a>> = Result<(I, O), Err<E>>;

#[derive(Debug, PartialEq, Clone)]
pub enum CompilerError {
    Parse(Type),
    Interpretation(String),
    Nom(nom::error::ErrorKind),
    NotFound,
    MemoryFull,
    Immutable(String),
    ContentError(String, Vec<CompilerError>),  // Description, errors
    BadTypes(HashSet<Type>, Vec<Type>, String),    // Expected, real, position (which expression or statement or ...)
    BorrowError(BorrowMarker, BorrowMarker),    // Previous borrow marker, new borrow marker
    NotImplemented,
    NoContextToReplace,
    NoOperator,
    Io(std::io::ErrorKind),
    FileNotEmpty(String),
    Other(String)
}

impl std::convert::From<std::io::Error> for CompilerError {
    fn from(e: std::io::Error) -> Self {
        CompilerError::Io(e.kind())
    }
}

//noinspection RsSelfConvention
pub fn to_set(types: &[Type]) -> HashSet<Type> {
    types.iter().cloned().collect()
}

impl std::convert::From<CompilerError> for Vec<CompilerError> {
    fn from(error: CompilerError) -> Self {
        vec![error]
    }
}

pub fn add_errors_from_statement_content(errors: &mut Vec<CompilerError>, new_errors: Vec<CompilerError>, description: String) {
    errors.push(
        CompilerError::ContentError(
            description,
            new_errors,
        )
    );
}