use crate::tokens::*;
use crate::error::*;
use std::collections::HashMap;

// Points to a value in the memory
#[derive(Debug, PartialEq, Clone)]
pub struct MemoryLocation {
    // In the `Context` linked-list (0 points to the current Context, 1 to its parent, ...)
    pub layer: usize,
    pub location: usize,
}

const MEMORY_SIZE: usize = 1000;

#[derive(Debug, PartialEq, Clone)]
pub struct Context {
    pub super_context: Option<Box<Context>>,
    functions: HashMap<String, Function>,
    // A `name` is linked to a tuple (Variable, position in the memory)
    variables: HashMap<String, (Variable, MemoryLocation)>,
    memory: Vec<Expression>,
}

impl Context {
    pub fn new() -> Context {
        Context {
            super_context: None,
            functions: Default::default(),
            variables: Default::default(),
            memory: vec![Expression::None; MEMORY_SIZE],
        }
    }
    pub fn new_from_parent_context(parent_context: Context) -> Context {
        Context {
            super_context: Some(Box::from(parent_context)),
            functions: Default::default(),
            variables: Default::default(),
            memory: vec![Expression::None; MEMORY_SIZE],
        }
    }
    pub fn new_for_function(parent_context: Context) -> Context {
        Context {
            super_context: Some
                (
                    Box::from
                        (
                            if parent_context.clone().super_context.is_some() {
                                Context {
                                    super_context: Some(Box::from(Context::new_for_function(*parent_context.clone().super_context.unwrap()))),
                                    functions: Default::default(),
                                    variables: Default::default(),
                                    memory: parent_context.memory,
                                }
                            } else {
                                Context {
                                    super_context: None,
                                    functions: parent_context.functions,
                                    variables: Default::default(),
                                    memory: parent_context.memory,
                                }
                            }
                        )
                ),
            functions: Default::default(),
            variables: Default::default(),
            memory: vec![Expression::None; MEMORY_SIZE],
        }
    }

    fn get_context(&mut self, index: usize) -> Result<&mut Context, CompilerError> {
        let mut context = self;
        for _ in 0..index {
            match &mut context.super_context {
                None => return Err(CompilerError::NotFound),
                Some(super_context) => context = super_context,
            };
        }
        Ok(context)
    }

    pub fn get_local_variables(&self) -> HashMap<String, (Variable, MemoryLocation)> {
        self.clone().variables
    }
    pub fn add_variable(&mut self, variable: Variable, expression: Expression) -> Result<bool, CompilerError> {
        let name = variable.clone().name;
        let memory_location = MemoryLocation {
            layer: 0,
            location: self.find_next_memory_location()?,
        };
        self.variables.insert(name, (variable, memory_location.clone()));
        self.replace_value_in_memory(&memory_location, expression)?;
        Ok(true)
    }
    pub fn add_reference(&mut self, reference: Variable, memory_location: MemoryLocation) -> Result<bool, CompilerError> {
        let name = reference.clone().name;
        self.variables.insert(name, (reference, memory_location));
        Ok(true)
    }
    pub fn replace_variable(&mut self, variable: Variable, expression: Expression) -> Result<&mut Context, CompilerError> {
        let name = variable.clone().name;
        let cloned_self = self.clone();
        let existing_variable = cloned_self.variables.get(&name);

        if existing_variable.clone().is_none() {
            if self.super_context.is_some() {
                let mut super_context = *self.super_context.clone().unwrap();
                let result = super_context.replace_variable(variable, expression)?;
                self.super_context = Some(Box::from(result.clone()));
                return Ok(self);
            } else {
                return Err(CompilerError::NotFound);
            }
        }

        let existing_variable = existing_variable.clone();
        let (existing_variable, location) = existing_variable.unwrap();

        match existing_variable.clone().variable_type {
            Type::Reference(_, mutable) => {
                if mutable == Mutable::Immutable {
                    return Err(CompilerError::Immutable(format!("Variable reference '{:?}' is immutable", existing_variable)));
                }
            }
            _ => {
                if !existing_variable.mutable {
                    return Err(CompilerError::Immutable(format!("Variable '{:?}' is immutable", existing_variable)));
                }
            }
        };

        self.replace_value_in_memory(location, expression)?;

        return Ok(self);
    }

    fn find_next_memory_location(&self) -> Result<usize, CompilerError> {
        for location in 0..self.memory.len() {
            match self.memory.get(location) {
                None => {}
                Some(expression) => if expression.clone() == Expression::None {
                    return Ok(location);
                },
            };
        }

        Err(CompilerError::MemoryFull)
    }
    fn replace_value_in_memory(&mut self, memory_location: &MemoryLocation, expression: Expression) -> Result<bool, CompilerError> {
        let context = self.get_context(memory_location.layer)?;
        context.memory.remove(memory_location.location);
        context.memory.insert(memory_location.location, expression);
        Ok(true)
    }
    fn get_value(&mut self, memory_location: MemoryLocation) -> Result<Expression, CompilerError> {
        let context = self.get_context(memory_location.layer)?;

        match context.memory.get(memory_location.location) {
            None => Err(CompilerError::NotFound),
            Some(value) => Ok(value.clone()),
        }
    }


    pub fn find_variable(&self, variable_name: String) -> Result<(Variable, MemoryLocation), CompilerError> {
        match self.get_local_variables().get(&variable_name) {
            None => {}
            Some((variable, memory_location)) => return Ok((variable.clone(), memory_location.clone())),
        };

        match self.clone().super_context {
            None => return Err(CompilerError::NotFound),
            Some(super_context) => {
                match super_context.find_variable(variable_name) {
                    Ok((variable, memory_location)) => Ok(
                        (
                            variable,
                            MemoryLocation {
                                layer: memory_location.layer + 1,
                                location: memory_location.location,
                            }
                        )
                    ),
                    Err(_) => Err(CompilerError::NotFound),
                }
            }
        }
    }
    pub fn find_variable_value(&mut self, variable_name: String) -> Result<Expression, CompilerError> {
        let location = self.find_variable(variable_name)?.1;

        self.get_value(location)
    }

    pub fn replace_into_original_context(self, original_context: &mut Context) -> Result<bool, CompilerError> {
        for location in 0..self.memory.len() {
            let expression = self.memory.get(location).unwrap();
            original_context.replace_value_in_memory(&MemoryLocation {
                layer: 0,
                location: location,
            }, expression.clone())?;
        }

        if self.super_context.is_some() {
            if original_context.super_context.is_none() {
                return Ok(true);
            }

            let super_context = *self.super_context.unwrap();
            let original_super_context = original_context.super_context.clone();
            let mut original_super_context = original_super_context.unwrap();

            super_context.replace_into_original_context(&mut original_super_context)?;
        }

        Ok(true)
    }


    // region Functions
    pub fn add_function(&mut self, function: Function) -> Option<Function> {
        let name = function.clone().name;
        self.functions.insert(name, function)
    }
    pub fn get_all_functions(self) -> HashMap<String, Function> {
        let mut all_functions = self.functions.clone();
        if self.super_context.is_some() {
            let super_functions = self.super_context.unwrap().get_all_functions();
            for (name, function) in super_functions.iter() {
                all_functions.insert(
                    name.clone(),
                    function.clone(),
                );
            }
        };
        all_functions
    }
    pub fn find_function(self, name: String) -> Result<Function, CompilerError> {
        let value = self.get_all_functions();
        let value = value.get(name.as_str());
        match value {
            None => Err(CompilerError::NotFound),
            Some(value) => Ok(value.clone()),
        }
    }
    // endregion
}