fn main() {
    let mut integer_variable: i32 = 1;
    integer_variable = -(integer_variable + *&integer_variable - 1);

    let boolean_variable: bool = true;

    if boolean_variable {
        println(integer_variable);
    };

    {
        let mut reference_on_integer: &mut i32 = &mut integer_variable;
        *reference_on_integer = 16;
    };

    while integer_variable != 0 {
        integer_variable = integer_variable / 2;
        println(integer_variable);
    };

    if integer_variable != 0 {
        println(true);
    } else {
        println(false);
    };

    println(test_if_equals(integer_variable + 3, 3));


    let mut other_variable: i32 = 1;
    let mut reference_on_other_variable: &mut i32 = &mut other_variable;
    assign_value_to_reference(reference_on_other_variable, 6);
    println(*reference_on_other_variable);
}

fn test_if_equals(x: i32, y: i32) -> bool {
    return x == y;
}

fn assign_value_to_reference(reference: &mut i32, value: i32) {
    *reference = value;
}