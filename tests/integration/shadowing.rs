fn main() {
    let x: i32 = 1;
    let mut mutable: bool = true;
    println(mutable);
    println(x);
    {
        let mut x:i32 =2;
        println(x);
        x = 3;
        println(x);
        mutable = false;
        println(mutable);
    };
    println(mutable);
    println(x);

    let x: i32 = mut_x(x);
    println(x);
}
fn mut_x(mut x: i32) -> i32 {
    println(x);
    x = 4;
    println(x);
    return x;
}