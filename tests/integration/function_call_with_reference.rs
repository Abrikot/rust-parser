fn main() {
    let i: i32 = 35;
    test(i);
    println(i);
}
fn test(mut i: i32) {
    i = 42;
    println(i);
}