fn main() {
    let a: i32 = 2;
    let b: &i32 = &a;
    let mut x: &mut i32 = & mut 5;
    println(x);

    {
        let y: &mut &mut i32 = &mut x;
        **y = 3;
    };

    test(x);
    println(x);

    let y: &bool= &true;
    println(y);
}

fn test(z: &mut i32) {
    println(z);
    *z = (*z) + 1;
}