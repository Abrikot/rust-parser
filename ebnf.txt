(* Basic definitions *)
letter = "a" | "b"| "c" | "d" | "e" | "f" | "g" | "h" | "i"| "j" | "k" | "l" | "m" | "n" | "o" | "p"| "q" | "r" | "s" | "t" | "u" | "v" | "w"| "x" | "y" | "z" ;
digit_excluding_zero = "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9";
digit                = "0" | digit_excluding_zero;

number = digit_excluding_zero | { digit };
boolean = "true" | "false";
terminal = number | boolean;
primitive_type = "i32" | "bool";
type =  [ &, [ "mut" ], type] | primitive_type;

(* Operations *)
binary_operator = "+"
    | "-"
    | "*"
    | "/"
    | "%"
    | "&&"
    | "||"
    | "=="
    | "^"
    | "<"
    | "!=";
binary_operation = (parenthesized_expression | unary_operation | terminal | variable), binary_operator, expression;
unary_operator = "-"
    | "!";
unary_operation = unary_operator, expression;

(* Expressions *)
parenthesized_expression = "(", expression, ")";
expression = block
    | if
    | function_call
    | binary_operation
    | unary_operation
    | parenthesized_expression
    | terminal
    | variable
    | reference
    | dereference;
reference = "&", ["mut"], expression;
dereference = "*", expression;

(* Variables *)
object_name = { letter | "_" };
variable = object_name;
declaration = "let", variable, ":", type, "=", expression;

(* Statements *)
left_hand_side = variable
    | ("&", left_hand_side)
    | ("*", left_hand_side);
assignment = left_hand_side, "=",expression;

non_returning_statement = (while | declaration | assignment | expression), ";";
returning_statement = "return", expression, ";";

(* Blocks *)
block = "{", [{ non_returning_statement }], [ returning_statement ], "}";

(* Conditionals *)
if = "if", expression, block, ["else", block];
while = "while", expression, block;

(* Functions *)
parameters_list = [{ "mut", object_name, ":", type }];
return_type = ["->", primitive_type];
function = "fn", object_name, "(", parameters_list, ")", return_type, block;

(* Functions calls *)
arguments_list = [expression, [{",", expression}]];
function_call = object_name, "(", arguments_list, ")";

(* File *)
file = [{ function }];